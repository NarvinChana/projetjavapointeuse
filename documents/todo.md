- [x] Simplified client Employee model

- [x] Serialization & modification of ip and port in NetworkConfigController

- [x] Serialization of simplified model

- [x] Save/Load on client relaunch or crash
  
  - [x] Employees and model version
  - [x] Punch-ins/outs to send

- [x] Push employees into controller to display in View

- [x] Client Sync
  
  - [x] Receive new model
  - [x] Ping version to server

- [x] com.noradrenalin.server.Server view
  
  - [x] Employee
  - [x] Punch-ins/outs

- [x] com.noradrenalin.server.Server controller
  
  - [x] Initialize the view 
  - [x] Update elements from model
  - [x] Receive data packets from client and transform them into server model data
  - [x] Add valid ip and port of incoming data from a client to a list

- [x] com.noradrenalin.server.Server model : Add simplified client Employee model

- [x] Update client interface

- [x] Allow TCPController multi address sending of packets

- [x] Fix potential overlap of timeslots