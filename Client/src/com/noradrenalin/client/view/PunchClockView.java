package com.noradrenalin.client.view;

import com.noradrenalin.common.model.ClientEmployee;

import javax.swing.*;
import java.awt.*;

/**
 * View for the punch clock. Creates the GUI.
 */
public class PunchClockView {

    private final JFrame frame;
    private final JButton punchInButton;
    private final JButton configButton;
    private final JComboBox<ClientEmployee> employeeComboBox;
    private final JLabel welcomeLabel;
    private final JLabel timeLabel;
    private final JLabel dateLabel;
    private final JLabel roundedTime;
    private final JLabel ledIndicatorLabel;
    private final JLabel ledIndicator;
    private final JLabel timeSinceLabel;

    /**
     * Constructor of class com.noradrenalin.client.view.PunchClockView.
     */
    public PunchClockView(String title) {
        // Create JFrame
        frame = new JFrame(title);

        // Create UI elements
        punchInButton = new JButton();
        configButton = new JButton();
        employeeComboBox = new JComboBox<>();
        welcomeLabel = new JLabel();
        timeLabel = new JLabel();
        dateLabel = new JLabel();
        roundedTime = new JLabel();
        ledIndicatorLabel = new JLabel();
        ledIndicator = new JLabel();
        timeSinceLabel = new JLabel();

        // Add UI elements to frame
        frame.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Positioning welcomeLabel
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 1;
        c.gridwidth = 4;
        c.gridy = 0;
        c.weighty = 5;
        c.insets = new Insets(5,5,5,5);
        frame.add(welcomeLabel, c);

        // Positioning dateLabel
        c.fill = GridBagConstraints.CENTER;
        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 1;
        c.insets = new Insets(0,0,0,0);
        frame.add(dateLabel, c);

        // Positioning timeLabel
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.weightx = 2;
        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 1;
        c.gridy = 2;
        frame.add(timeLabel, c);

        // Positioning roundedTime
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 0);
        c.gridx = 2;
        c.gridy = 2;
        frame.add(roundedTime, c);

        // Positioning ledIndicatorLabel
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_END;
        c.gridwidth = 1;
        c.weightx = 2;
        c.gridx = 1;
        c.gridy = 4;
        frame.add(ledIndicatorLabel, c);

        // Positioning ledIndicator
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.CENTER;
        c.gridwidth = 1;
        c.weightx = 0;
        c.gridx = 2;
        c.gridy = 4;
        c.insets = new Insets(0, 5, 0, 0);
        frame.add(ledIndicator, c);

        // Positioning employeeComboBox
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 3;
        c.gridwidth = 2;
        c.gridx = 1;
        c.gridy = 3;
        c.insets = new Insets(5,5,5,5);
        frame.add(employeeComboBox, c);

        // Positioning punchInButton
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 3;
        c.insets = new Insets(5,5,5,5);
        frame.add(punchInButton, c);

        // Positioning configButton
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 1;
        c.insets = new Insets(5,5,5,5);
        frame.add(configButton, c);

        // Positioning timeSinceLabel
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 4;
        c.insets = new Insets(30,20,0,0);
        frame.add(timeSinceLabel, c);

        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(560, 250));
        frame.pack();
        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public JButton getPunchInButton() {
        return punchInButton;
    }

    public JButton getConfigButton() {
        return configButton;
    }

    public JComboBox<ClientEmployee> getEmployeeComboBox() {
        return employeeComboBox;
    }

    public JLabel getWelcomeLabel() {
        return welcomeLabel;
    }

    public JLabel getTimeLabel() {
        return timeLabel;
    }

    public JLabel getDateLabel() {
        return dateLabel;
    }

    public JLabel getRoundedTime() {
        return roundedTime;
    }

    public JLabel getLedIndicatorLabel() {
        return ledIndicatorLabel;
    }

    public JLabel getLedIndicator() {
        return ledIndicator;
    }

    public JLabel getTimeSinceLabel() {
        return timeSinceLabel;
    }
}