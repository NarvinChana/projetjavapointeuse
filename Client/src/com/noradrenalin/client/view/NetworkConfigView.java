package com.noradrenalin.client.view;

import javax.swing.*;
import java.awt.*;

/**
 * View of the client's networkConfig window.
 */
public class NetworkConfigView {
    private final JFrame configFrame;
    private final JLabel ipLabel;
    private final JLabel portLabel;
    private final JLabel localPortLabel;
    private final JTextPane localPortValueTextPane;
    private final JTextPane ipTextPane;
    private final JTextPane portTextPane;
    private final JButton acceptButton;
    private final JButton cancelButton;

    public NetworkConfigView(String title) {
        configFrame = new JFrame(title);
        ipLabel = new JLabel();
        portLabel = new JLabel();
        localPortLabel = new JLabel();
        localPortValueTextPane = new JTextPane();
        ipTextPane = new JTextPane();
        portTextPane = new JTextPane();
        acceptButton = new JButton();
        cancelButton = new JButton();

        // Add UI elements to frame
        configFrame.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        // Positioning ipLabel
        c.fill = GridBagConstraints.CENTER;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 0;
        configFrame.add(ipLabel, c);

        // Positioning portLabel
        c.fill = GridBagConstraints.CENTER;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 1;
        configFrame.add(portLabel, c);

        // Positioning localPortLabel
        c.fill = GridBagConstraints.CENTER;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 2;
        configFrame.add(localPortLabel, c);

        // Positioning ipTextPane
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 0;
        configFrame.add(ipTextPane, c);

        // Positioning portTextPane
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 1;
        configFrame.add(portTextPane, c);

        // Positioning localPortValueTextPane
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 2;
        configFrame.add(localPortValueTextPane, c);

        // Positioning acceptButton
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 1;
        c.gridy = 3;
        configFrame.add(cancelButton, c);

        // Positioning cancelButton
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 0;
        c.gridy = 3;
        configFrame.add(acceptButton, c);

        configFrame.setLocationRelativeTo(null);
        configFrame.setMinimumSize(new Dimension(400, 200));
        configFrame.pack();
        configFrame.setVisible(true);
    }

    public JFrame getConfigFrame() {
        return configFrame;
    }

    public JLabel getIpLabel() {
        return ipLabel;
    }

    public JLabel getPortLabel() {
        return portLabel;
    }

    public JTextPane getIpTextPane() {
        return ipTextPane;
    }

    public JTextPane getPortTextPane() {
        return portTextPane;
    }

    public JButton getAcceptButton() {
        return acceptButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JLabel getLocalPortLabel() {
        return localPortLabel;
    }

    public JTextPane getLocalPortValueTextPane() {
        return localPortValueTextPane;
    }

}
