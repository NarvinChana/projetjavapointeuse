package com.noradrenalin.client.controller;

import com.noradrenalin.client.controller.listeners.NetworkConfigChangeListener;
import com.noradrenalin.client.view.NetworkConfigView;
import com.noradrenalin.common.network.NetworkConfig;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Controls the networkConfig window's logic. Uses a NetworkConfigView and NetworkConfig.
 */
public class NetworkConfigController {
    private NetworkConfigView configView;

    private NetworkConfig configModel;
    private NetworkConfig configLocalPortModel;

    private NetworkConfigChangeListener cListener = null;

    /**
     * Accepts and sets up the new network settings.
     */
    private final ActionListener acceptButtonListener = e -> {
        NetworkConfig tempNetConf = null;
        NetworkConfig tempLocalPort = null;
        try {
            tempNetConf = new NetworkConfig(configView.getIpTextPane().getText(), Integer.parseInt(configView.getPortTextPane().getText()));
            tempLocalPort = new NetworkConfig(Integer.parseInt(configView.getLocalPortValueTextPane().getText()));
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(
                    configView.getConfigFrame(),
                    "<html>There was error changing the network configuration.<br>The specified port/IP is not valid. Please verify what you entered.</html>",
                    "Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
        if (tempNetConf != null && tempLocalPort != null) {
            configModel = tempNetConf;
            configLocalPortModel = tempLocalPort;
            if (cListener != null) {
                cListener.networkConfigChanged();
            }
            configView.getConfigFrame().dispose();
            configView = null;
        }
    };

    /**
     * Closes the frame.
     */
    private final ActionListener cancelButtonListener = e -> {
        configView.getConfigFrame().dispose();
        configView = null;
    };

    public NetworkConfigController() throws UnknownHostException {
        this.configModel = new NetworkConfig();
        this.configLocalPortModel = new NetworkConfig(2320);
    }

    public NetworkConfigView getNetworkConfigView() {
        return configView;
    }

    public NetworkConfig getNetworkConfig() {
        return configModel;
    }

    public void setNetworkConfig(NetworkConfig netConf) {
        this.configModel = netConf;
    }

    public NetworkConfig getLocalPortNetworkConfig() {
        return configLocalPortModel;
    }

    public void setConfigLocalPortModel(NetworkConfig configLocalPortModel) {
        this.configLocalPortModel = configLocalPortModel;
    }

    public void setCListener(NetworkConfigChangeListener listener) {
        this.cListener = listener;
    }

    /**
     * Initializes the config view's elements with their default texts.
     */
    public void initConfigView() {
        configView = new NetworkConfigView("Network Configuration");
        configView.getIpLabel().setText("IP Address : ");
        configView.getPortLabel().setText("Port : ");
        configView.getLocalPortLabel().setText("Local Server Port : ");

        configView.getIpTextPane().setText(configModel.getIpAsString());
        configView.getPortTextPane().setText(configModel.getPortAsString());
        configView.getLocalPortValueTextPane().setText(configLocalPortModel.getPortAsString());

        configView.getAcceptButton().setText("Accept");
        configView.getCancelButton().setText("Cancel");

        configView.getAcceptButton().addActionListener(acceptButtonListener);
        configView.getCancelButton().addActionListener(cancelButtonListener);

        configView.getConfigFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                configView = null;
                super.windowClosing(e);
            }
        });
    }
}
