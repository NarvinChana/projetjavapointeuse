package com.noradrenalin.client.controller.listeners;

import java.util.EventListener;

/**
 * Listens for the change of the NetworkConfig.
 */
public interface NetworkConfigChangeListener extends EventListener {
    void networkConfigChanged();
}