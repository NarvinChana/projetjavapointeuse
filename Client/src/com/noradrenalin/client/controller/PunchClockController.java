package com.noradrenalin.client.controller;

import com.noradrenalin.client.controller.listeners.NetworkConfigChangeListener;
import com.noradrenalin.client.view.PunchClockView;
import com.noradrenalin.common.model.ClientDatabase;
import com.noradrenalin.common.model.ClientEmployee;
import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.NetworkThreadException;
import com.noradrenalin.common.network.TCPController;
import com.noradrenalin.common.network.listeners.PacketMaxRetryListener;
import com.noradrenalin.common.network.listeners.TCPErrorListener;
import com.noradrenalin.common.network.packets.DatabasePacket;
import com.noradrenalin.common.network.packets.Packet;
import com.noradrenalin.common.network.packets.PingPacket;
import com.noradrenalin.common.network.packets.PunchClockPacket;
import com.noradrenalin.common.network.packets.listeners.DatabasePacketListener;
import com.noradrenalin.common.network.packets.listeners.PacketListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

import static com.noradrenalin.common.util.time.TimeOperations.roundToNearestQuarterHour;

/**
 * Controls the PunchClock's logic. Uses the com.noradrenalin.common.model and view.
 */
public class PunchClockController implements NetworkConfigChangeListener, DatabasePacketListener, TCPErrorListener, PacketMaxRetryListener {
    private String CLIENT_DATA_PATH;
    private static final int UPDATE_CLOCK_DELAY = 500;
    private static final int PING_SERVER_DELAY = 5000;
    
    private PunchClockView view;
    private NetworkConfigController configController;

    private TCPController network;
    private ClientDatabase database;

    private LocalTime lastReceivedTime;


    private enum ConnectionState  {
        CONNECTED,
        NOT_CONNECTED
    }

    /**
     * Listener called when the punch-in/out button is clicked.
     */
    private final ActionListener punchInButtonListener = e -> {
        Object selectedItem = view.getEmployeeComboBox().getSelectedItem();

        if (selectedItem != null) {
            ClientEmployee employee = (ClientEmployee) selectedItem;
            sendPunchClockPacket(employee);
        }
    };

    /**
     * Listener called when the configuration button is clicked.
     */
    private final ActionListener configButtonListener = e -> {
        if (configController.getNetworkConfigView() == null) {
            configController.initConfigView();
        } else {
            configController.getNetworkConfigView().getConfigFrame().toFront();
            configController.getNetworkConfigView().getConfigFrame().requestFocus();
        }
    };

    /**
     * Listener called at a regular interval to update the displayed time.
     */
    private final ActionListener updateClockAction = e -> {
        updateDateLabel();
        updateTimeLabel();
        updateLastPacketTime();
    };

    /**
     * Listener called at a regular interval to send a PingPacket to the server.
     */
    private final ActionListener pingServer = e -> pingServer();

    public PunchClockController(PunchClockView view) {
        this(view, "client-config.ser");
    }

    public PunchClockController(PunchClockView view, String savePath) {
        this.CLIENT_DATA_PATH = savePath;

        try {
            configController = new NetworkConfigController();
            configController.setCListener(this);
        } catch (UnknownHostException ignored) {
            // Can't happen as only default constructors with valid data are used.
        }

        try {
            deserializeData();
        } catch (IOException e) {
            System.out.println("No serialized data found, creating new data on exit.");
            database = new ClientDatabase();
            network = new TCPController(configController.getNetworkConfig(), configController.getLocalPortNetworkConfig());
            network.addListener((PacketListener) this);
            network.addListener((DatabasePacketListener) this);
            network.addListener((TCPErrorListener) this);
            network.addListener((PacketMaxRetryListener) this);
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found.");
            JOptionPane.showMessageDialog(view.getFrame(), "", "Error while opening the database.", JOptionPane.WARNING_MESSAGE);
        }

        this.view = view;
        initView();

        initController();
    }


    @Override
    public void packetMaxRetry(Packet packet, NetworkConfig dest) {
        if (!(packet instanceof PingPacket)) {
            network.sendPacket(packet, dest);
        }
    }

    @Override
    public void networkConfigChanged() {
        network.changeServerPort(configController.getLocalPortNetworkConfig());
        pingServer();
    }

    @Override
    public synchronized void databasePacketReceived(DatabasePacket dbPacket) {
        if (dbPacket.getDatabase().getVersionCode() != database.getVersionCode()) {
            database = dbPacket.getDatabase();
            updateEmployeeList();
        }
    }

    @Override
    public void packetReceived(Packet packet, NetworkConfig sender) {
        System.out.println("Packet received from :" + sender);
        setNetworkIndicatorState(ConnectionState.CONNECTED);
        lastReceivedTime = LocalTime.now();
    }

    /**
     * Allows the client to manage any tcp network errors that occur.
     * @param e exception that has been raised
     */
    @Override
    public void tcpErrorOccurred(Exception e) {
        setNetworkIndicatorState(ConnectionState.NOT_CONNECTED);

        NetworkThreadException exception = (NetworkThreadException) e;

        if (exception.getType() == NetworkThreadException.Type.PassiveSocketOpen) {
            JOptionPane.showMessageDialog(view.getFrame(), "A problem occurred while opening the serverSocket on the specified local port. If this persists please try changing it.", "Error opening local serverSocket", JOptionPane.WARNING_MESSAGE);
        }
    }

    /**
     * Launches the window logic and updates routines.
     */
    private void initController() {
        Timer updateClockTimer = new Timer(UPDATE_CLOCK_DELAY, updateClockAction);
        updateClockTimer.start();

        Timer pingServerTimer = new Timer(PING_SERVER_DELAY, pingServer);
        pingServerTimer.start();
        pingServer();
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        view.getWelcomeLabel().setText("Welcome to Noradrenalin!");
        view.getWelcomeLabel().setFont(new Font("Dialog", Font.BOLD, 24));
        view.getDateLabel().setFont(new Font("Dialog", Font.BOLD, 18));
        view.getEmployeeComboBox().setToolTipText("Select an employee");
        view.getPunchInButton().setText("Punch in/out");
        view.getConfigButton().setText("Configure Network Settings");
        view.getEmployeeComboBox().setSelectedItem(null);
        view.getPunchInButton().addActionListener(punchInButtonListener);
        view.getConfigButton().addActionListener(configButtonListener);
        view.getLedIndicatorLabel().setText("State of connection : ");
        setNetworkIndicatorState(ConnectionState.NOT_CONNECTED);

        updateDateLabel();
        updateTimeLabel();

        view.getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        view.getFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    serializeData();
                } catch (IOException ioException) {
                    System.out.println("Serialization failed, data will not be saved and corrupted data will be destroyed.");
                    File file = new File(CLIENT_DATA_PATH);
                    if (!file.delete()) {
                        System.out.println("Deletion of corrupted data failed, there may be a problem on relaunch.");
                    }
                }
                e.getWindow().dispose();
                System.exit(0);
            }
        });
    }

    /**
     * Updates the shown date if it has changed.
     */
    private void updateDateLabel() {
        String newDateString = LocalDate.now().toString();
        if (!newDateString.equals(view.getDateLabel().getText())) {
            view.getDateLabel().setText(newDateString);
        }
    }

    /**
     * Updates the shown time and roundedTime.
     */
    private void updateTimeLabel() {
        view.getTimeLabel().setText(LocalTime.now().truncatedTo(ChronoUnit.SECONDS).toString());

        LocalTime roundedNewTime = roundToNearestQuarterHour(LocalTime.now());
        view.getRoundedTime().setText("... let's say " + roundedNewTime.truncatedTo(ChronoUnit.SECONDS).toString());
    }

    /**
     * Adds an employee to the comboBox.
     * @param employee Employee
     */
    private void addEmployeeToComboBox(ClientEmployee employee) {
        view.getEmployeeComboBox().addItem(employee);
    }

    /**
     * Serializes data for next launch of PunchClock.
     * @throws IOException File error in ObjectOutputStream or file creation in FileOutputStream
     */
    private void serializeData() throws IOException {
        FileOutputStream fos = new FileOutputStream(CLIENT_DATA_PATH);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(configController.getNetworkConfig());
        oos.writeObject(configController.getLocalPortNetworkConfig());
        oos.writeObject(network);
        oos.close();
        fos.close();
    }

    /**
     * Deserializes data when the PunchClock launches.
     * @throws IOException File error in ObjectInputStream or file creation in FileInputStream
     * @throws ClassNotFoundException Class doesn't exist
     */
    private void deserializeData() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(CLIENT_DATA_PATH);
        ObjectInputStream ois = new ObjectInputStream(fis);

        database = new ClientDatabase();
        configController.setNetworkConfig((NetworkConfig) ois.readObject());
        configController.setConfigLocalPortModel((NetworkConfig) ois.readObject());
        network = (TCPController) ois.readObject();
        network.addListener((PacketListener) this);
        network.addListener((DatabasePacketListener) this);
        network.addListener((TCPErrorListener) this);

        ois.close();
        fis.close();
    }

    /**
     * Sends a PunchClockPacket for the employee at the current date and time.
     * @param ce Employee to send
     */
    private void sendPunchClockPacket(ClientEmployee ce) {
        PunchClockPacket packet = new PunchClockPacket(network.getPacketID(),
                configController.getLocalPortNetworkConfig().getPort(),
                database.getVersionCode(),
                ce.getId(),
                LocalDateTime.of(LocalDate.now(), roundToNearestQuarterHour(LocalTime.now())));

        network.sendPacket(packet, configController.getNetworkConfig());
    }

    /**
     * Updates the ComboBox with to match the database version.
     */
    private void updateEmployeeList() {
        view.getEmployeeComboBox().removeAllItems();

        for(ClientEmployee ce : database.getEmployees()) {
            addEmployeeToComboBox(ce);
        }
    }

    /**
     * Sends a ping to the server to verify local DB.
     */
    private void pingServer() {
        PingPacket pingPacket = new PingPacket(network.getPacketID(), configController.getLocalPortNetworkConfig().getPort(), database.getVersionCode());
        network.sendPacket(pingPacket, configController.getNetworkConfig());
    }

    /**
     * Sets the connection state of the client's LED indicator.
     * @param c ConnectionState enum
     */
    private void setNetworkIndicatorState(ConnectionState c) {
        switch (c) {
            case CONNECTED -> {
                view.getLedIndicator().setText("CONNECTED");
                view.getLedIndicator().setFont(new Font("DIALOG", Font.BOLD, 18));
                view.getLedIndicator().setForeground(Color.GREEN);
            }
            case NOT_CONNECTED -> {
                view.getLedIndicator().setText("NOT CONNECTED");
                view.getLedIndicator().setFont(new Font("DIALOG", Font.BOLD, 18));
                view.getLedIndicator().setForeground(Color.RED);
            }
        }

        view.getLedIndicator().repaint();
    }

    /**
     * Updates the label displaying the time since the last packet has been received.
     */
    private void updateLastPacketTime() {
        if (lastReceivedTime != null) {
            view.getTimeSinceLabel().setText(String.format("Time since last packet : %dms",Duration.between(lastReceivedTime, LocalTime.now()).toMillis()));
        } else {
            view.getTimeSinceLabel().setText("");
        }
    }
}