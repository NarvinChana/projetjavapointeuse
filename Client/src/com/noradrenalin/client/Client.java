package com.noradrenalin.client;

import com.noradrenalin.client.controller.PunchClockController;
import com.noradrenalin.client.view.PunchClockView;

/**
 * Main class of the client.
 */
public class Client {
    public static void main(String[] args) {
        PunchClockView view = new PunchClockView("Punch Clock");
        if(args.length == 0) {
            PunchClockController controller = new PunchClockController(view);
        } else {
            PunchClockController controller = new PunchClockController(view, args[0]);
        }
    }
}
