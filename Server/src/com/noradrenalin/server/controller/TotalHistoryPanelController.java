package com.noradrenalin.server.controller;

import com.noradrenalin.server.controller.listeners.TimeSlotHistoryListener;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import com.noradrenalin.server.view.ElementCBox;
import com.noradrenalin.server.view.TotalHistoryPanelView;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

/**
 * Controls the TotalHistory's logic. Uses the model and view.
 */
public class TotalHistoryPanelController {
    private TotalHistoryPanelView view;
    private Database database;
    private TimeSlotHistoryListener listener;

    private int selectedIndex = 0;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Reload all the Panel if filterComboBox change.
     */
    private final ActionListener filterComboBoxListener = e -> reloadTotalHistoryPanel();

    /**
     * Checks if a table reload is needed
     */
    private final ActionListener empOrDeptComboBoxListener = e -> {
        if (view.getEmpOrDeptComboBox().getSelectedIndex() != selectedIndex && view.getEmpOrDeptComboBox().getItemCount() != 0) {
            selectedIndex = view.getEmpOrDeptComboBox().getSelectedIndex();
            reloadTotalHistoryTable();
        }
    };

    /**
     * Launches a research when pressing the Enter key.
     */
    private final Action launchResearchKeyListener = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            reloadTotalHistoryTable();
        }
    };

    /**
     * Opens the EditTimeSlotFrame.
     */
    private final ActionListener addTimeSlotButtonListener = e -> listener.openEditTimeSlotFrame((LocalDate) null);

    public TotalHistoryPanelController(TotalHistoryPanelView view, Database database) {
        this.view = view;
        this.database = database;

        initView();
    }

    public void setListener(TimeSlotHistoryListener listener) {
        this.listener = listener;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * Reloads everything on the page
     */
    public void reloadTotalHistoryPanel() {
        view.getEmpOrDeptComboBox().removeActionListener(empOrDeptComboBoxListener);

        view.getSearchDateTextField().setText("");
        selectedIndex = 0;
        reloadEmpOrDeptComboBox();
        reloadTotalHistoryTable();

        view.getEmpOrDeptComboBox().addActionListener(empOrDeptComboBoxListener);
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        view.getAddTimeSlotButton().setText("Add TimeSlot");
        view.getAddTimeSlotButton().addActionListener(addTimeSlotButtonListener);

        view.getSearchDateLabel().setText("Date : ");
        view.getSearchDateButton().setText("Search");
        view.getSearchDateButton().addActionListener(launchResearchKeyListener);

        view.getFilterLabel().setText("Filter by : ");
        view.getFilterComboBox().addItem(new ElementCBox(1, "Employee"));
        view.getFilterComboBox().addItem(new ElementCBox(2, "Department"));
        view.getFilterComboBox().addActionListener(filterComboBoxListener);

        String[] headers = {"Name", "Date", "Arriving time", "Departure time"};
        setTotalHistoryTableHeader(headers);

        // Allows the TimeSlots with a problem to be highlighted
        view.getHistoryTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setHorizontalAlignment(JLabel.CENTER);

                if (value.equals("null")) {
                    setValue("");
                    c.setBackground(Color.LIGHT_GRAY);
                    Font f = c.getFont();
                    c.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
                } else {
                    c.setBackground(Color.WHITE);
                    c.setForeground(Color.BLACK);
                }

                if (column == 2 || column == 3) {
                    try {
                        Employee emp = (Employee) view.getHistoryModel().getDataVector().elementAt(row).elementAt(0);
                        LocalDate date = LocalDate.parse((String) view.getHistoryModel().getDataVector().elementAt(row).elementAt(1), formatter);

                        LocalTime startTime = LocalTime.parse((String) view.getHistoryModel().getDataVector().elementAt(row).elementAt(2));
                        LocalTime endTime = LocalTime.parse((String) view.getHistoryModel().getDataVector().elementAt(row).elementAt(3));

                        TimeSlot timeSlot = new TimeSlot(startTime, endTime);

                        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> stateMap =
                                HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(emp, date, timeSlot);

                        if (column == 2) {
                            if (stateMap.getKey() == HistoryPanelUtils.EmployeeState.EarlyStart) {
                                c.setBackground(new Color(0, 129, 72));
                                c.setForeground(Color.WHITE);
                            } else if (stateMap.getKey() == HistoryPanelUtils.EmployeeState.LateStart) {
                                c.setBackground(new Color(226, 50, 33, 255));
                                c.setForeground(Color.WHITE);
                            }
                        }

                        if (column == 3) {
                            if (stateMap.getValue() == HistoryPanelUtils.EmployeeState.EarlyEnd) {
                                c.setBackground(new Color(226, 50, 33, 255));
                                c.setForeground(Color.WHITE);
                            } else if (stateMap.getValue() == HistoryPanelUtils.EmployeeState.LateEnd) {
                                c.setBackground(new Color(0, 129, 72));
                                c.setForeground(Color.WHITE);
                            }
                        }
                    } catch (DateTimeParseException | IncorrectTimeException | IllegalArgumentException ignored) {}
                }

                if (hasFocus) {
                    Vector vector = view.getHistoryModel().getDataVector().elementAt(row);
                    listener.openEditTimeSlotFrame(vector);
                }

                return c;
            }
        });

        reloadTotalHistoryPanel();

        view.getMainPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "launchResearchKeyListener");
        view.getMainPanel().getActionMap().put("launchResearchKeyListener", launchResearchKeyListener);
    }

    /**
     * Sets the headers for the total history table.
     * @param headers The list of headers
     */
    private void setTotalHistoryTableHeader(String[] headers) {
        for (String header : headers) {
            view.getHistoryModel().addColumn(header);
        }
    }

    /**
     * Adds data into the daily history table.
     * @param data The data to insert into the table
     */
    private void addDataToTotalHistoryTable(Object[] data) {
        if (data.length != view.getHistoryModel().getColumnCount()) {
            System.out.println("Data size incorrect");
        } else {
            view.getHistoryModel().addRow(data);
        }
    }

    /**
     * Adds all selected employee's timeslots for the selected date into the table.
     * @param employee Employee
     * @param date Date
     */
    private void addDataOfEmployee(Employee employee, LocalDate date) {
        if (date == null) {
            HashMap<LocalDate, ArrayList<TimeSlot>> timeSlots = employee.getActual().getTimeSlots();

            ArrayList<LocalDate> dates = new ArrayList<>(timeSlots.keySet());
            Collections.sort(dates);
            for (int i = dates.size() - 1; i >= 0; i--) {
                LocalDate key = dates.get(i);

                ArrayList<TimeSlot> times = timeSlots.get(key);
                Collections.sort(times);
                for (int j = times.size() - 1; j >=0; j--) {
                    TimeSlot timeSlot = times.get(j);
                    Object[] data = {employee, key.format(formatter), String.valueOf(timeSlot.getStartTime()), String.valueOf(timeSlot.getEndTime())};
                    addDataToTotalHistoryTable(data);
                }
            }
        } else {
            try {
                ArrayList<TimeSlot> timeSlots = employee.getActual().getTimeSlotsOfDate(date);
                Collections.sort(timeSlots);

                for (int i = timeSlots.size() - 1; i >=0; i--) {
                    TimeSlot timeSlot = timeSlots.get(i);
                    Object[] data = {employee, date.format(formatter), String.valueOf(timeSlot.getStartTime()), String.valueOf(timeSlot.getEndTime())};
                    addDataToTotalHistoryTable(data);
                }
            } catch (ElementNotFoundException e) {
                // The employee doesn't have any data for this day, that's not a problem
                System.out.println(employee + " does not have any timeslots for " + date.format(formatter));
            }
        }
    }

    /**
     * Adds all selected department's timeslots into the table.
     * @param department department to add
     */
    private void addDataOfDepartment(Department department, LocalDate date) {
        for (Employee employee : database.getEmployees()) {
            if (employee.getDepartment().equals(department))
            {
                addDataOfEmployee(employee, date);
            }
        }
    }

    /**
     * Reloads the history table
     */
    private void reloadTotalHistoryTable() {
        String date = view.getSearchDateTextField().getText();
        LocalDate searchDate = null;
        boolean excTriggered = false;

        if (!date.isEmpty()) {
            try {
                searchDate = LocalDate.parse(date, formatter);
            } catch (DateTimeParseException exc) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "Date is invalid.", "Error", JOptionPane.ERROR_MESSAGE);
                view.getSearchDateTextField().setText("");
                excTriggered = true;
            }
        }

        if (!excTriggered) {
            view.getHistoryModel().setRowCount(0);

            ElementCBox filterSelected = (ElementCBox) view.getFilterComboBox().getSelectedItem();
            assert filterSelected != null;

            if (selectedIndex == 0) {
                for (Employee employee : database.getEmployees()) {
                    addDataOfEmployee(employee, searchDate);
                }
            } else {
                if (filterSelected.getId() == 1) {
                    Employee employee = (Employee) view.getEmpOrDeptComboBox().getSelectedItem();
                    addDataOfEmployee(employee, searchDate);
                } else if (filterSelected.getId() == 2) {
                    Department department = (Department) view.getEmpOrDeptComboBox().getSelectedItem();
                    assert department != null;
                    addDataOfDepartment(department, searchDate);
                }
            }

            if (view.getHistoryModel().getRowCount() == 0 && (searchDate != null || view.getEmpOrDeptComboBox().getSelectedIndex() != 0)) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "Nothing found for this date/employee/department combination.", "No entry found", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Reloads the employees list
     */
    private void reloadEmpOrDeptComboBox() {
        JComboBox<Object> empOrDeptSelected = view.getEmpOrDeptComboBox();
        ElementCBox filterSelected = (ElementCBox) view.getFilterComboBox().getSelectedItem();
        assert filterSelected != null;

        for (ActionListener al : empOrDeptSelected.getActionListeners()) {
            empOrDeptSelected.removeActionListener(al);
        }

        empOrDeptSelected.removeAllItems();

        if (filterSelected.getId() == 1) {
            empOrDeptSelected.addItem("All employees");
            for (Employee employee : database.getEmployees()) {
                empOrDeptSelected.addItem(employee);
            }
        } else if (filterSelected.getId() == 2) {
            empOrDeptSelected.addItem("All departments");
            for (Department department : database.getDepartments()) {
                empOrDeptSelected.addItem(department);
            }
        }

        view.getAddTimeSlotButton().setEnabled(empOrDeptSelected.getItemCount() != 0);

        view.getEmpOrDeptComboBox().addActionListener(empOrDeptComboBoxListener);
    }
}
