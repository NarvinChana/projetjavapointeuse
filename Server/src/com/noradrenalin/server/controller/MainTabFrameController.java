package com.noradrenalin.server.controller;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.TCPController;
import com.noradrenalin.common.network.listeners.PacketMaxRetryListener;
import com.noradrenalin.common.network.packets.DatabasePacket;
import com.noradrenalin.common.network.packets.Packet;
import com.noradrenalin.common.network.packets.PingPacket;
import com.noradrenalin.common.network.packets.PunchClockPacket;
import com.noradrenalin.common.network.packets.listeners.PingPacketListener;
import com.noradrenalin.server.controller.listeners.*;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.common.network.packets.listeners.PunchClockPacketListener;
import com.noradrenalin.server.util.csv.CSVExportDatabase;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.database.DatabaseListener;
import com.noradrenalin.server.view.MainTabFrameView;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.net.UnknownHostException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Controls the MainTab's logic. Uses the model and view.
 */
public class MainTabFrameController implements
        DatabaseListener,
        PingPacketListener,
        PunchClockPacketListener,
        NetworkConfigChangedListener,
        EditTimeSlotListener,
        TimeSlotHistoryListener,
        DatabaseExportListener,
        DatabaseImportListener,
        PacketMaxRetryListener,
        EmployeeManagementListener,
        PlanningManagementListener {

    public static final String SERVER_DATABASE_PATH = "server-database.ser";
    public static final String SERVER_CONFIG_PATH = "server-config.ser";

    private final MainTabFrameView view;
    private Database database;

    private final DailyHistoryPanelController dailyController;
    private final TotalHistoryPanelController totalController;
    private final EmployeeManagementPanelController employeeController;
    private final DepartmentManagementPanelController departmentController;
    private final SettingsPanelController parametersController;
    private final EditTimeSlotFrameController editTimeSlotFrameController;
    private final PlanningManagementFrameController planningController;

    private TCPController network;
    private List<NetworkConfig> clientWhitelist;
    private final List<NetworkConfig> clientBlacklist;
    private final List<NetworkConfig> clientsAskingPermission;

    private NetworkConfig serverPort;

    /**
     * Reloads the selected panel if needed
     */
    private final ChangeListener tabbedPaneListener = e -> reloadGUI();

    public MainTabFrameController(MainTabFrameView view) throws UnknownHostException, ClassNotFoundException {
        clientsAskingPermission = new ArrayList<>();

        clientWhitelist = new ArrayList<>();
        clientBlacklist = new ArrayList<>();

        serverPort = new NetworkConfig();

        this.view = view;

        try {
            deserializeData();
        } catch (IOException e) {
            System.out.println("No serialized data found, creating new data on exit.");
            database = new Database();
            database.addListener(this);
            network = new TCPController(new NetworkConfig(), serverPort);
            network.addListener((PingPacketListener) this);
            network.addListener((PacketMaxRetryListener) this);
            network.addListener((PunchClockPacketListener) this);
        }

        initView();

        dailyController = new DailyHistoryPanelController(view.getDailyView(), database);
        dailyController.setListener(this);

        totalController = new TotalHistoryPanelController(view.getTotalView(), database);
        totalController.setListener(this);

        employeeController = new EmployeeManagementPanelController(view.getEmployeeView(), database);
        employeeController.setListener(this);

        departmentController = new DepartmentManagementPanelController(view.getDepartmentView(), database);

        parametersController = new SettingsPanelController(
                view.getParametersView(),
                serverPort,
                clientWhitelist,
                clientBlacklist);
        parametersController.setNetConfChanged(this);
        parametersController.setDatabaseImportListener(this);
        parametersController.setDatabaseExportListener(this);

        editTimeSlotFrameController = new EditTimeSlotFrameController(database);
        editTimeSlotFrameController.setListener(this);

        planningController = new PlanningManagementFrameController();
        planningController.setListener(this);
    }

    /**
     * Called when the database's version code is updated.
     * @param d Database
     * @param oldCode Old code version
     * @param newCode New code version
     */
    @Override
    public void versionCodeUpdated(Database d, long oldCode, long newCode) {
        sendDatabaseToClients();
    }

    private void sendDatabaseToClients() {
        for (NetworkConfig n : clientWhitelist) {
            DatabasePacket updatePacket = new DatabasePacket(network.getPacketID(), serverPort.getPort(), database.toClientDatabase());

            try {
                network.sendPacket(updatePacket, n);
            } catch (Exception e) {
                System.out.println("There was an error while sanding the update packet to " + n.getIpAsString() + ":" + n.getPortAsString());
            }
        }
    }

    /**
     * Called when the department list size has changed.
     * @param d Database
     * @param newSize New department list size
     */
    @Override
    public void departmentsListSizeChanged(Database d, int newSize) {
        reloadEmployeeTabStatus();
    }

    @Override
    public void packetReceived(Packet packet, NetworkConfig sender) {
        System.out.println("Received packet : " + packet + " from: " + sender);
    }

    @Override
    public void pingPacketReceived(PingPacket packet, NetworkConfig networkConfig) {
        NetworkConfig tmp = new NetworkConfig(networkConfig.getIp(), packet.getPort());
        if (!clientsAskingPermission.contains(tmp)) {
            clientsAskingPermission.add(tmp);

            if (!clientBlacklist.contains(tmp)) {
                if (clientWhitelist.contains(tmp)) {
                    if (packet.getVersionCode() != database.getVersionCode()) {
                        DatabasePacket updatePacket = new DatabasePacket(network.getPacketID(), packet.getPort(), database.toClientDatabase());
                        network.sendPacket(updatePacket, new NetworkConfig(networkConfig.getIp(), packet.getPort()));
                    }
                } else {
                    int choice = JOptionPane.showConfirmDialog(view.getMainFrame(), "Client ip=" + networkConfig.getIpAsString() + " and port=" + packet.getPort() + " wants to connect to server. Do you accept? If you refuse, the address will be added to the blacklist.", "Accept Client?", JOptionPane.YES_NO_OPTION);
                    if (choice == JOptionPane.YES_OPTION) {
                        // Client was accepted

                        NetworkConfig temp;

                        synchronized (this) {
                            clientWhitelist.add(new NetworkConfig(networkConfig.getIp(), packet.getPort()));
                            parametersController.reloadClientData();
                            temp = clientWhitelist.get(clientWhitelist.size() - 1);
                        }

                        DatabasePacket updatePacket = new DatabasePacket(network.getPacketID(), serverPort.getPort(), database.toClientDatabase());
                        try {
                            network.sendPacket(updatePacket, temp);
                        } catch (Exception e) {
                            System.out.println("Update packet sending failed.");
                        }
                    } else {
                        // Client was refused
                        synchronized (this) {
                            clientBlacklist.add(new NetworkConfig(networkConfig.getIp(), packet.getPort()));
                        }
                    }
                }
            }
            clientsAskingPermission.remove(tmp);
        }
    }

    /**
     * Called when a PunchClockPacket is received and checks if the database versions are equal.
     * If the versions are equal, the punch-in/out will be added.
     * @param packet Packet received
     */
    @Override
    public synchronized void punchClockPacketReceived(PunchClockPacket packet) {
        if (packet.getVersionCode() == database.getVersionCode()) {
            for (Employee e : database.getEmployees()) {
                if (packet.getEmployeeId() == e.getId()) {
                    try {
                        e.addPunchIn(packet.getTime());
                        reloadGUI();
                        break;
                    } catch (TimeSlotOverlapException | IncorrectTimeException exception) {
                        JOptionPane.showMessageDialog(
                                view.getMainFrame(),
                                "<html>Punch Clock packet received was unable to be added to database." +
                                        "<br>Check the Punch Clock's version.</html>",
                                "Error adding Punch In/Out",
                                JOptionPane.WARNING_MESSAGE
                        );
                    }
                }
            }
        }
    }

    /**
     * Opens the EditTimeSlotFrame in adding mode
     * @param date Is today if open from DailyHistory and is null is opened from TotalHistory
     */
    @Override
    public void openEditTimeSlotFrame(LocalDate date) {
        view.getMainFrame().setEnabled(false);
        editTimeSlotFrameController.enableFrameAdding(date);
    }

    /**
     * Opens the EditTimeSlotFrame in editing mode
     * @param vector Is a line of data is a history table
     */
    @Override
    public void openEditTimeSlotFrame(Vector<Object> vector) {
        view.getMainFrame().setEnabled(false);
        editTimeSlotFrameController.enableFrameEditing(vector);
    }

    @Override
    public void closeEditTimeSlotFrame() {
        view.getMainFrame().setEnabled(true);
        editTimeSlotFrameController.disableFrame();

        reloadGUI();
    }

    @Override
    public void openPlanningManagement(Employee employee) {
        view.getMainFrame().setEnabled(false);
        planningController.enableFrame(employee);
    }

    @Override
    public void closePlanningManagement() {
        view.getMainFrame().setEnabled(true);
        planningController.disableFrame();
    }

    /**
     * Implemented interface, changes the receive port on which the server is open.
     * @param netConf NetworkConfig containing the new port
     */
    @Override
    public void networkConfigChanged(NetworkConfig netConf) {
        serverPort = netConf;
        network.changeServerPort(netConf);
    }

    @Override
    public synchronized void packetMaxRetry(Packet packet, NetworkConfig dest) {
        System.err.println("Removing " + dest + " from client list.");
        clientWhitelist.remove(dest);
        parametersController.reloadClientData();
    }

    /**
     * Modifies a database after following a successful importation.
     * @param database New database
     */
    @Override
    public void databaseChanged(Database database) {
        setDatabase(database);
        reloadGUI();
    }

    @Override
    public void databaseExport(BufferedWriter writer) throws IOException {
        CSVExportDatabase.saveCSV(database, writer);
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        JTabbedPane tabbedPane = view.getTabbedPane();
        tabbedPane.addTab("Today", null, view.getDailyView().getMainPanel(), "History of today's time slots");
        tabbedPane.addTab("Total", null, view.getTotalView().getMainPanel(), "History of all time slots");
        tabbedPane.addTab("Employee", null, view.getEmployeeView().getMainPanel(), "Employee management tab");
        tabbedPane.addTab("Department", null, view.getDepartmentView().getMainPanel(), "Department management tab");
        tabbedPane.addTab("Settings", null, view.getParametersView().getMainPanel(), "Settings tab");

        if (database.getDepartments().size() == 0) {
            view.getTabbedPane().setEnabledAt(2, false);
        }

        tabbedPane.addChangeListener(tabbedPaneListener);

        networkConfigChanged(serverPort);

        view.getMainFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        view.getMainFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                serializeData();
                e.getWindow().dispose();
                System.exit(0);
            }
        });
    }

    /**
     * Serializes data for next launch of the server
     * If the data was unable to be written in specific cases, the user will be able to choose between discarding the changes or retrying to save.
     */
    private void serializeData() {
        // This listener must be removed each time the database is serialized because the controller is not serialized
        database.removeListener(this);

        try {
            FileOutputStream fosDB = new FileOutputStream(SERVER_DATABASE_PATH);
            FileOutputStream fosConfig = new FileOutputStream(SERVER_CONFIG_PATH);
            try {
                ObjectOutputStream oosDB = new ObjectOutputStream(fosDB);
                ObjectOutputStream oosConfig = new ObjectOutputStream(fosConfig);

                try {
                    oosDB.writeObject(database);
                    oosConfig.writeObject(network);
                    oosConfig.writeObject(clientWhitelist);
                    oosConfig.writeObject(serverPort);
                } catch (InvalidClassException e) {
                    System.out.println("The object's class is invalid.");
                } catch (NotSerializableException e) {
                    System.out.println("The object is not serializable.");
                } catch (IOException e) {
                    System.out.println("Can't write in the file.");
                    retrySerialization();
                } finally {
                    try {
                        oosDB.close();
                        oosConfig.close();
                    } catch (IOException e) {
                        System.out.println("Can't close the ObjectOutputStream.");
                    }
                }
            } catch (IOException e) {
                System.out.println("Can't create the ObjectOutputStream.");
                retrySerialization();
            } finally {
                try {
                    fosDB.close();
                    fosConfig.close();
                } catch (IOException e) {
                    System.out.println("Can't close properly the file.");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Can't open or create the output file.");
            retrySerialization();
        }
    }

    /**
     * Shows a dialog box letting the user choose between closing without saving or retrying the serialization.
     */
    private void retrySerialization() {
        Object[] options = {"Retry", "Close"};

        int result = JOptionPane.showOptionDialog(view.getMainFrame(),
                "The data was not saved. Do you want to retry or quit without saving ?",
                "Serialization problem",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
        if (result == JOptionPane.YES_OPTION) {
            serializeData();
        }
    }

    /**
     * Deserializes data when the server launches
     */
    private void deserializeData() throws IOException, ClassNotFoundException {
        FileInputStream fisDB = new FileInputStream(SERVER_DATABASE_PATH);
        FileInputStream fisConfig = new FileInputStream(SERVER_CONFIG_PATH);

        try {
            ObjectInputStream oisDB = new ObjectInputStream(fisDB);
            database = (Database) oisDB.readObject();
            database.addListener(this);

            ObjectInputStream oisConfig = new ObjectInputStream(fisConfig);
            network = (TCPController) oisConfig.readObject();
            network.addListener((PingPacketListener) this);
            network.addListener((PacketMaxRetryListener) this);
            network.addListener((PunchClockPacketListener) this);
            clientWhitelist = (List<NetworkConfig>) oisConfig.readObject();
            serverPort = (NetworkConfig) oisConfig.readObject();

            try {
                oisDB.close();
                oisConfig.close();
            } catch (IOException e) {
                System.out.println("Can't close the ObjectInputStream.");
            }
        } finally {
            try {
                fisDB.close();
                fisConfig.close();
            } catch (IOException e) {
                System.out.println("Can't close properly the file.");
            }
        }
    }

    /**
     * Reloads current GUI and checks if employee tab should be activated or not.
     */
    private void reloadGUI() {
        int index = view.getTabbedPane().getSelectedIndex();

        switch (index) {
            case 0 -> dailyController.reloadDailyHistoryPanel();
            case 1 -> totalController.reloadTotalHistoryPanel();
            case 2 -> employeeController.reloadEmployeePanel();
            case 3 -> departmentController.reloadDepartmentPanel();
            case 4 -> parametersController.reloadClientData();
        }
    }

    /**
     * Changes value of database everywhere it is referenced
     * (this is NOT an elegant way to access the DB in all controllers)
     * @param database new database
     */
    private void setDatabase(Database database) {
        this.database = database;
        dailyController.setDatabase(database);
        totalController.setDatabase(database);
        employeeController.setDatabase(database);
        departmentController.setDatabase(database);
        editTimeSlotFrameController.setDatabase(database);

        sendDatabaseToClients();

        reloadEmployeeTabStatus();
        reloadGUI();
    }

    /**
     * Enables or disables the employee tab
     */
    private void reloadEmployeeTabStatus() {
        view.getTabbedPane().setEnabledAt(2, database.getDepartments().size() != 0);
    }
}
