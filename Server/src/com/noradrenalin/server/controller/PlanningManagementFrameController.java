package com.noradrenalin.server.controller;

import com.noradrenalin.server.controller.listeners.PlanningManagementListener;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import com.noradrenalin.server.view.ElementCBox;
import com.noradrenalin.server.view.PlanningManagementFrameView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Collections;

import static com.noradrenalin.common.util.time.TimeOperations.roundToNearestQuarterHour;

public class PlanningManagementFrameController {
    private PlanningManagementFrameView view;
    private PlanningManagementListener listener;

    private Employee employee;

    /**
     * If "+ Add timeslot" is selected, shows addButton and empties the textFields.
     * Otherwise, shows modifyButton and deleteButton and fills the textFields with the available data.
     */
    private final ActionListener timeSlotComboBoxListener = e -> {
        // Prevent the listener to be activated during a day change
        if (view.getTimeSlotComboBox().getSelectedIndex() == 0) {
            view.getAddButton().setVisible(true);
            view.getModifyButton().setVisible(false);
            view.getDeleteButton().setVisible(false);

            view.getStartTimeTextField().setText("");
            view.getEndTimeTextField().setText("");
        } else {
            view.getAddButton().setVisible(false);
            view.getModifyButton().setVisible(true);
            view.getDeleteButton().setVisible(true);

            TimeSlot timeSlot = getSelectedTimeSlot();
            view.getStartTimeTextField().setText(timeSlot.getStartTime().toString());
            view.getEndTimeTextField().setText(timeSlot.getEndTime().toString());
        }
    };

    /**
     * Allows to reset the view when changing the selected day.
     */
    private final ActionListener dayComboBoxListener = e -> {
        refreshTimeSlotComboBox();

        view.getTimeSlotComboBox().setSelectedIndex(0);

        view.getStartTimeTextField().setText("");
        view.getEndTimeTextField().setText("");
    };

    /**
     * Adds a TimeSlot to the Database.
     */
    private final ActionListener addButtonListener = e -> {
        String startStr = view.getStartTimeTextField().getText();
        String endStr = view.getEndTimeTextField().getText();
        LocalDate date = getSelectedDay();

        if (!startStr.isEmpty() && !endStr.isEmpty()) {
            LocalTime startTime = null;
            LocalTime endTime = null;

            try {
                startTime = LocalTime.parse(startStr);
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            try {
                endTime = LocalTime.parse(endStr);
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            if (startTime != null && endTime != null) {
                try {
                    // Insert the new TimeSlot to the good position (ascending order)
                    TimeSlot timeSlot = new TimeSlot(roundToNearestQuarterHour(startTime), roundToNearestQuarterHour(endTime));
                    int index = getIndexInsertTimeSlot(timeSlot);
                    employee.getPlanned().addTimeSlotOnDate(date, timeSlot);
                    view.getTimeSlotComboBox().insertItemAt(timeSlot, index);
                    view.getTimeSlotComboBox().setSelectedIndex(index);

                    // Sort in ascending order the list of TimeSlots
                    Collections.sort(employee.getPlanned().getTimeSlotsOfDate(date));
                } catch (IncorrectTimeException incorrectTimeException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "Start time must be before end time.","Error", JOptionPane.ERROR_MESSAGE);
                } catch (TimeSlotOverlapException timeSlotOverlapException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "This new timeslot is overlapping another.", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (ElementNotFoundException ignored) {
                    // Should not happen anyway
                }
            }

        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "Time fields must be filled.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Modifies the selected TimeSlot.
     */
    private final ActionListener modifyButtonListener = e -> {
        String startStr = view.getStartTimeTextField().getText();
        String endStr = view.getEndTimeTextField().getText();
        TimeSlot timeSlot = getSelectedTimeSlot();
        LocalDate date = getSelectedDay();

        if (!startStr.isEmpty() && !endStr.isEmpty()) {
            LocalTime startTime = null;
            LocalTime endTime = null;

            try {
                startTime = LocalTime.parse(startStr);
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            try {
                endTime = LocalTime.parse(endStr);
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            if (startTime != null && endTime != null) {
                try {
                    // Remove the old TimeSlot and insert the modified new one
                    // This is necessary to prevent overlapping
                    int index = view.getTimeSlotComboBox().getSelectedIndex();
                    TimeSlot modifiedTimeSlot = new TimeSlot(roundToNearestQuarterHour(startTime), roundToNearestQuarterHour(endTime));
                    employee.getPlanned().removeTimeSlotOfDate(date, timeSlot);
                    view.getTimeSlotComboBox().removeItemAt(index);
                    employee.getPlanned().addTimeSlotOnDate(date, modifiedTimeSlot);
                    view.getTimeSlotComboBox().insertItemAt(modifiedTimeSlot, index);
                    view.getTimeSlotComboBox().setSelectedIndex(index);

                    // Sort in ascending order the list of TimeSlots
                    Collections.sort(employee.getPlanned().getTimeSlotsOfDate(date));
                } catch (IncorrectTimeException incorrectTimeException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "Start time must be before end time.", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (ElementNotFoundException ignored) {
                    // Should not happen anyway
                } catch (TimeSlotOverlapException timeSlotOverlapException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "This new timeslot is overlapping another.", "Error", JOptionPane.ERROR_MESSAGE);

                    try {
                        // If TimeSlots are overlapping, insert back the old TimeSlot
                        employee.getPlanned().addTimeSlotOnDate(date, timeSlot);

                        int index = getIndexInsertTimeSlot(timeSlot);
                        view.getTimeSlotComboBox().insertItemAt(timeSlot, index);
                        view.getTimeSlotComboBox().setSelectedIndex(index);
                        Collections.sort(employee.getPlanned().getTimeSlotsOfDate(date));
                    } catch (TimeSlotOverlapException | ElementNotFoundException ignored) {
                        // Should not happen anyway
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "Please enter a name.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Deletes the selected TimeSlot.
     */
    private final ActionListener deleteButtonListener = e -> {
        LocalDate date = getSelectedDay();
        TimeSlot timeSlot = getSelectedTimeSlot();

        int result = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure that you want to delete this timeslot ?",
                "Confirm deletion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);

        if (result == JOptionPane.OK_OPTION) {
            try {
                employee.getPlanned().removeTimeSlotOfDate(date, timeSlot);

                int index = view.getTimeSlotComboBox().getSelectedIndex();
                view.getTimeSlotComboBox().removeItemAt(index);
                view.getTimeSlotComboBox().setSelectedIndex(0);
            } catch (ElementNotFoundException ignored) {
                // Should not happen anyway
            }
        }
    };

    /**
     * When pressing enter, calls add or modify depending on the context.
     */
    private final Action addOrModifyKeyListener = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            if (view.getAddButton().isVisible()) {
                addButtonListener.actionPerformed(e);
            } else {
                modifyButtonListener.actionPerformed(e);
            }
        }
    };

    public PlanningManagementFrameController() {
        view = new PlanningManagementFrameView("Planning Manager");

        initView();
    }

    public void setListener(PlanningManagementListener listener) {
        this.listener = listener;
    }

    public void enableFrame(Employee employee) {
        this.employee = employee;
        view.getMainFrame().setVisible(true);
        reloadPlanningFrame();
    }

    public void disableFrame() {
        view.getMainFrame().setVisible(false);
    }

    /**
     * Initializes the view
     */
    private void initView() {
        // Label Configuration
        view.getStartTimeLabel().setText("Start : ");
        view.getEndTimeLabel().setText("End : ");
        view.getTimeSlotLabel().setText("Planning :");

        // Day selection
        view.getDayLabel().setText("Day : ");
        view.getDayComboBox().addItem(new ElementCBox(1, "Monday"));
        view.getDayComboBox().addItem(new ElementCBox(2, "Tuesday"));
        view.getDayComboBox().addItem(new ElementCBox(3, "Wednesday"));
        view.getDayComboBox().addItem(new ElementCBox(4, "Thursday"));
        view.getDayComboBox().addItem(new ElementCBox(5, "Friday"));
        view.getDayComboBox().addItem(new ElementCBox(6, "Saturday"));
        view.getDayComboBox().addItem(new ElementCBox(7, "Sunday"));
        view.getDayComboBox().setSelectedIndex(0);
        view.getDayComboBox().addActionListener(dayComboBoxListener);

        view.getTimeSlotComboBox().addActionListener(timeSlotComboBoxListener);

        // Buttons
        view.getAddButton().setText("Add");
        view.getAddButton().addActionListener(addButtonListener);

        view.getModifyButton().setText("Modify");
        view.getModifyButton().setVisible(false);
        view.getModifyButton().addActionListener(modifyButtonListener);

        view.getDeleteButton().setText("Delete");
        view.getDeleteButton().setVisible(false);
        view.getDeleteButton().addActionListener(deleteButtonListener);

        view.getMainPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "addOrModifyKeyListener");
        view.getMainPanel().getActionMap().put("addOrModifyKeyListener", addOrModifyKeyListener);

        view.getMainFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        view.getMainFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                listener.closePlanningManagement();
            }
        });
    }

    /**
     * Returns a LocalDate object corresponding to the given day (1 = Monday, 2 = Tuesday, etc.).
     * @param idDay Id of the selected day
     */
    private LocalDate dayToLocalDate(int idDay) {
        if (idDay < 1 || idDay > 7) {
            throw new IllegalArgumentException("idDay doesn't correspond to a day of the week.");
        } else {
            // Month starting with Monday
            return LocalDate.of(2018, 1, idDay);
        }
    }

    /**
     * Gets the LocalDate object associated with the selected day combobox item.
     */
    private LocalDate getSelectedDay() {
        ElementCBox elementSelected = (ElementCBox) view.getDayComboBox().getSelectedItem();
        if (elementSelected == null) {
            // Should not happen anyway
            throw new NullPointerException("A ElementCBox of DayComboBox is null.");
        }
        return dayToLocalDate((int) elementSelected.getId());
    }

    /**
     * Gets the TimeSlot of the selected TimeSlot combobox item.
     */
    private TimeSlot getSelectedTimeSlot() {
        TimeSlot timeSlot = (TimeSlot) view.getTimeSlotComboBox().getSelectedItem();

        if (timeSlot == null) {
            // Should not happen anyway
            throw new NullPointerException("A TimeSlot of TimeSlotComboBox is null.");
        } else {
            return timeSlot;
        }
    }

    /**
     * Return the index for inserting a TimeSlot in TimeSlotComboBox in ascending order.
     * @param insertSlot
     * @return int
     */
    private int getIndexInsertTimeSlot(TimeSlot insertSlot) {
        for (int index = 1; index < view.getTimeSlotComboBox().getItemCount(); index++) {
            TimeSlot otherSlot = (TimeSlot) view.getTimeSlotComboBox().getItemAt(index);
            if (insertSlot.compareTo(otherSlot) < 0) {
                return index;
            }
        }
        return view.getTimeSlotComboBox().getItemCount();
    }

    /**
     * Refreshes the TimeSlot combobox.
     */
    private void refreshTimeSlotComboBox() {
        for (ActionListener al : view.getTimeSlotComboBox().getActionListeners()) {
            view.getTimeSlotComboBox().removeActionListener(timeSlotComboBoxListener);
        }

        view.getTimeSlotComboBox().removeAllItems();
        view.getTimeSlotComboBox().addItem("+ Add timeslot");

        ArrayList<TimeSlot> timeSlotArrayList = new ArrayList<>();
        try {
            ElementCBox elementSelected = (ElementCBox) view.getDayComboBox().getSelectedItem();
            timeSlotArrayList = employee.getPlanned().getTimeSlotsOfDate(dayToLocalDate((int) elementSelected.getId()));
        } catch (ElementNotFoundException elementNotFoundException) {
            System.out.println("No time slots for the selected day. Not a problem.");
        }
        for (TimeSlot timeSlot : timeSlotArrayList) {
            view.getTimeSlotComboBox().addItem(timeSlot);
        }

        view.getTimeSlotComboBox().addActionListener(timeSlotComboBoxListener);
    }

    /**
     * Reloads the planning frame
     */
    public void reloadPlanningFrame() {
        refreshTimeSlotComboBox();
        view.getStartTimeTextField().setText("");
        view.getEndTimeTextField().setText("");
    }
}
