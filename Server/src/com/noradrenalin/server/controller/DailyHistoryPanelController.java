package com.noradrenalin.server.controller;

import com.noradrenalin.server.controller.listeners.TimeSlotHistoryListener;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import com.noradrenalin.server.view.DailyHistoryPanelView;
import com.noradrenalin.server.view.ElementCBox;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;

/**
 * Controls the DailyHistory's logic. Uses the model and view.
 */
public class DailyHistoryPanelController {
    private DailyHistoryPanelView view;
    private Database database;
    private TimeSlotHistoryListener listener;

    private int selectedIndex = 0;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Reload all the Panel if filterComboBox change.
     */
    private final ActionListener filterComboBoxListener = e -> reloadDailyHistoryPanel();

    /**
     * Checks if a table reload is needed.
     */
    private final ActionListener empOrDeptComboBoxListener = e -> {
        if (view.getEmpOrDeptComboBox().getSelectedIndex() != selectedIndex && view.getEmpOrDeptComboBox().getItemCount() != 0) {
            selectedIndex = view.getEmpOrDeptComboBox().getSelectedIndex();
            reloadDailyHistoryTable();
        }
    };

    /**
     * Opens the EditTimeSlotFrame.
     */
    private final ActionListener addTimeSlotButtonListener = e -> listener.openEditTimeSlotFrame(LocalDate.now());

    public DailyHistoryPanelController(DailyHistoryPanelView view, Database database) {
        this.view = view;
        this.database = database;

        initView();
    }

    /**
     * Reloads everything on the page.
     */
    public void reloadDailyHistoryPanel() {
        view.getEmpOrDeptComboBox().removeActionListener(empOrDeptComboBoxListener);

        selectedIndex = 0;
        reloadEmpOrDeptComboBox();
        reloadDailyHistoryTable();

        view.getEmpOrDeptComboBox().addActionListener(empOrDeptComboBoxListener);
    }

    public void setListener(TimeSlotHistoryListener listener) {
        this.listener = listener;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        view.getAddTimeSlotButton().setText("Add TimeSlot");
        view.getAddTimeSlotButton().addActionListener(addTimeSlotButtonListener);

        view.getFilterLabel().setText("Filter by : ");
        view.getFilterComboBox().addItem(new ElementCBox(1, "Employee"));
        view.getFilterComboBox().addItem(new ElementCBox(2, "Department"));
        view.getFilterComboBox().addActionListener(filterComboBoxListener);

        String[] headers = {"Name", "Arriving time", "Departure time"};
        setDailyHistoryTableHeader(headers);

        // Allows the TimeSlots with a problem to be highlighted
        view.getHistoryTable().setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                setHorizontalAlignment(JLabel.CENTER);

                if (value.equals("null")) {
                    setValue("");
                    c.setBackground(Color.LIGHT_GRAY);
                    Font f = c.getFont();
                    c.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
                } else {
                    c.setBackground(Color.WHITE);
                    c.setForeground(Color.BLACK);
                }

                if (column == 1 || column == 2) {
                    try {
                        Employee emp = (Employee) view.getHistoryModel().getDataVector().elementAt(row).elementAt(0);
                        LocalDate date = LocalDate.now();

                        LocalTime startTime = LocalTime.parse((String) view.getHistoryModel().getDataVector().elementAt(row).elementAt(1));
                        LocalTime endTime = LocalTime.parse((String) view.getHistoryModel().getDataVector().elementAt(row).elementAt(2));

                        TimeSlot timeSlot = new TimeSlot(startTime, endTime);

                        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> stateMap =
                                HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(emp, date, timeSlot);

                        if (column == 1) {
                            if (stateMap.getKey() == HistoryPanelUtils.EmployeeState.EarlyStart) {
                                c.setBackground(new Color(0, 129, 72));
                                c.setForeground(Color.WHITE);
                            } else if (stateMap.getKey() == HistoryPanelUtils.EmployeeState.LateStart) {
                                c.setBackground(new Color(226, 50, 33, 255));
                                c.setForeground(Color.WHITE);
                            }
                        }

                        if (column == 2) {
                            if (stateMap.getValue() == HistoryPanelUtils.EmployeeState.EarlyEnd) {
                                c.setBackground(new Color(226, 50, 33, 255));
                                c.setForeground(Color.WHITE);
                            } else if (stateMap.getValue() == HistoryPanelUtils.EmployeeState.LateEnd) {
                                c.setBackground(new Color(0, 129, 72));
                                c.setForeground(Color.WHITE);
                            }
                        }
                    } catch (DateTimeParseException | IncorrectTimeException | IllegalArgumentException ignored) {}
                }

                if (hasFocus) {
                    Vector<Object> vector = new Vector<Object>(view.getHistoryModel().getDataVector().elementAt(row));
                    vector.insertElementAt(LocalDate.now().format(formatter), 1);
                    listener.openEditTimeSlotFrame(vector);
                }

                return c;
            }
        });

        reloadDailyHistoryPanel();
    }

    /**
     * Sets the headers for the daily history table.
     * @param headers The list of headers
     */
    private void setDailyHistoryTableHeader(String[] headers) {
        for (String header : headers) {
            view.getHistoryModel().addColumn(header);
        }
    }

    /**
     * Adds data into the daily history table.
     * @param data The data to insert into the table
     */
    private void addDataToDailyHistoryTable(Object[] data) {
        if (data.length != view.getHistoryModel().getColumnCount()) {
            System.out.println("Data size incorrect");
        } else {
            view.getHistoryModel().addRow(data);
        }
    }

    /**
     * Adds all selected employee's timeslots into the table.
     * @param employee Employee to add
     */
    private void addDataOfEmployee(Employee employee) {
        LocalDate today = LocalDate.now();
        try {
            ArrayList<TimeSlot> timeSlots = employee.getActual().getTimeSlotsOfDate(today);
            Collections.sort(timeSlots);

            for (int i = timeSlots.size() - 1; i >=0; i--) {
                TimeSlot timeSlot = timeSlots.get(i);
                Object[] data = {employee, String.valueOf(timeSlot.getStartTime()), String.valueOf(timeSlot.getEndTime())};
                addDataToDailyHistoryTable(data);
            }
        } catch (ElementNotFoundException elementNotFoundException) {
            // This employee doesn't have timeslots for this date, this isn't a problem
            System.out.println(employee + " does not have any timeslots for " + today.format(formatter));
        }
    }

    /**
     * Adds all selected department's timeslots into the table.
     * @param department department to add
     */
    private void addDataOfDepartment(Department department) {
        for (Employee employee : database.getEmployees()) {
            if (employee.getDepartment().equals(department))
            {
                addDataOfEmployee(employee);
            }
        }
    }

    /**
     * Reloads the history table.
     */
    private void reloadDailyHistoryTable() {
        view.getHistoryModel().setRowCount(0);

        ElementCBox filterSelected = (ElementCBox) view.getFilterComboBox().getSelectedItem();
        assert filterSelected != null;

        if (selectedIndex == 0) {
            for (Employee employee : database.getEmployees()) {
                addDataOfEmployee(employee);
            }
        } else {
            // If the employee filter is selected
            if (filterSelected.getId() == 1) {
                Employee employee = (Employee) view.getEmpOrDeptComboBox().getSelectedItem();
                assert employee != null;
                addDataOfEmployee(employee);
            } else if (filterSelected.getId() == 2) {
                Department department = (Department) view.getEmpOrDeptComboBox().getSelectedItem();
                assert department != null;
                addDataOfDepartment(department);
            }
        }

        if (view.getHistoryModel().getRowCount() == 0 && view.getEmpOrDeptComboBox().getSelectedIndex() != 0) {
            JOptionPane.showMessageDialog(view.getMainPanel(), "Nothing found for this employee or department.", "No entry found", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Reloads the employees list.
     */
    private void reloadEmpOrDeptComboBox() {
        JComboBox<Object> empOrDeptSelected = view.getEmpOrDeptComboBox();
        ElementCBox filterSelected = (ElementCBox) view.getFilterComboBox().getSelectedItem();
        assert filterSelected != null;

        for (ActionListener al : empOrDeptSelected.getActionListeners()) {
            empOrDeptSelected.removeActionListener(al);
        }
        empOrDeptSelected.removeAllItems();

        if (filterSelected.getId() == 1) {
            empOrDeptSelected.addItem("All employees");
            for (Employee employee : database.getEmployees()) {
                empOrDeptSelected.addItem(employee);
            }
        } else if (filterSelected.getId() == 2) {
            empOrDeptSelected.addItem("All departments");
            for (Department department : database.getDepartments()) {
                empOrDeptSelected.addItem(department);
            }
        }
        view.getAddTimeSlotButton().setEnabled(empOrDeptSelected.getItemCount() != 0);

        view.getEmpOrDeptComboBox().addActionListener(empOrDeptComboBoxListener);
    }
}
