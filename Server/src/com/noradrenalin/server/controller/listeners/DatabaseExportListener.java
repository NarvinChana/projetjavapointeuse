package com.noradrenalin.server.controller.listeners;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.EventListener;

/**
 * Interface for classes listening to database exportation.
 */
public interface DatabaseExportListener extends EventListener {
    void databaseExport(BufferedWriter writer) throws IOException;
}
