package com.noradrenalin.server.controller.listeners;

import java.time.LocalDate;
import java.util.Vector;

/**
 * Interface for classes listening to History panels.
 */
public interface TimeSlotHistoryListener {
    /**
     * Opens the EditTimeSlotFrame in adding mode.
     * @param date Is today if open from DailyHistory and is null is opened from TotalHistory
     */
    void openEditTimeSlotFrame(LocalDate date);

    /**
     * Opens the EditTimeSlotFrame in editing mode.
     * @param vector Is a line of data is a history table
     */
    void openEditTimeSlotFrame(Vector<Object> vector);
}
