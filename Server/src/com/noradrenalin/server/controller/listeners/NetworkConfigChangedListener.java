package com.noradrenalin.server.controller.listeners;

import com.noradrenalin.common.network.NetworkConfig;

import java.util.EventListener;

/**
 * Interface for classes listening for a network config change.
 */
public interface NetworkConfigChangedListener extends EventListener {
    void networkConfigChanged(NetworkConfig netConf);
}
