package com.noradrenalin.server.controller.listeners;

import com.noradrenalin.server.model.employee.Employee;

/**
 * Interface for classes listening to the employee management panel.
 */
public interface EmployeeManagementListener {
    void openPlanningManagement(Employee employee);
}
