package com.noradrenalin.server.controller.listeners;

/**
 * Interface for classes listening to the planning management frame.
 */
public interface PlanningManagementListener {
    void closePlanningManagement();
}
