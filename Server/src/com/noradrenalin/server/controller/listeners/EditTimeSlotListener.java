package com.noradrenalin.server.controller.listeners;

/**
 * Interface for classes listening to EditTimeSlotFrameController actions.
 */
public interface EditTimeSlotListener {
    void closeEditTimeSlotFrame();
}
