package com.noradrenalin.server.controller.listeners;

import com.noradrenalin.server.model.database.Database;

import java.util.EventListener;

/**
 * Interface for classes listening for modifications of a variable database.
 */
public interface DatabaseImportListener extends EventListener {
    void databaseChanged(Database database);
}
