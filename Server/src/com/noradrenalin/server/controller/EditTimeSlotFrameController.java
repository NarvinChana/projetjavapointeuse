package com.noradrenalin.server.controller;

import com.noradrenalin.server.controller.listeners.EditTimeSlotListener;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import com.noradrenalin.server.view.EditTimeSlotFrameView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Vector;

import static com.noradrenalin.common.util.time.TimeOperations.roundToNearestQuarterHour;

/**
 * Controls the EditTimeSlot's logic. Uses the model and view.
 */
public class EditTimeSlotFrameController {
    private EditTimeSlotFrameView view;
    private Database database;
    private EditTimeSlotListener listener;

    private Employee currentEmployee;
    private LocalDate currentDate;
    private TimeSlot currentTimeSlot;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * Adds a new TimeSlot to the database.
     */
    private final ActionListener addButtonListener = e -> {
        if (!view.getStartTimeTextField().getText().isEmpty() && !view.getEndTimeTextField().getText().isEmpty() && !view.getDateTextField().getText().isEmpty()) {
            LocalDate date = null;
            LocalTime startTime = null;
            LocalTime endTime = null;

            try {
                date = LocalDate.parse(view.getDateTextField().getText(), formatter);
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Date field must be valid.","Error", JOptionPane.ERROR_MESSAGE);
            }

            try {
                startTime = LocalTime.parse(view.getStartTimeTextField().getText());
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.","Error", JOptionPane.ERROR_MESSAGE);
            }

            try {
                endTime = LocalTime.parse(view.getEndTimeTextField().getText());
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.","Error", JOptionPane.ERROR_MESSAGE);
            }

            if (date != null & startTime != null && endTime != null) {
                try {
                    TimeSlot timeSlot = new TimeSlot(roundToNearestQuarterHour(startTime), roundToNearestQuarterHour(endTime));
                    Employee employee = (Employee) view.getEmployeeComboBox().getSelectedItem();

                    employee.getActual().addTimeSlotOnDate(date, timeSlot);

                    listener.closeEditTimeSlotFrame();
                } catch (IncorrectTimeException incorrectTimeException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "Start time must be before end time.", "Error", JOptionPane.ERROR_MESSAGE);
                } catch (TimeSlotOverlapException timeSlotOverlapException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "This new timeslot is overlapping another one.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(view.getMainFrame(), "All fields must be filled.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Modifies a TimeSlot.
     */
    private final ActionListener modifyButtonListener = e -> {
        if (!view.getStartTimeTextField().getText().isEmpty() && !view.getEndTimeTextField().getText().isEmpty()) {
            LocalTime startTime = null;
            LocalTime endTime = null;

            try {
                startTime = LocalTime.parse(view.getStartTimeTextField().getText());
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.","Error", JOptionPane.ERROR_MESSAGE);
            }

            try {
                endTime = LocalTime.parse(view.getEndTimeTextField().getText());
            } catch (DateTimeParseException exception) {
                JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be valid.","Error", JOptionPane.ERROR_MESSAGE);
            }

            if (startTime != null && endTime != null) {
                TimeSlot toInsert = null;
                try {
                    toInsert = new TimeSlot(roundToNearestQuarterHour(startTime), roundToNearestQuarterHour(endTime));

                } catch (IncorrectTimeException incorrectTimeException) {
                    JOptionPane.showMessageDialog(view.getMainFrame(), "Start time must be before end time.","Error", JOptionPane.ERROR_MESSAGE);
                }

                if (toInsert != null) {
                    try {
                        currentEmployee.getActual().removeTimeSlotOfDate(currentDate, currentTimeSlot);
                        currentEmployee.getActual().addTimeSlotOnDate(currentDate, toInsert);
                        listener.closeEditTimeSlotFrame();
                    } catch (TimeSlotOverlapException timeSlotOverlapException) {
                        try {
                            currentEmployee.getActual().addTimeSlotOnDate(currentDate, currentTimeSlot);
                        } catch (TimeSlotOverlapException ignored) {
                            //Should not happen
                        }
                        JOptionPane.showMessageDialog(view.getMainFrame(), "The TimeSlot is overlapping another one.","Error", JOptionPane.ERROR_MESSAGE);
                    } catch (ElementNotFoundException ignored) {
                        //Should not happen
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(view.getMainFrame(), "Time fields must be filled.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Removes a TimeSlot in the database.
     */
    private final ActionListener deleteButtonListener = e -> {
        int result = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure that you want to delete this timeslot ?","Confirm deletion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);

        if (result == JOptionPane.OK_OPTION) {
            try {
                currentEmployee.getActual().removeTimeSlotOfDate(currentDate, currentTimeSlot);
            } catch (ElementNotFoundException ignored) {
                // Should not happen anyway
            }
            listener.closeEditTimeSlotFrame();
        }
    };

    /**
     * When pressing enter, calls add or modify depending on the context.
     */
    private final Action addOrModifyKeyListener = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            if (view.getAddButton().isVisible()) {
                addButtonListener.actionPerformed(e);
            } else {
                modifyButtonListener.actionPerformed(e);
            }
        }
    };

    /**
     * This class controls how the EditTimeSlotFrame behaves.
     * @param database The server database
     */
    public EditTimeSlotFrameController(Database database) {
        view = new EditTimeSlotFrameView("TimeSlots Editor");
        this.database = database;

        initView();
    }

    public void setListener(EditTimeSlotListener listener) {
        this.listener = listener;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * Enables the frame to add data.
     */
    public void enableFrameAdding(LocalDate date) {
        view.getMainFrame().setVisible(true);

        // Buttons
        view.getAddButton().setVisible(true);
        view.getModifyButton().setVisible(false);
        view.getDeleteButton().setVisible(false);

        // Employee & date
        view.getEmployeeInfoLabel().setText("Adding a TimeSlot");
        view.getDateInfoLabel().setVisible(false);
        view.getEmployeeLabel().setVisible(true);
        view.getEmployeeComboBox().setVisible(true);
        view.getDateLabel().setVisible(true);
        view.getDateTextField().setVisible(true);

        if (date == null) {
            // This case is in TotalHistory
            view.getDateTextField().setText("");
            view.getDateTextField().setEnabled(true);
        } else {
            // This case comes from DailyHistory (date is today)
            view.getDateTextField().setText(date.format(formatter));
            view.getDateTextField().setEnabled(false);
        }

        reloadEmployeeComboBox();

        currentEmployee = null;
        currentDate = null;
        currentTimeSlot = null;

        view.getStartTimeTextField().setText("");
        view.getEndTimeTextField().setText("");
    }

    /**
     * Enables the frame for editing and resets it with the given data.
     */
    public void enableFrameEditing(Vector<Object> vector) {
        view.getMainFrame().setVisible(true);

        // Buttons
        view.getAddButton().setVisible(false);
        view.getModifyButton().setVisible(true);
        view.getDeleteButton().setVisible(true);

        // Employee & date
        view.getDateInfoLabel().setVisible(true);
        view.getEmployeeLabel().setVisible(false);
        view.getEmployeeComboBox().setVisible(false);
        view.getDateLabel().setVisible(false);
        view.getDateTextField().setVisible(false);

        Employee employee = (Employee) vector.elementAt(0);
        currentEmployee = employee;
        view.getEmployeeInfoLabel().setText(employee.toString());

        LocalDate date = LocalDate.parse((String) vector.elementAt(1), formatter);
        currentDate = date;
        view.getDateInfoLabel().setText(date.format(formatter));

        LocalTime startTime = null;
        LocalTime endTime = null;

        try {
            startTime = LocalTime.parse((String) vector.elementAt(2));
            view.getStartTimeTextField().setText(startTime.toString());
        } catch (DateTimeParseException e) {
            view.getStartTimeTextField().setText("");
        }

        try {
            endTime = LocalTime.parse((String) vector.elementAt(3));
            view.getEndTimeTextField().setText(endTime.toString());
        } catch (DateTimeParseException e) {
            view.getEndTimeTextField().setText("");
        }

        try {
            currentTimeSlot = currentEmployee.getActual().findTimeSlot(currentDate, startTime, endTime);
        } catch (ElementNotFoundException ignored) {
            // Shouldn't happen anyway
        }
    }

    /**
     * Disables the frame.
     */
    public void disableFrame() {
        view.getMainFrame().setVisible(false);
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        // Labels
        view.getEmployeeLabel().setText("Employee : ");
        view.getDateLabel().setText("<html>Date : <br>(dd/mm/yyyy)</html>");
        view.getStartTimeLabel().setText("<html>Start time : <br>(hh:mm)</html)");
        view.getEndTimeLabel().setText("<html>End time : <br>(hh:mm)</html)");

        Font font = new Font("Dialog", Font.BOLD, 14);
        view.getEmployeeInfoLabel().setFont(font);
        view.getDateInfoLabel().setFont(font);

        // Button
        view.getAddButton().setText("Add");
        view.getAddButton().addActionListener(addButtonListener);
        view.getModifyButton().setText("Modify");
        view.getModifyButton().addActionListener(modifyButtonListener);
        view.getDeleteButton().setText("Delete");
        view.getDeleteButton().addActionListener(deleteButtonListener);

        view.getMainPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "addOrModifyKeyListener");
        view.getMainPanel().getActionMap().put("addOrModifyKeyListener", addOrModifyKeyListener);

        view.getMainFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        view.getMainFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                listener.closeEditTimeSlotFrame();
            }
        });
    }

    /**
     * Reloads the employees list.
     */
    private void reloadEmployeeComboBox() {
        JComboBox<Employee> employeeCB = view.getEmployeeComboBox();
        employeeCB.removeAllItems();

        for (Employee employee : database.getEmployees()) {
            employeeCB.addItem(employee);
        }

        if (employeeCB.getItemCount() != 0) {
            employeeCB.setSelectedIndex(0);
        }
    }
}
