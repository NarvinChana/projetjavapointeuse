package com.noradrenalin.server.controller;

import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Map;

import static java.time.temporal.ChronoUnit.MINUTES;

public class HistoryPanelUtils {
    public enum EmployeeState {
        LateStart,
        EarlyStart,
        LateEnd,
        EarlyEnd,
        NotLate
    }

    /**
     * Checks if, without any uncertainty, emp is late.
     * @param emp Employee to be checked
     * @param day Day of timeslot
     * @param time Timeslot to look at
     * @return SimpleEntry of the employee's tardiness at the start and end of their shift
     */
    public static AbstractMap.SimpleEntry<EmployeeState, EmployeeState> checkIfEmployeeIsLateOrEarly(Employee emp, LocalDate day, TimeSlot time) {
        final int ARRIVAL_DEPARTURE_MARGIN = 15;

        // Checks if date exists in the planning
        Map.Entry<LocalDate, ArrayList<TimeSlot>> checkDate = null;
        for (Map.Entry<LocalDate, ArrayList<TimeSlot>> d : emp.getPlanned().getTimeSlots().entrySet()) {
            if (d.getKey().getDayOfWeek().equals(day.getDayOfWeek())) {
                checkDate = d;
                break;
            }
        }

        if (checkDate == null) {
            throw new IllegalArgumentException("The specified day does not exist.");
        }

        long closestDelay = Long.MAX_VALUE;
        TimeSlot closestTimeSlot = null;

        EmployeeState state = EmployeeState.NotLate;

        // Looks in all planned TimeSlots to get the closest TimeSlot to the parameter time
        for (TimeSlot t : checkDate.getValue()) {
            if (time.getStartTime() != null && time.getEndTime() != null && t.getStartTime() != null && t.getEndTime() != null) {
                long localDelay = 0;
                localDelay += Math.abs(MINUTES.between(t.getStartTime(), time.getStartTime()));
                localDelay += Math.abs(MINUTES.between(t.getEndTime(), time.getEndTime()));

                if (localDelay < closestDelay) {
                    closestTimeSlot = t;
                    closestDelay = localDelay;
                }
            }
        }
        if (closestTimeSlot == null) {
            throw new IllegalArgumentException("There is no timeslot that corresponds to the specified work period.");
        }

        // Checks if employee is late/early
        long startDiff = MINUTES.between(closestTimeSlot.getStartTime(), time.getStartTime());
        long endDiff = MINUTES.between(closestTimeSlot.getEndTime(), time.getEndTime());
        EmployeeState startState = EmployeeState.NotLate;
        EmployeeState endState = EmployeeState.NotLate;

        if (Math.abs(startDiff) >= ARRIVAL_DEPARTURE_MARGIN) {
            if (startDiff < 0) {
                startState = EmployeeState.EarlyStart;
            } else {
                startState = EmployeeState.LateStart;
            }
        }
        if (Math.abs(endDiff) >= ARRIVAL_DEPARTURE_MARGIN){
            if (endDiff < 0) {
                endState = EmployeeState.EarlyEnd;
            } else {
                endState = EmployeeState.LateEnd;
            }
        }

        return new AbstractMap.SimpleEntry<>(startState, endState);
    }
}
