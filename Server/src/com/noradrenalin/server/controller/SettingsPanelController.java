package com.noradrenalin.server.controller;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.server.controller.listeners.DatabaseExportListener;
import com.noradrenalin.server.controller.listeners.DatabaseImportListener;
import com.noradrenalin.server.controller.listeners.NetworkConfigChangedListener;
import com.noradrenalin.server.util.csv.CSVImportDatabase;
import com.noradrenalin.server.util.csv.FileParseException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.view.SettingsPanelView;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.io.*;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static java.lang.Integer.parseInt;

/**
 * Controls the Settings Panel's logic. Uses the model and view.
 */
public class SettingsPanelController {
    private SettingsPanelView view;

    private List<NetworkConfig> clientWhitelist;
    private List<NetworkConfig> clientBlacklist;
    private NetworkConfig serverPort;
    private NetworkConfigChangedListener netConfChanged;
    private DatabaseImportListener databaseImportListener;
    private DatabaseExportListener databaseExportListener;

    /**
     * Listener that applies the change of the port number if it is valid.
     */
    private final ActionListener portChangeListener = e -> {
        if (!serverPort.getPortAsString().equals(view.getPortTextField().getText())) {
            NetworkConfig n = null;
            try {
                n = new NetworkConfig();
            } catch (UnknownHostException ignored) {
                // Can't happen as only default constructors with valid data are used.
            }
            assert n != null;
            try {
                n.setPort(parseInt(view.getPortTextField().getText()));
                serverPort = n;
                netConfChanged.networkConfigChanged(n);
            } catch (IllegalArgumentException exception) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "The port number entered is not a valid port. Please try again.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    };

    /**
     * Import listener button that opens the dialog box for selecting.
     */
    private final ActionListener importDatabaseButtonListener = e -> {
        final JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(view.getMainPanel());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            Database db;
            try {
                db = CSVImportDatabase.parseCSV(file);
                if (databaseImportListener == null) {
                    JOptionPane.showMessageDialog(view.getMainPanel(), "There was an error importing the database from the file.", "Importation Error", JOptionPane.ERROR_MESSAGE);
                }
                databaseImportListener.databaseChanged(db);
            } catch(FileParseException fileParseException) {
                String errorMessage = "There was an error importing the database from the file.";
                String errorDetail = "Reading of " + fileParseException.getPartEnum() + " at line " + fileParseException.getErrorLine() + " : \"" + fileParseException.getErrorString() + "\".";
                JOptionPane.showMessageDialog(view.getMainPanel(), errorMessage + "\n" + errorDetail, "Importation Error", JOptionPane.ERROR_MESSAGE);
            } catch(IOException ioException) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "There was a problem opening the file.", "Importation Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    };

    /**
     * Listener for the export database button.
     */
    private final ActionListener exportDatabaseButtonListener = e -> {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Specify a file to save");

        int userSelection = fileChooser.showSaveDialog(view.getMainPanel());

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileToSave), StandardCharsets.UTF_8));
                databaseExportListener.databaseExport(writer);
            } catch (IOException ioException) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "There was an error exporting the database into the file.\n" + ioException.getLocalizedMessage(), "Importation Error", JOptionPane.ERROR_MESSAGE);
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException ioException) {
                        System.out.println("There was an error while closing the BufferedWriter after exporting the database.");
                    }
                }
            }
        }
    };

    /**
     * Listener that will clear the clientBlacklist when the user clicks on the clearBlacklistButton.
     */
    private final ActionListener clearBlacklistListener = e -> {
        int choice = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure you want to clear the blacklist ?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (choice == 0) {
            // Clear blacklist
            if (clientBlacklist != null) {
                clientBlacklist.clear();
            }
            JOptionPane.showMessageDialog(view.getMainPanel(), "The blacklist has been cleared.", "Confirmation", JOptionPane.INFORMATION_MESSAGE);
        }
        // Else do nothing
    };

    /**
     * Listener that removes a selected client from the client list.
     */
    private final ActionListener removeClientListener = e -> {
        if (view.getClientTable().getRowCount() > 0) {
            // Check number of rows
            int row = view.getClientTable().getSelectedRow();
            if (row == -1) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "No client network data selected.", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                int choice = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure you want to remove the client from the database ?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                // User replies OK
                if (choice == 0) {
                    synchronized (this) {
                        NetworkConfig element = clientWhitelist.get(view.getClientTable().convertRowIndexToModel(row));
                        clientWhitelist.remove(element);
                    }
                    reloadClientData();
                }
            }
        }
    };

    /**
     * Shows a confirmation popup before deleting the database.
     */
    private final ActionListener resetButtonListener = e -> {
        int result = JOptionPane.showConfirmDialog(view.getMainPanel(),
                "Are you sure you want to erase the database ?",
                "Reset database",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            File file = new File(MainTabFrameController.SERVER_DATABASE_PATH);
            file.delete();
            databaseImportListener.databaseChanged(new Database());
        }
    };

    public SettingsPanelController(SettingsPanelView view, NetworkConfig netConf, List<NetworkConfig> clientWhitelist, List<NetworkConfig> clientBlacklist) {
        this.view = view;
        this.clientWhitelist = clientWhitelist;
        this.clientBlacklist = clientBlacklist;
        this.serverPort = netConf;

        initView();
    }

    public void setNetConfChanged(NetworkConfigChangedListener netConfChanged) {
        this.netConfChanged = netConfChanged;
    }

    public void setDatabaseImportListener(DatabaseImportListener databaseImportListener) {
        this.databaseImportListener = databaseImportListener;
    }

    public void setDatabaseExportListener(DatabaseExportListener databaseExportListener) {
        this.databaseExportListener = databaseExportListener;
    }

    /**
     * Reloads the data in the settings window.
     */
    public void reloadClientData() {
        view.getModel().refresh(clientWhitelist);
        view.getClientTable().setModel(view.getModel());
        view.getPortTextField().setText(serverPort.getPortAsString());
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        view.getClientLabel().setText("Client List : ");
        view.getPortLabel().setText("Listening Port : ");
        view.getResetButton().setText("Reset Database");
        view.getPortChangeButton().setText("Change Listening Port");

        view.getImportDatabaseButton().setText("Import Database from File");
        view.getExportDatabaseButton().setText("Export Database to File");

        view.getRemoveClientButton().setText("Remove Client");
        view.getClearBlacklistButton().setText("Clear Blacklist");

        view.getPortChangeButton().addActionListener(portChangeListener);
        view.getRemoveClientButton().addActionListener(removeClientListener);

        view.getImportDatabaseButton().addActionListener(importDatabaseButtonListener);
        view.getExportDatabaseButton().addActionListener(exportDatabaseButtonListener);

        view.getResetButton().addActionListener(resetButtonListener);
        view.getClearBlacklistButton().addActionListener(clearBlacklistListener);
    }
}
