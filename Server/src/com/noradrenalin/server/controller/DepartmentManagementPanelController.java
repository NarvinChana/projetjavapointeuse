package com.noradrenalin.server.controller;

import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IdAlreadyTakenException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.view.DepartmentManagementPanelView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

/**
 * Controls the DepartmentManagement's logic. Uses the model and view.
 */
public class DepartmentManagementPanelController {
    private DepartmentManagementPanelView view;
    private Database database;

    /**
     * If "+ Add department" is selected, shows addButton and empties the textFields.
     * Otherwise, shows modifyButton and deleteButton and fills the textField with the available data.
     */
    private final ActionListener departmentComboBoxListener = e -> {
        if (view.getDepartmentComboBox().getSelectedIndex() == 0) {
            resetView();
        } else {
            view.getAddButton().setVisible(false);
            view.getModifyButton().setVisible(true);
            view.getDeleteButton().setVisible(true);

            Department department = (Department) view.getDepartmentComboBox().getSelectedItem();
            assert department != null;
            view.getDepNameTextField().setText(department.getName());
        }
    };

    /**
     * Adds a new department to the database.
     */
    private final ActionListener addButtonListener = e -> {
        String name = view.getDepNameTextField().getText();

        if (!name.isEmpty()) {
            Department department = new Department(database.getNewDepartmentId(), name);

            try {
                database.addDepartment(department);
                view.getDepartmentComboBox().addItem(department);
                view.getDepartmentComboBox().setSelectedIndex(view.getDepartmentComboBox().getItemCount() - 1);
            } catch (IdAlreadyTakenException ignored){
                // Can't be triggered anyway
            }
        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "Please enter a name.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Modifies the selected department already existing in the database.
     */
    private final ActionListener modifyButtonListener = e -> {
        String name = view.getDepNameTextField().getText();
        Department department = (Department) view.getDepartmentComboBox().getSelectedItem();

        if (!name.isEmpty() && !name.equals(Objects.requireNonNull(department).getName())) {
            department.setName(name);

            // This selection swap allows the combobox to refresh
            int index = view.getDepartmentComboBox().getSelectedIndex();
            view.getDepartmentComboBox().setSelectedIndex(0);
            view.getDepartmentComboBox().setSelectedIndex(index);
        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "Please enter a valid name.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Deletes the selected department in the database.
     */
    private final ActionListener deleteButtonListener = e -> {
        int result = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure that you want to delete this department ?","Confirm deletion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);

        if (result == JOptionPane.OK_OPTION) {
            try {
                database.removeDepartment((Department) view.getDepartmentComboBox().getSelectedItem());
                int index = view.getDepartmentComboBox().getSelectedIndex();
                view.getDepartmentComboBox().removeItemAt(index);
                view.getDepartmentComboBox().setSelectedIndex(0);
            } catch (ElementNotFoundException ignored) {
                // Can't happen as we're deleting an existing department.
            }
        }
    };

    /**
     * When pressing enter, calls add or modify depending on the context.
     */
    private final Action addOrModifyKeyListener = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            if (view.getDepartmentComboBox().getSelectedIndex() == 0) {
                addButtonListener.actionPerformed(e);
            } else {
                modifyButtonListener.actionPerformed(e);
            }
        }
    };

    public DepartmentManagementPanelController(DepartmentManagementPanelView view, Database database) {
        this.view = view;
        this.database = database;

        initView();
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * Reloads the departmentPanel.
     */
    public void reloadDepartmentPanel() {
        reloadDepartmentComboBox();
        resetView();
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        // Department selection
        view.getDepartmentLabel().setText("Department : ");
        reloadDepartmentComboBox();

        // Buttons
        view.getAddButton().setText("Add");
        view.getAddButton().addActionListener(addButtonListener);

        view.getModifyButton().setText("Modify");
        view.getModifyButton().setVisible(false);
        view.getModifyButton().addActionListener(modifyButtonListener);

        view.getDeleteButton().setText("Delete");
        view.getDeleteButton().setVisible(false);
        view.getDeleteButton().addActionListener(deleteButtonListener);

        // Labels
        view.getDepNameLabel().setText("Name : ");

        view.getMainPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "addOrModifyKeyListener");
        view.getMainPanel().getActionMap().put("addOrModifyKeyListener", addOrModifyKeyListener);
    }

    /**
     * Reloads the departments combobox.
     */
    private void reloadDepartmentComboBox() {
        for (ActionListener al : view.getDepartmentComboBox().getActionListeners()) {
            view.getDepartmentComboBox().removeActionListener(al);
        }

        view.getDepartmentComboBox().removeAllItems();

        view.getDepartmentComboBox().addItem("+ Add department");
        view.getDepartmentComboBox().setSelectedIndex(0);

        for (Department department : database.getDepartments()) {
            view.getDepartmentComboBox().addItem(department);
        }

        view.getDepartmentComboBox().addActionListener(departmentComboBoxListener);
    }

    /**
     * Resets the view.
     */
    private void resetView() {
        view.getAddButton().setVisible(true);
        view.getModifyButton().setVisible(false);
        view.getDeleteButton().setVisible(false);

        view.getDepNameTextField().setText("");
    }
}
