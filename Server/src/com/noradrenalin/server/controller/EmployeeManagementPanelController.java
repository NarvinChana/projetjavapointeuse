package com.noradrenalin.server.controller;

import com.noradrenalin.server.controller.listeners.EmployeeManagementListener;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IdAlreadyTakenException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.view.EmployeeManagementPanelView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Controls the EmployeeManagement's logic. Uses the model and view.
 */
public class EmployeeManagementPanelController {
    private EmployeeManagementPanelView view;
    private Database database;
    private EmployeeManagementListener listener;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     * If "+ Add employee" is selected, shows addButton and empties the textFields.
     * Otherwise, shows modifyButton and deleteButton and fills the textFields with the available data.
     */
    private final ActionListener employeeComboBoxListener = e -> {
        if (view.getEmployeeComboBox().getSelectedIndex() == 0) {
            resetView();
        } else {
            view.getAddButton().setVisible(false);
            view.getModifyButton().setVisible(true);
            view.getDeleteButton().setVisible(true);
            view.getPlanningButton().setVisible(true);

            Employee employee = (Employee) view.getEmployeeComboBox().getSelectedItem();

            view.getFirstNameTextField().setText(employee.getFirstName());
            view.getLastNameTextField().setText(employee.getLastName());
            view.getBirthdateTextField().setText(employee.getBirthDate().format(formatter));
            view.getHourCreditLabel().setText("Minutes Credit : " + employee.getMinutesCredit());

            if (employee.getDepartment() == null) {
                view.getDepartmentComboBox().setSelectedIndex(0);
            } else {
                view.getDepartmentComboBox().setSelectedItem(employee.getDepartment());
            }
        }
    };

    /**
     * Adds a new employee to the database.
     */
    private final ActionListener addButtonListener = e -> {
        String fName = view.getFirstNameTextField().getText();
        String lName = view.getLastNameTextField().getText();
        String date = view.getBirthdateTextField().getText();

        if (!fName.isEmpty() && !lName.isEmpty() && !date.isEmpty() && view.getDepartmentComboBox().getSelectedIndex() != 0) {
            LocalDate birthDate;
            try {
                birthDate = LocalDate.parse(date, formatter);
            } catch (DateTimeParseException exc) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "Date is invalid.","Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            Employee employee;
            try {
                employee = new Employee(database.getNewEmployeeId(), fName, lName, birthDate, (Department) view.getDepartmentComboBox().getSelectedItem());
            } catch (IncorrectTimeException incorrectTimeException) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "Date can't be in the future.","Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                database.addEmployee(employee);
            } catch (IdAlreadyTakenException ignored) {
                // Can't happen as we are creating a new Employee with an ID returned by getNewEmployeeId()
                return;
            }

            view.getEmployeeComboBox().addItem(employee);
            view.getEmployeeComboBox().setSelectedIndex(view.getEmployeeComboBox().getItemCount() - 1);
        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "All fields must be filled.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Modifies the selected employee already existing in the database.
     */
    private final ActionListener modifyButtonListener = e -> {
        String fName = view.getFirstNameTextField().getText();
        String lName = view.getLastNameTextField().getText();
        String date = view.getBirthdateTextField().getText();

        if (!fName.isEmpty() && !lName.isEmpty() && !date.isEmpty()) {
            LocalDate birthDate = null;
            try {
                birthDate = LocalDate.parse(date, formatter);
            } catch (DateTimeParseException exc) {
                JOptionPane.showMessageDialog(view.getMainPanel(), "Date is invalid.", "Error", JOptionPane.ERROR_MESSAGE);
            }

            if (birthDate != null) {
                Employee employee = (Employee) view.getEmployeeComboBox().getSelectedItem();

                try {
                    employee.setBirthDate(birthDate);
                    employee.setFirstName(fName);
                    employee.setLastName(lName);
                    employee.setDepartment((Department) view.getDepartmentComboBox().getSelectedItem());

                    int index = view.getEmployeeComboBox().getSelectedIndex();
                    view.getEmployeeComboBox().setSelectedIndex(0);
                    view.getEmployeeComboBox().setSelectedIndex(index);
                } catch (IncorrectTimeException incorrectTimeException) {
                    JOptionPane.showMessageDialog(view.getMainPanel(), "Date can't be in the future.","Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(view.getMainPanel(), "All fields must be filled.","Error", JOptionPane.ERROR_MESSAGE);
        }
    };

    /**
     * Deletes the selected employee in the database.
     */
    private final ActionListener deleteButtonListener = e -> {
        int result = JOptionPane.showConfirmDialog(view.getMainPanel(), "Are you sure that you want to delete this employee ?","Confirm deletion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);

        if (result == JOptionPane.OK_OPTION) {
            try {
                database.removeEmployee((Employee) view.getEmployeeComboBox().getSelectedItem());

                int index = view.getEmployeeComboBox().getSelectedIndex();
                view.getEmployeeComboBox().removeItemAt(index);
                view.getEmployeeComboBox().setSelectedIndex(0);
            } catch (ElementNotFoundException ignored) {
                // Can't happen as we're deleting an existing employee.
            }
        }
    };

    /**
     * Launches the planning management window.
     */
    private final ActionListener planningButtonListener = e -> {
        Employee employee = (Employee) view.getEmployeeComboBox().getSelectedItem();
        listener.openPlanningManagement(employee);
    };

    /**
     * When pressing enter, calls add or modify depending on the context.
     */
    private final Action addOrModifyKeyListener = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            if (view.getEmployeeComboBox().getSelectedIndex() == 0) {
                addButtonListener.actionPerformed(e);
            } else {
                modifyButtonListener.actionPerformed(e);
            }
        }
    };

    public EmployeeManagementPanelController(EmployeeManagementPanelView view, Database database) {
        this.view = view;
        this.database = database;

        initView();
    }

    public void setListener(EmployeeManagementListener listener) {
        this.listener = listener;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * Reloads the entire panel.
     */
    public void reloadEmployeePanel() {
        reloadDepartmentsComboBox();
        reloadEmployeeComboBox();
        resetView();
    }

    /**
     * Initializes the view.
     */
    private void initView() {
        // Employee selection
        view.getEmployeeLabel().setText("Employee : ");
        reloadEmployeeComboBox();

        // Buttons
        view.getAddButton().setText("Add");
        view.getAddButton().addActionListener(addButtonListener);

        view.getModifyButton().setText("Modify");
        view.getModifyButton().setVisible(false);
        view.getModifyButton().addActionListener(modifyButtonListener);

        view.getDeleteButton().setText("Delete");
        view.getDeleteButton().setVisible(false);
        view.getDeleteButton().addActionListener(deleteButtonListener);

        view.getPlanningButton().setText("Planning");
        view.getPlanningButton().setVisible(false);
        view.getPlanningButton().addActionListener(planningButtonListener);

        // Labels
        view.getFirstNameLabel().setText("First name : ");
        view.getLastNameLabel().setText("Last name : ");
        view.getBirthdateLabel().setText("<html>Birth date :<br>(dd/mm/yyyy)</html>");
        view.getDepartmentLabel().setText("Department : ");

        view.getMainPanel().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("ENTER"), "addOrModifyKeyListener");
        view.getMainPanel().getActionMap().put("addOrModifyKeyListener", addOrModifyKeyListener);
    }

    /**
     * Reloads the departments combobox.
     */
    private void reloadDepartmentsComboBox() {
        view.getDepartmentComboBox().removeAllItems();

        view.getDepartmentComboBox().addItem("Choose a department");

        for (Department d : database.getDepartments()) {
            view.getDepartmentComboBox().addItem(d);
        }

        if (view.getEmployeeComboBox().getSelectedIndex() != 0) {
            Employee employee = (Employee) view.getEmployeeComboBox().getSelectedItem();

            if (employee.getDepartment() != null) {
                view.getDepartmentComboBox().setSelectedItem(employee.getDepartment());
            }
        }
    }

    /**
     * Reloads employee combobox.
     */
    private void reloadEmployeeComboBox() {
        for (ActionListener al : view.getEmployeeComboBox().getActionListeners()) {
            view.getEmployeeComboBox().removeActionListener(al);
        }

        view.getEmployeeComboBox().removeAllItems();

        view.getEmployeeComboBox().addItem("+ Add employee");

        for (Employee employee : database.getEmployees()) {
            view.getEmployeeComboBox().addItem(employee);
        }

        view.getEmployeeComboBox().addActionListener(employeeComboBoxListener);
    }

    /**
     * Resets the view.
     */
    private void resetView() {
        view.getAddButton().setVisible(true);
        view.getModifyButton().setVisible(false);
        view.getDeleteButton().setVisible(false);
        view.getPlanningButton().setVisible(false);

        view.getFirstNameTextField().setText("");
        view.getLastNameTextField().setText("");
        view.getBirthdateTextField().setText("");

        view.getDepartmentComboBox().setSelectedIndex(0);
    }
}
