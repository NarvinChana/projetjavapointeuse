package com.noradrenalin.server;

import com.noradrenalin.server.controller.MainTabFrameController;
import com.noradrenalin.server.view.MainTabFrameView;

import java.io.IOException;

/**
 * The central server. Must only be launched once.
 */
public class Server {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        MainTabFrameView view = new MainTabFrameView("Server");
        MainTabFrameController controller = new MainTabFrameController(view);
    }
}
