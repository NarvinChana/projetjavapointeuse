package com.noradrenalin.server.util.csv;

import com.noradrenalin.server.model.IdAlreadyTakenException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Contains static methods that allow the importation of a database from a csv file.
 */
public class CSVImportDatabase {
    private static final String[] databaseHeader = {"Database", "VersionCode", "LastDepartmentId", "LastEmployeeId"};
    private static final String[] departmentHeader = {"Departments", "Id", "Name"};
    private static final String[] employeeHeader = {"Employees", "Id", "Fname", "Lname", "Bdate", "DepartmentId"};
    private static final String[] plannedScheduleHeader = {"PlannedSchedule", "Date", "Start", "End"};
    private static final String[] actualScheduleHeader = {"ActualSchedule", "Date", "Start", "End"};

    /**
     * Parses the CSV file.
     * @param file File to read
     * @return A new database
     * @throws IOException When the file can't be read
     * @throws FileParseException When the file isn't formatted correctly
     */
    public static Database parseCSV(File file) throws IOException, FileParseException {
        Database db = new Database();
        BufferedReader reader = Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8);
        int lineNumber = 0;

        String line;
        String[] tokens;

        // Reading Database Information
        line = reader.readLine();
        lineNumber++;

        if (headerStringIsNotValid(line, databaseHeader)){
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Database);
        }

        line = reader.readLine();
        lineNumber++;

        tokens = line.split(";");

        if (tokens.length != databaseHeader.length) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Database);
        }

        try {
            db.setVersionCode(Long.parseLong(tokens[1]));
            db.setLastDepartmentId(Long.parseLong(tokens[2]));
            db.setLastEmployeeId(Long.parseLong(tokens[3]));
        } catch (NumberFormatException e) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Database);
        }

        // Reading Department Information
        line = reader.readLine();
        lineNumber++;

        if (headerStringIsNotValid(line, departmentHeader)) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Departments);
        }

        try {
            tokens = line.split(";");

            if (tokens.length != departmentHeader.length) {
                throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Departments);
            }

            int numberOfDepartments = Integer.parseInt(tokens[0].split("=")[1]);

            for (int i = 0; i < numberOfDepartments; i++) {
                line = reader.readLine();
                lineNumber++;

                tokens = line.split(";");

                if (tokens.length != departmentHeader.length) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Departments);
                }

                db.addDepartment(new Department(Long.parseLong(tokens[1]), tokens[2]));
            }
        } catch (NumberFormatException | IdAlreadyTakenException e) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Departments);
        }

        // Reading Employee Information
        line = reader.readLine();
        lineNumber++;

        if (headerStringIsNotValid(line, employeeHeader)) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Employees);
        }

        try {
            tokens = line.split(";");

            if (tokens.length != employeeHeader.length) {
                throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Employees);
            }

            int numberOfEmployees = Integer.parseInt(tokens[0].split("=")[1]);

            for (int i = 0; i < numberOfEmployees; i++) {
                line = reader.readLine();
                lineNumber++;

                tokens = line.split(";");

                if (tokens.length != employeeHeader.length) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Employees);
                }

                Employee tempEmployee = new Employee(
                        Long.parseLong(tokens[1]),
                        tokens[2],
                        tokens[3],
                        LocalDate.parse(tokens[4], DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        db.getDepartmentById(Integer.parseInt(tokens[5]))
                );

                db.addEmployee(tempEmployee);

                // Reading Planned Schedule of Employee
                line = reader.readLine();
                lineNumber++;

                if (headerStringIsNotValid(line, plannedScheduleHeader)) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.PlannedSchedule);
                }

                tokens = line.split(";");

                if (tokens.length != plannedScheduleHeader.length) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.PlannedSchedule);
                }

                int plannedScheduleTimeSlotCount = Integer.parseInt(tokens[0].split("=")[1]);

                for (int j = 0; j < plannedScheduleTimeSlotCount; j++) {
                    line = reader.readLine();
                    lineNumber++;

                    tokens = line.split(";");

                    if (tokens.length != plannedScheduleHeader.length) {
                        throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.PlannedSchedule);
                    }

                    LocalDate tempDate = LocalDate.parse(tokens[1], DateTimeFormatter.ofPattern("dd/MM/yyyy"));

                    LocalTime tempTimeStart = "null".equals(tokens[2]) ? null : LocalTime.parse(tokens[2]);
                    LocalTime tempTimeEnd = "null".equals(tokens[3]) ? null : LocalTime.parse(tokens[3]);

                    tempEmployee.getPlanned().addTimeSlotOnDate(tempDate, new TimeSlot(tempTimeStart, tempTimeEnd));
                }

                // Reading Actual Schedule of Employee
                line = reader.readLine();
                lineNumber++;

                if (headerStringIsNotValid(line, actualScheduleHeader)) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.ActualSchedule);
                }

                tokens = line.split(";");

                if (tokens.length != actualScheduleHeader.length) {
                    throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.ActualSchedule);
                }

                int actualScheduleTimeSlotCount = Integer.parseInt(tokens[0].split("=")[1]);

                for (int j = 0; j < actualScheduleTimeSlotCount; j++) {
                    line = reader.readLine();
                    lineNumber++;

                    tokens = line.split(";");

                    if (tokens.length != actualScheduleHeader.length) {
                        throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.ActualSchedule);
                    }

                    LocalDate tempDate = LocalDate.parse(tokens[1], DateTimeFormatter.ofPattern("dd/MM/yyyy"));

                    LocalTime tempTimeStart = "null".equals(tokens[2]) ? null : LocalTime.parse(tokens[2]);
                    LocalTime tempTimeEnd = "null".equals(tokens[3]) ? null : LocalTime.parse(tokens[3]);

                    tempEmployee.getActual().addTimeSlotOnDate(tempDate, new TimeSlot(tempTimeStart, tempTimeEnd));
                }
            }
        } catch (NumberFormatException | IdAlreadyTakenException | IncorrectTimeException | DateTimeParseException | TimeSlotOverlapException e) {
            throw new FileParseException(lineNumber, line, FileParseException.FilePartEnum.Employees);
        }
        return db;
    }

    /**
     * Checks if a file line is not equal to a passed header.
     * @param line Line to check
     * @param correctHeader Default header
     * @return True if the header is not valid and false otherwise
     */
    private static boolean headerStringIsNotValid(String line, String[] correctHeader) {
        String[] tokens = line.split(";");

        if (tokens.length != correctHeader.length) {
            return true;
        }

        boolean validHeader = true;

        for (int i = 0; i < tokens.length; i++) {
            if (i == 0) {
                String a = tokens[0].split("=")[0];
                String b = correctHeader[0];

                if (!(a.equals(b))) {
                    validHeader = false;
                }
            } else if (!tokens[i].equals(correctHeader[i])) {
                validHeader = false;
            }
        }
        return !validHeader;
    }
}
