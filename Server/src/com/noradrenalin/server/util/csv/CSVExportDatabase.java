package com.noradrenalin.server.util.csv;

import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.Schedule;
import com.noradrenalin.server.model.schedule.TimeSlot;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;

/**
 * Class that allows the save of a Database in a CSV formatted file.
 */
public class CSVExportDatabase {
    /**
     * Saves in the file currently opened by out.
     */
    public static void saveCSV(Database db, BufferedWriter out) throws IOException {
        out.append("Database;VersionCode;LastDepartmentId;LastEmployeeId;;\n");
        out.append(String.format(";%d;%d;%d;;\n", db.getVersionCode(), db.getLastDepartmentId(), db.getLastEmployeeId()));
        out.append(String.format("Departments=%d;Id;Name;;;\n",db.getDepartments().size()));
        for (Department d : db.getDepartments()) {
            out.append(String.format(";%d;%s;;;\n", d.getId(), d.getName()));
        }
        out.append(String.format("Employees=%d;Id;Fname;Lname;Bdate;DepartmentId\n", db.getEmployees().size()));
        for (Employee e : db.getEmployees()) {
            out.append(String.format(";%d;%s;%s;%s;%d\n", e.getId(), e.getFirstName(), e.getLastName(), e.getBirthDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), e.getDepartment().getId()));

            writeSchedule(e.getPlanned(), "PlannedSchedule", out);
            writeSchedule(e.getActual(), "ActualSchedule", out);
        }
    }

    /**
     * Writes the data to save a specific Schedule.
     */
    private static void writeSchedule(Schedule s, String scheduleName, BufferedWriter out) throws IOException {
        int count = 0;
        StringBuilder timeSlotsText = new StringBuilder();
        for (Map.Entry<LocalDate, ArrayList<TimeSlot>> entry : s.getTimeSlots().entrySet()) {
            for (TimeSlot ts : entry.getValue()) {
                timeSlotsText.append(
                        String.format(
                                ";%s;%s;%s;;\n",
                                entry.getKey().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                                ts.getStartTime() == null ? "null" : ts.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm")),
                                ts.getEndTime() == null ? "null" : ts.getEndTime().format(DateTimeFormatter.ofPattern("HH:mm"))));
                count++;
            }
        }
        out.append(String.format("%s=%d;Date;Start;End;;\n", scheduleName, count));
        out.append(timeSlotsText.toString());
    }
}


