package com.noradrenalin.server.util.csv;

/**
 * Exception to be raised when the parsing of a file encounters a problem. Can contain the line where the error occurred and an enum representing which part of the reading failed.
 */
public class FileParseException extends Exception {
    public enum FilePartEnum {
        Database,
        Departments,
        Employees,
        PlannedSchedule,
        ActualSchedule
    }

    private final int errorLine;
    private final String errorString;
    private final FilePartEnum partEnum;

    public FileParseException(int errorLine, String errorString, FilePartEnum partEnum) {
        this.errorLine = errorLine;
        this.errorString = errorString;
        this.partEnum = partEnum;
    }

    public int getErrorLine() {
        return errorLine;
    }

    public String getErrorString() {
        return errorString;
    }

    public FilePartEnum getPartEnum() {
        return partEnum;
    }
}
