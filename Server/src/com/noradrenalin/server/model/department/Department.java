package com.noradrenalin.server.model.department;

import java.io.Serializable;
import java.util.Objects;

/**
 * Represents a department of a company defined by a name and composed of employees.
 */
public class Department implements Serializable {
    private final long id;
    private String name;

    /**
     * Default constructor for a Department.
     * @param id   Department's id
     * @param name Department's name
     */
    public Department(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Department(Department d) {
        id = d.getId();
        name = d.getName();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof Department) {
            Department other = (Department) obj;
            return getId() == other.getId();
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}