package com.noradrenalin.server.model.schedule;

import java.time.LocalDate;
import java.util.EventListener;

/**
 * Interface that allows for the implementation of a listener that allows us to know that a day's timeslot list has changed.
 */
public interface ScheduleListener extends EventListener {
    void daysTimeSlotChanged(LocalDate date);
}
