package com.noradrenalin.server.model.schedule;

import com.noradrenalin.server.model.IncorrectTimeException;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalTime;

/**
 * Represents a period of time in a day.
 */
public class TimeSlot implements Serializable, Comparable<TimeSlot> {
    private LocalTime startTime;
    private LocalTime endTime;
    private Duration duration;

    /**
     * Default constructor of TimeSlot.
     */
    public TimeSlot(LocalTime start, LocalTime end) throws IncorrectTimeException {
        setStartTime(start);
        setEndTime(end);
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    /**
     * Modifies the start time.
     * @throws IncorrectTimeException If the chosen start time is after the defined end time
     */
    public void setStartTime(LocalTime startTime) throws IncorrectTimeException {
        if (startTime == null || getEndTime() == null) {
            this.startTime = startTime;
            duration = Duration.ofHours(0);
        } else if (startTime.isBefore(getEndTime())) {
            this.startTime = startTime;
            duration = Duration.between(getStartTime(), getEndTime());
        } else {
            throw new IncorrectTimeException("The specified start time cannot be after the current end time.");
        }
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    /**
     * Modifies the end time.
     * @throws IncorrectTimeException If the chosen end time is before the defined start time
     */
    public void setEndTime(LocalTime endTime) throws IncorrectTimeException {
        if (getStartTime() == null || endTime == null) {
            this.endTime = endTime;
            duration = Duration.ofHours(0);
        } else if (endTime.isAfter(getStartTime())) {
            this.endTime = endTime;
            duration = Duration.between(getStartTime(), getEndTime());
        } else {
            throw new IncorrectTimeException("The specified end time cannot be before the current start time.");
        }
    }

    /**
     * Returns the duration between start and end time.
     * @return The duration
     */
    public Duration getDuration() {
        return duration;
    }

    @Override
    public int compareTo(TimeSlot other) {
        if (other == null) {
            throw new NullPointerException();
        } else {
            int result;

            LocalTime startTime = getStartTime();
            LocalTime otherStartTime = other.getStartTime();
            LocalTime endTime = getEndTime();
            LocalTime otherEndTime = other.getEndTime();

            // The following tests are not very clean but they're the only way to prevent null values to throw NullPointerException
            if (startTime == otherStartTime) {
                if (endTime == otherEndTime) {
                    result = 0;
                } else if (otherEndTime == null) {
                    result = -1;
                } else if (endTime == null) {
                    result = 1;
                } else if (endTime.isBefore(otherEndTime)) {
                    result = -1;
                } else {
                    result = 1;
                }
            } else if (otherStartTime == null) {
                result = -1;
            } else if (startTime == null) {
                result = 1;
            } else if (startTime.isBefore(otherStartTime)) {
                result = -1;
            } else {
                result = 1;
            }

            return result;
        }
    }

    @Override
    public String toString() {
        return getStartTime() + " - " + getEndTime();
    }
}
