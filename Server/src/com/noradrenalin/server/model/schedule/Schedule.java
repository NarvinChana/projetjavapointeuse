package com.noradrenalin.server.model.schedule;

import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.TimeSlotOverlapException;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Class that stores the associations of a Date and an ArrayList of TimeSlots.
 */
public class Schedule implements Serializable {
    private final HashMap<LocalDate, ArrayList<TimeSlot>> timeSlots;
    private transient ScheduleListener listener;

    /**
     * Default constructor of Schedule.
     */
    public Schedule() {
        timeSlots = new HashMap<>();
    }

    /**
     * Returns the time slots of a specific date.
     * @param date Date of the time slots
     * @return Time slots of date
     * @throws ElementNotFoundException If the date has currently no entry registered
     */
    public ArrayList<TimeSlot> getTimeSlotsOfDate(LocalDate date) throws ElementNotFoundException {
        ArrayList<TimeSlot> slots;

        if (timeSlots.containsKey(date)) {
            slots = timeSlots.get(date);
        } else {
            throw new ElementNotFoundException("The specified date is not registered.", LocalDate.class);
        }

        return slots;
    }

    /**
     * Returns a copy of the HashMap containing the TimeSlots.
     * @return The full HashMap
     */
    public HashMap<LocalDate, ArrayList<TimeSlot>> getTimeSlots() {
        return new HashMap<>(timeSlots);
    }

    /**
     * Returns the TimeSlots that does not have an end time defined.
     * @param date Date of the TimeSlots
     * @return Null-ended timeslot if there is one or null
     */
    public TimeSlot getLastTimeSlotOfDate(LocalDate date) {
        ArrayList<TimeSlot> timeSlots;
        try {
            timeSlots = getTimeSlotsOfDate(date);
        } catch (ElementNotFoundException e) {
            return null;
        }

        for (TimeSlot ts : timeSlots) {
            if (ts.getEndTime() == null) {
                return ts;
            }
        }
        return null;
    }

    /**
     * Add a time slot to a specific date. If the date is not registered, it creates the list associated.
     * @param date Date of the new time slot
     * @param slot The new time slot to add
     */
    public void addTimeSlotOnDate(LocalDate date, TimeSlot slot) throws TimeSlotOverlapException {
        if (!timeSlots.containsKey(date)) {
            timeSlots.put(date, new ArrayList<>());
        } else {
            for (TimeSlot slot2 : timeSlots.get(date)) {
                if (slot.getStartTime() != null && slot.getEndTime() != null && slot2.getStartTime() != null && slot2.getEndTime() != null) {
                    if ((slot.getStartTime().isAfter(slot2.getStartTime()) && slot.getStartTime().isBefore(slot2.getEndTime())) ||
                            (slot.getEndTime().isAfter(slot2.getStartTime()) && slot.getEndTime().isBefore(slot2.getEndTime())) ||
                            (slot.getStartTime().compareTo(slot2.getStartTime()) <= 0 && slot.getEndTime().compareTo(slot2.getEndTime()) >= 0)) {
                        throw new TimeSlotOverlapException("This new timeslot overlaps another.");
                    }
                }
            }
        }
        timeSlots.get(date).add(slot);

        if (listener != null) {
            listener.daysTimeSlotChanged(date);
        }
    }

    /**
     * Removes a time slot from a specific date.
     * @param date Date of the time slot to remove
     * @param slot The time slot to remove
     * @throws ElementNotFoundException If the time slot is not associated with the date or the date is not registered in the schedule
     */
    public void removeTimeSlotOfDate(LocalDate date, TimeSlot slot) throws ElementNotFoundException {
        ArrayList<TimeSlot> dateSlots = getTimeSlotsOfDate(date);

        if (dateSlots.contains(slot)) {
            dateSlots.remove(slot);
            listener.daysTimeSlotChanged(date);

            if (dateSlots.isEmpty()) {
                timeSlots.remove(date);
            }
        } else {
            throw new ElementNotFoundException("No such time slot found in the Schedule.", TimeSlot.class);
        }
    }

    /**
     * Finds a specific TimeSlot.
     * @param date Date of the TimeSlot to find
     * @param startTime Start time of the TimeSlot to find
     * @param endTime End time of the TimeSlot to find
     * @return The TimeSlot that we are looking for
     * @throws ElementNotFoundException This TimeSlot doesn't exist
     */
    public TimeSlot findTimeSlot(LocalDate date, LocalTime startTime, LocalTime endTime) throws ElementNotFoundException {
        ArrayList<TimeSlot> slots = getTimeSlotsOfDate(date);

        for (TimeSlot timeSlot : slots) {
            if (startTime == null) {
                if (timeSlot.getEndTime().equals(endTime) && timeSlot.getStartTime() == null) {
                    return timeSlot;
                }
            } else if (endTime == null) {
                if (timeSlot.getStartTime().equals(startTime) && timeSlot.getEndTime() == null) {
                    return timeSlot;
                }
            } else if (timeSlot.getStartTime().equals(startTime) && timeSlot.getEndTime().equals(endTime)) {
                return timeSlot;
            }
        }

        throw new ElementNotFoundException("No such time slot found in the Schedule.", TimeSlot.class);
    }

    public void setListener(ScheduleListener listener) {
        this.listener = listener;
    }
}
