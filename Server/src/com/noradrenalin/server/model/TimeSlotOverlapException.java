package com.noradrenalin.server.model;

/**
 * Thrown when a TimeSlot is overlapping another TimeSlot in a Schedule
 */
public class TimeSlotOverlapException extends Exception{
    private final String message;

    public TimeSlotOverlapException(String msg)
    {
        message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
