package com.noradrenalin.server.model;

/**
 * Thrown when a time cannot be set logically
 *
 * Example: when a LocalTime start is set after an end time
 */
public class IncorrectTimeException extends Exception
{
    private final String message;

    public IncorrectTimeException(String msg)
    {
        message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
