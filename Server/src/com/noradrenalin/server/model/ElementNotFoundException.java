package com.noradrenalin.server.model;

/**
 * Exception raised when an element is not found.
 */
public class ElementNotFoundException extends Exception {
    private final String message;

    private final Class<?> objectType;

    public ElementNotFoundException(String msg, Class<?> objType)
    {
        message = msg;
        objectType = objType;
    }

    @Override
    public String getMessage() {
        return message;
    }
    
    public Class<?> getObjectType() {
        return objectType;
    }
}
