package com.noradrenalin.server.model;

/**
 * Exception raised when an object with an already used ID is added to the database.
 */
public class IdAlreadyTakenException extends Exception {
    private final String message;

    public IdAlreadyTakenException(String msg) {
        message = msg;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
