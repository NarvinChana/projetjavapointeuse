package com.noradrenalin.server.model.employee;

import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.schedule.Schedule;
import com.noradrenalin.server.model.schedule.TimeSlot;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

/**
 * This class represents an employee with their name, id, birthdate, the planning and the history of punch in/out.
 */
public class Employee implements Serializable {
    private long id;

    private String firstName;
    private String lastName;

    private LocalDate birthDate;

    private Department department;

    private Schedule planned;
    private Schedule actual;

    private HashMap<LocalDate, Long> dailyMinutesCredit;

    private int minutesCredit;

    private transient List<EmployeeListener> listeners;

    /**
     * Default constructor of an Employee.
     * @param id Employee's ID
     * @param fName Employee's first name
     * @param lName Employee's last name
     * @param bDate Employee's birth date
     * @param department Employee's department
     */
    public Employee(long id, String fName, String lName, LocalDate bDate, Department department) throws IncorrectTimeException {
        setBirthDate(bDate);

        this.id = id;
        firstName = fName;
        lastName = lName;
        this.department = department;

        planned = new Schedule();
        actual = new Schedule();
        actual.setListener(this::recalculateMinutesCredit);

        dailyMinutesCredit = new HashMap<>();

        listeners = new ArrayList<>();
    }

    /**
     * Returns the Employee's ID.
     * @return Employee's ID
     */
    public long getId() {
        return id;
    }

    /**
     * Returns the Employee's first name.
     * @return Employee's first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Modifies the Employee's first name.
     * @param firstName New Employee's first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
        fireNameChanged(getFirstName(), getLastName());
    }

    /**
     * Returns the Employee's last name.
     * @return Employee's last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Returns the Employee's last name.
     * @param lastName New Employee's last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
        fireNameChanged(getFirstName(), getLastName());
    }

    /**
     * Returns the Employee's birth date.
     * @return Employee's birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Modify the Employee's birth date.
     * @param birthDate New Employee's date
     * @throws IncorrectTimeException If the new birth date is in the future.
     */
    public void setBirthDate(LocalDate birthDate) throws IncorrectTimeException {
        if (birthDate.isAfter(LocalDate.now())) {
            throw new IncorrectTimeException("The birth date cannot be in the future.");
        } else {
            this.birthDate = birthDate;
        }
    }

    /**
     * Returns the planned schedule.
     */
    public Schedule getPlanned() {
        return planned;
    }

    /**
     * Returns the history of punch-ins and punch-outs.
     */
    public Schedule getActual() {
        return actual;
    }

    public Department getDepartment() {
        if (department == null) {
            return null;
        } else {
            return new Department(department);
        }
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     * Returns the cumulated extra/missing hours.
     * @return Cumulated extra/missing hours.
     */
    public int getMinutesCredit() {
        return minutesCredit;
    }

    /**
     * Adds a listener that will be notified of changes on the Employee.
     */
    public void addListener(EmployeeListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener of the Employee.
     */
    public void removeListener(EmployeeListener listener) {
        listeners.remove(listener);
    }

    /**
     * Adds a punch-in/out to the actual Schedule of the employee.
     */
    public void addPunchIn(LocalDateTime dateTime) throws TimeSlotOverlapException, IncorrectTimeException {
        LocalDate date = dateTime.toLocalDate();
        LocalTime time = dateTime.toLocalTime();
        TimeSlot timeSlot = null;

        ArrayList<TimeSlot> actualTimeSlotList = null;
        try {
            actualTimeSlotList = getActual().getTimeSlotsOfDate(date);
        } catch (ElementNotFoundException ignored) { }

        if (actualTimeSlotList == null) {
            timeSlot = new TimeSlot(time, null);
            getActual().addTimeSlotOnDate(date, timeSlot);
        } else {
            for (TimeSlot t : actualTimeSlotList) {
                if (t.getEndTime() == null) {
                    timeSlot = t;
                    break;
                } else {
                    if ((t.getEndTime().isAfter(time)) && (t.getStartTime().isBefore(time))) {
                        throw new IncorrectTimeException("Punch in/out cannot be added, a TimeSlot already contains the specified time.");
                    }
                }
            }

            if (timeSlot == null) {
                timeSlot = new TimeSlot(time, null);
                getActual().addTimeSlotOnDate(date, timeSlot);
            } else {
                timeSlot.setEndTime(time);
                recalculateMinutesCredit(date);
            }
        }
    }

    /**
     * Recalculates the hourCredit of an employee.
     * @param date Date to be recalculated.
     */
    public void recalculateMinutesCredit(LocalDate date) {
        long totalDailyMinutes = 0;

        try {
            ArrayList<TimeSlot> plannedTimeSlotList = getPlanned().getTimeSlotsOfDate(LocalDate.of(2018, 1, date.getDayOfWeek().getValue()));
            for (TimeSlot t : plannedTimeSlotList) {
                totalDailyMinutes -= t.getDuration().toMinutes();
            }
        } catch (ElementNotFoundException ignored) {
            // If there is no planned schedule for that day, we don't calculate we only add the actual schedule to the totalDailyMinutes worked.
        }

        ArrayList<TimeSlot> actualTimeSlotList;
        try {
            actualTimeSlotList = getActual().getTimeSlotsOfDate(date);
            for (TimeSlot t : actualTimeSlotList) {
                if (t.getEndTime() != null) {
                    totalDailyMinutes += t.getDuration().toMinutes();
                }
            }
        } catch (ElementNotFoundException ignored) {
            // If there is no actual schedule for the day, we only take into account the planned schedule.
            // If both schedules are empty, we only remove the previous DailyMinutesCredit from the total.
        }

        if (dailyMinutesCredit.containsKey(date)) {
            minutesCredit -= dailyMinutesCredit.get(date);
            dailyMinutesCredit.replace(date, totalDailyMinutes);
        } else {
            dailyMinutesCredit.put(date, totalDailyMinutes);
        }

        minutesCredit += totalDailyMinutes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj instanceof Employee) {
            Employee other = (Employee) obj;
            return getId() == other.getId();
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return getLastName() + " " + getFirstName();
    }

    /**
     * Triggers all listeners whenever the Employee's name is changed.
     */
    protected void fireNameChanged(String newFirstName, String newLastName) {
        for (EmployeeListener l : listeners) {
            l.employeeNameChanged(this, newFirstName, newLastName);
        }
    }

    /**
     * Deserializes the Employee.
     */
    @Serial
    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException, IncorrectTimeException {
        id = (long) aInputStream.readObject();
        firstName = (String) aInputStream.readObject();
        lastName = (String) aInputStream.readObject();
        setBirthDate((LocalDate) aInputStream.readObject());
        department = (Department) aInputStream.readObject();

        planned = (Schedule) aInputStream.readObject();
        actual = (Schedule) aInputStream.readObject();
        actual.setListener(this::recalculateMinutesCredit);

        dailyMinutesCredit = (HashMap<LocalDate, Long>) aInputStream.readObject();

        listeners = new ArrayList<>();
    }

    /**
     * Serializes the Employee.
     */
    @Serial
    private void writeObject(ObjectOutputStream aOutputStream) throws IOException
    {
        aOutputStream.writeObject(id);
        aOutputStream.writeObject(firstName);
        aOutputStream.writeObject(lastName);
        aOutputStream.writeObject(birthDate);
        aOutputStream.writeObject(department);
        aOutputStream.writeObject(planned);
        aOutputStream.writeObject(actual);
        aOutputStream.writeObject(dailyMinutesCredit);
    }

}
