package com.noradrenalin.server.model.employee;

import java.util.EventListener;

/**
 * Interface for classes listening to Employee changes.
 */
public interface EmployeeListener extends EventListener {
    void employeeNameChanged(Employee e, String fName, String lName);
}
