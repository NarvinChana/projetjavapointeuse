package com.noradrenalin.server.model.database;

import java.util.EventListener;

/**
 * Interface for classes listening to Database changes
 */
public interface DatabaseListener extends EventListener {
    /**
     * Triggered when the database's version code is updated.
     */
    void versionCodeUpdated(Database d, long oldCode, long newCode);
    void departmentsListSizeChanged(Database d, int newSize);
}
