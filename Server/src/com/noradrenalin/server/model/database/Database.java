package com.noradrenalin.server.model.database;

import com.noradrenalin.common.model.ClientDatabase;
import com.noradrenalin.common.model.ClientEmployee;
import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IdAlreadyTakenException;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.employee.EmployeeListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Stores the complete model.
 */
public class Database implements EmployeeListener, Serializable {
    private long versionCode;

    private final ArrayList<Department> departments;
    private final ArrayList<Employee> employees;

    private long lastEmployeeId = 0;
    private long lastDepartmentId = 0;

    private final List<DatabaseListener> listeners;

    /**
     * Default constructor of Database
     *
     * It initializes the database in an incorrect state with an empty list of Companies and a version code of -1."
     */
    public Database() {
        versionCode = -1;
        departments = new ArrayList<>();
        employees = new ArrayList<>();
        listeners = new ArrayList<>();
    }

    public ClientDatabase toClientDatabase() {
        ClientDatabase clientDatabase = new ClientDatabase();

        ArrayList<ClientEmployee> clientEmployeeList = new ArrayList<>();
        List<Employee> employeeList = getEmployees();

        for (Employee e : employeeList) {
            clientEmployeeList.add(new ClientEmployee(e.getId(), e.getFirstName(), e.getLastName()));
        }

        clientDatabase.setEmployeesWithVersion(clientEmployeeList, getVersionCode());

        return clientDatabase;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(long versionCode) {
        this.versionCode = versionCode;
    }

    public long getLastEmployeeId() {
        return lastEmployeeId;
    }

    public void setLastEmployeeId(long lastEmployeeId) {
        this.lastEmployeeId = lastEmployeeId;
    }

    public long getLastDepartmentId() {
        return lastDepartmentId;
    }

    public void setLastDepartmentId(long lastDepartmentId) {
        this.lastDepartmentId = lastDepartmentId;
    }

    public List<Employee> getEmployees() {
        return Collections.unmodifiableList(employees);
    }

    public List<Department> getDepartments() {
        return Collections.unmodifiableList(departments);
    }

    /**
     * Increases the last employee index by one and returns this modified value.
     * @return lastEmployeeId++
     */
    public long getNewEmployeeId() {
        lastEmployeeId++;
        return lastEmployeeId;
    }

    /**
     * Increases the last department index by one and returns this modified value.
     * @return lastDepartmentId++
     */
    public long getNewDepartmentId() {
        lastDepartmentId++;
        return lastDepartmentId;
    }

    /**
     * Gets a department from the db with the id id.
     * @param id Id of department
     * @return Department of id specified
     */
    public Department getDepartmentById(int id) {
        for (Department d : departments) {
            if (d.getId() == id) {
                return d;
            }
        }
        return null;
    }

    /**
     * Adds a listener that will be notified of changes on the Database.
     */
    public void addListener(DatabaseListener listener) {
        listeners.add(listener);
    }

    /**
     * Removes a listener of the Database.
     */
    public void removeListener(DatabaseListener listener) {
        listeners.remove(listener);
    }

    /**
     * Adds an employee to the list.
     * @param employee Employee to add
     * @throws IdAlreadyTakenException If the new employee's ID is already in use by another employee
     */
    public void addEmployee(Employee employee) throws IdAlreadyTakenException {
        for (Employee e : employees) {
            if (e.equals(employee)) {
                throw new IdAlreadyTakenException("The employee's ID is already taken");
            }
        }

        employee.addListener(this);
        employees.add(employee);
        lastEmployeeId = Math.max(lastEmployeeId, employee.getId());

        updateVersionCode();
    }

    /**
     * Removes an employee from the list.
     * @param employee Employee to remove
     * @throws ElementNotFoundException If the employee isn't in the list
     */
    public void removeEmployee(Employee employee) throws ElementNotFoundException {
        if (employees.contains(employee)) {
            employee.removeListener(this);
            employees.remove(employee);

            updateVersionCode();
        } else {
            throw new ElementNotFoundException("The employee could not be found.", Employee.class);
        }
    }

    /**
     * Adds a department to the list.
     * @param department Department to add
     * @throws IdAlreadyTakenException If the new department's ID is already in use by another department
     */
    public void addDepartment(Department department) throws IdAlreadyTakenException {

        for (Department d : departments) {
            if(d.equals(department)) {
                throw new IdAlreadyTakenException("The department's ID is already taken.");
            }
        }

        departments.add(department);
        lastDepartmentId = Math.max(lastDepartmentId, department.getId());
        fireDepartmentsListSizeChanged(departments.size());
    }

    /**
     * Removes a department from the list.
     * @param department Department to remove
     * @throws ElementNotFoundException If the department isn't in the list
     */
    public void removeDepartment(Department department) throws ElementNotFoundException {
        if (departments.contains(department)) {
            departments.remove(department);

            for (Employee e : employees) {
                Department d = e.getDepartment();

                if (d != null) {
                    if (d.equals(department)) {
                        e.setDepartment(null);
                    }
                }
            }
            fireDepartmentsListSizeChanged(departments.size());
        } else {

            throw new ElementNotFoundException("The department could not be found.", Department.class);
        }
    }

    /**
     * Method called whenever an Employee has his first name or last name changed.
     * @param e The modified Employee
     * @param fName New Employee's first name
     * @param lName New Employee's last name
     */
    @Override
    public void employeeNameChanged(Employee e, String fName, String lName) {
        updateVersionCode();
    }

    /**
     * Triggers all listeners whenever the Departments list's size is changed.
     */
    protected void fireDepartmentsListSizeChanged(int newSize) {
        for (DatabaseListener l : listeners) {
            l.departmentsListSizeChanged(this, newSize);
        }
    }

    /**
     * Triggers all listeners whenever the version code is updated.
     */
    protected void fireVersionCodeUpdated(long oldCode, long newCode) {
        for (DatabaseListener l : listeners) {
            l.versionCodeUpdated(this, oldCode, newCode);
        }
    }

    private void updateVersionCode() {
        versionCode++;
        fireVersionCodeUpdated(versionCode - 1, versionCode);
    }
}
