package com.noradrenalin.server.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for Settings.
 */
public class SettingsPanelView {
    private final JPanel mainPanel;
    private final JLabel clientLabel;
    private final JTable clientTable;
    private final JScrollPane tableScroll;
    private final ClientListTableModel model;
    private final JButton removeClientButton;
    private final JLabel portLabel;
    private final JTextField portTextField;
    private final JButton portChangeButton;
    private final JButton importDatabaseButton;
    private final JButton exportDatabaseButton;
    private final JButton resetButton;
    private final JButton clearBlacklistButton;

    public SettingsPanelView() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        clientLabel = new JLabel();
        model = new ClientListTableModel();
        clientTable = new JTable(model);
        tableScroll = new JScrollPane(clientTable);
        removeClientButton = new JButton();
        portLabel = new JLabel();
        portTextField = new JTextField();
        portChangeButton = new JButton();
        importDatabaseButton = new JButton();
        exportDatabaseButton = new JButton();
        resetButton = new JButton();
        clearBlacklistButton = new JButton();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning of tableScroll
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 2;
        c.weighty = 2;
        c.insets = new Insets(5, 0, 0, 0);
        mainPanel.add(tableScroll, c);

        // Positioning of clientLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(clientLabel, c);

        // Positioning of removeClientButton
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(removeClientButton, c);

        // Positioning of portLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(portLabel, c);

        // Positioning of portTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(portTextField, c);

        // Positioning of portChangeButton
        c.gridx = 3;
        c.gridy = 2;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(portChangeButton, c);

        // Positioning of importDatabaseButton
        c.gridx = 2;
        c.gridy = 3;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(importDatabaseButton, c);

        // Positioning of exportDatabaseButton
        c.gridx = 3;
        c.gridy = 3;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(exportDatabaseButton, c);

        // Positioning of resetButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 4;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(resetButton, c);

        // Positioning of clearBlacklistButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 4;
        c.weightx = 0;
        c.weighty = 0;
        mainPanel.add(clearBlacklistButton, c);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JLabel getClientLabel() {
        return clientLabel;
    }

    public JTable getClientTable() {
        return clientTable;
    }

    public ClientListTableModel getModel() {
        return model;
    }

    public JButton getRemoveClientButton() {
        return removeClientButton;
    }

    public JButton getResetButton() {
        return resetButton;
    }

    public JLabel getPortLabel() {
        return portLabel;
    }

    public JTextField getPortTextField() {
        return portTextField;
    }

    public JButton getImportDatabaseButton() {
        return importDatabaseButton;
    }

    public JButton getExportDatabaseButton() {
        return exportDatabaseButton;
    }

    public JButton getPortChangeButton() {
        return portChangeButton;
    }

    public JButton getClearBlacklistButton() {
        return clearBlacklistButton;
    }
}
