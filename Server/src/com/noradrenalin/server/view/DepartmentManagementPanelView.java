package com.noradrenalin.server.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for DepartmentManagement.
 */
public class DepartmentManagementPanelView {
    private final JPanel mainPanel;
    private final JLabel departmentLabel;
    private final JComboBox<Object> departmentComboBox;
    private final JPanel modifPanel;
    private final JLabel depNameLabel;
    private final JTextField depNameTextField;
    private final JPanel buttonPanel;
    private final JButton addButton;
    private final JButton modifyButton;
    private final JButton deleteButton;

    public DepartmentManagementPanelView() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        modifPanel = new JPanel();
        buttonPanel = new JPanel();
        departmentLabel = new JLabel();
        departmentComboBox = new JComboBox<>();
        depNameLabel = new JLabel();
        depNameTextField = new JTextField();
        addButton = new JButton();
        deleteButton = new JButton();
        modifyButton = new JButton();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning departmentLabel
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(departmentLabel, c);

        // Positioning departmentComboBox
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(departmentComboBox, c);

        // Positioning modifPanel
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(modifPanel, c);
        modifPanel.setBorder(BorderFactory.createEtchedBorder());
        modifPanel.setLayout(new GridBagLayout());

        c = new GridBagConstraints();

        // Positioning nomDeptLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.LINE_END;
        modifPanel.add(depNameLabel, c);

        // Positioning nomDeptTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 25);
        c.anchor = GridBagConstraints.LINE_START;
        modifPanel.add(depNameTextField, c);

        // Positioning buttonPanel
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        modifPanel.add(buttonPanel, c);
        buttonPanel.setLayout(new GridBagLayout());

        c = new GridBagConstraints();

        // Positioning modifyButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        buttonPanel.add(modifyButton, c);

        // Positioning addButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        buttonPanel.add(addButton, c);

        // Positioning deleteButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        buttonPanel.add(deleteButton, c);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JLabel getDepartmentLabel() {
        return departmentLabel;
    }

    public JComboBox<Object> getDepartmentComboBox() {
        return departmentComboBox;
    }

    public JLabel getDepNameLabel() {
        return depNameLabel;
    }

    public JTextField getDepNameTextField() {
        return depNameTextField;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getModifyButton() {
        return modifyButton;
    }
}
