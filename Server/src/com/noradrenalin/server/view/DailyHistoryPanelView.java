package com.noradrenalin.server.view;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * View for DailyHistory.
 */
public class DailyHistoryPanelView {
    private final JPanel mainPanel;
    private final JButton addTimeSlotButton;
    private final JLabel filterLabel;
    private final JComboBox<Object> filterComboBox;
    private final JComboBox<Object> empOrDeptComboBox;
    private final JScrollPane historyPanel;
    private final JTable historyTable;
    private final DefaultTableModel historyModel;

    public DailyHistoryPanelView() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        addTimeSlotButton = new JButton();

        filterLabel = new JLabel();
        filterComboBox = new JComboBox<>();
        empOrDeptComboBox = new JComboBox<>();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning filterLabel
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(filterLabel, c);

        // Positioning filterComboBox
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(filterComboBox, c);

        // Positioning empOrDeptComboBox
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.PAGE_START;
        mainPanel.add(empOrDeptComboBox, c);

        // Positioning historyPanel
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.gridwidth = 4;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.PAGE_START;

        // Positioning historyTable
        historyModel = new CustomTableModel();
        historyTable = new JTable(historyModel);
        historyPanel = new JScrollPane(historyTable);

        mainPanel.add(historyPanel, c);

        // Positioning addTimeSlotButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.gridwidth = 4;
        c.insets = new Insets(0, 0, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(addTimeSlotButton, c);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JButton getAddTimeSlotButton() {
        return addTimeSlotButton;
    }

    public JLabel getFilterLabel() {
        return filterLabel;
    }

    public JComboBox<Object> getFilterComboBox() {
        return filterComboBox;
    }

    public JComboBox<Object> getEmpOrDeptComboBox() {
        return empOrDeptComboBox;
    }

    public JTable getHistoryTable() {
        return historyTable;
    }

    public DefaultTableModel getHistoryModel() {
        return historyModel;
    }
}
