package com.noradrenalin.server.view;

import javax.swing.table.DefaultTableModel;

/**
 * Custom model used in history tables.
 */
public class CustomTableModel extends DefaultTableModel {
    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
