
package com.noradrenalin.server.view;

/**
 * Class for JComboBox objects, with a hidden long value (id).
 */
public class ElementCBox {
    private final long id;
    private String name;

    public ElementCBox(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}