package com.noradrenalin.server.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for ManagePlanning.
 */
public class PlanningManagementFrameView {
    private final JFrame mainFrame;
    private final JPanel mainPanel;
    private final JLabel dayLabel;
    private final JComboBox<Object> dayComboBox;
    private final JPanel modifPanel;
    private final JLabel startTimeLabel;
    private final JTextField startTimeTextField;
    private final JLabel endTimeLabel;
    private final JTextField endTimeTextField;
    private final JLabel timeSlotLabel;
    private final JComboBox<Object> timeSlotComboBox;
    private final JButton addButton;
    private final JButton modifyButton;
    private final JButton deleteButton;

    public PlanningManagementFrameView(String title) {
        mainFrame = new JFrame(title);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        mainFrame.add(mainPanel);

        dayLabel = new JLabel();
        dayComboBox = new JComboBox<>();
        startTimeLabel = new JLabel();
        startTimeTextField = new JTextField();
        endTimeLabel = new JLabel();
        endTimeTextField = new JTextField();
        timeSlotLabel = new JLabel();
        timeSlotComboBox = new JComboBox();
        addButton = new JButton();
        deleteButton = new JButton();
        modifyButton = new JButton();

        modifPanel = new JPanel();
        modifPanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        // Positioning dayLabel
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(dayLabel, c);

        // Positioning dayComboBox
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(dayComboBox, c);

        // Positioning modifPanel
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(modifPanel, c);
        modifPanel.setBorder(BorderFactory.createEtchedBorder());
        modifPanel.setLayout(new GridBagLayout());

        c = new GridBagConstraints();

        // Positioning timeSlotLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(timeSlotLabel, c);

        // Positioning timeSlotComboBox
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(timeSlotComboBox, c);

        // Positioning startTimeLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(startTimeLabel, c);

        // Positioning startTimeTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 0;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 25);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(startTimeTextField, c);

        // Positioning endTimeLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(endTimeLabel, c);

        // Positioning endTimeTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 1;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 25);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(endTimeTextField, c);

        // Positioning modifyButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_END;
        modifPanel.add(modifyButton, c);

        // Positioning addButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(addButton, c);

        // Positioning deleteButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 4;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        modifPanel.add(deleteButton, c);

        mainFrame.setLocationRelativeTo(null);
        mainFrame.setMinimumSize(new Dimension(560, 200));
        mainFrame.pack();
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JComboBox<Object> getDayComboBox() {
        return dayComboBox;
    }

    public JLabel getDayLabel() {
        return dayLabel;
    }

    public JLabel getStartTimeLabel() {
        return startTimeLabel;
    }

    public JLabel getEndTimeLabel() {
        return endTimeLabel;
    }

    public JLabel getTimeSlotLabel() {
        return timeSlotLabel;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JComboBox<Object> getTimeSlotComboBox() {
        return timeSlotComboBox;
    }

    public JButton getModifyButton() {
        return modifyButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JTextField getEndTimeTextField() {
        return endTimeTextField;
    }

    public JTextField getStartTimeTextField() {
        return startTimeTextField;
    }
}
