package com.noradrenalin.server.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for the main tabbed window.
 */
public class MainTabFrameView {
    private final JFrame mainFrame;
    private final JTabbedPane tabbedPane;
    private final DailyHistoryPanelView dailyView;
    private final TotalHistoryPanelView totalView;
    private final EmployeeManagementPanelView employeeView;
    private final DepartmentManagementPanelView departmentView;
    private final SettingsPanelView settingsView;

    public MainTabFrameView(String title) {
        mainFrame = new JFrame(title);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dailyView = new DailyHistoryPanelView();
        totalView = new TotalHistoryPanelView();
        employeeView = new EmployeeManagementPanelView();
        departmentView = new DepartmentManagementPanelView();
        settingsView = new SettingsPanelView();

        tabbedPane = new JTabbedPane();

        mainFrame.getContentPane().add(tabbedPane, BorderLayout.CENTER);

        mainFrame.setLocationRelativeTo(null);
        mainFrame.setMinimumSize(new Dimension(650, 250));
        mainFrame.setPreferredSize(new Dimension(800, 400));

        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public DailyHistoryPanelView getDailyView() {
        return dailyView;
    }

    public TotalHistoryPanelView getTotalView() {
        return totalView;
    }

    public EmployeeManagementPanelView getEmployeeView() {
        return employeeView;
    }

    public DepartmentManagementPanelView getDepartmentView() {
        return departmentView;
    }

    public SettingsPanelView getParametersView() {
        return settingsView;
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }
}
