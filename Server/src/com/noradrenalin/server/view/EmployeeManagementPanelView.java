package com.noradrenalin.server.view;

import javax.swing.*;
import java.awt.*;

/**
 * View for EmployeeManagement.
 */
public class EmployeeManagementPanelView {
    private final JPanel mainPanel;
    private final JLabel employeeLabel;
    private final JComboBox<Object> employeeComboBox;
    private final JPanel modifPanel;
    private final JLabel firstNameLabel;
    private final JTextField firstNameTextField;
    private final JLabel lastNameLabel;
    private final JTextField lastNameTextField;
    private final JLabel birthdateLabel;
    private final JTextField birthdateTextField;
    private final JLabel departmentLabel;
    private final JComboBox<Object> departmentComboBox;
    private final JLabel hourCreditLabel;
    private final JButton addButton;
    private final JButton modifyButton;
    private final JButton deleteButton;
    private final JButton planningButton;

    public EmployeeManagementPanelView() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        employeeLabel = new JLabel();
        employeeComboBox = new JComboBox<>();
        modifPanel = new JPanel();
        firstNameLabel = new JLabel();
        firstNameTextField = new JTextField();
        lastNameLabel = new JLabel();
        lastNameTextField = new JTextField();
        birthdateLabel = new JLabel();
        birthdateTextField = new JTextField();
        departmentLabel = new JLabel();
        departmentComboBox = new JComboBox<>();

        // This line sets a "max" size of the departmentComboBox
        departmentComboBox.setPreferredSize(new Dimension(2,25));
        hourCreditLabel = new JLabel();
        addButton = new JButton();
        modifyButton = new JButton();
        deleteButton = new JButton();
        planningButton = new JButton();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning employeeLabel
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 0, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(employeeLabel, c);

        // Positioning employeeComboBox
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 0, 5);
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        mainPanel.add(employeeComboBox, c);

        // Positioning hourCreditLabel
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(hourCreditLabel, c);

        // Positioning modifPanel
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 3;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(5, 5, 5, 5);
        mainPanel.add(modifPanel, c);
        modifPanel.setBorder(BorderFactory.createEtchedBorder());
        modifPanel.setLayout(new GridBagLayout());

        c = new GridBagConstraints();

        // Positioning firstNameLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(firstNameLabel, c);

        // Positioning firstNameTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(firstNameTextField, c);

        // Positioning birthdateLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(birthdateLabel, c);

        // Positioning birthdateTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 0;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 25);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(birthdateTextField, c);

        // Positioning lastNameLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(lastNameLabel, c);

        // Positioning lastNameTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(lastNameTextField, c);

        // Positioning departmentLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(departmentLabel, c);

        // Positioning departmentComboBox
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 1;
        c.weightx = 4;
        c.weighty = 0;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(departmentComboBox, c);

        // Positioning modifyButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_END;
        modifPanel.add(modifyButton, c);

        // Positioning addButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        modifPanel.add(addButton, c);

        // Positioning deleteButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        modifPanel.add(deleteButton, c);

        // Positioning planningButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 4;
        c.gridy = 2;
        c.weightx = 0;
        c.weighty = 0;
        c.insets = new Insets(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_START;
        modifPanel.add(planningButton, c);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JLabel getEmployeeLabel() {
        return employeeLabel;
    }

    public JComboBox<Object> getEmployeeComboBox() {
        return employeeComboBox;
    }

    public JLabel getFirstNameLabel() {
        return firstNameLabel;
    }

    public JTextField getFirstNameTextField() {
        return firstNameTextField;
    }

    public JLabel getLastNameLabel() {
        return lastNameLabel;
    }

    public JTextField getLastNameTextField() {
        return lastNameTextField;
    }

    public JLabel getBirthdateLabel() {
        return birthdateLabel;
    }

    public JTextField getBirthdateTextField() {
        return birthdateTextField;
    }

    public JLabel getDepartmentLabel() {
        return departmentLabel;
    }

    public JComboBox<Object> getDepartmentComboBox() {
        return departmentComboBox;
    }

    public JLabel getHourCreditLabel() {
        return hourCreditLabel;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getModifyButton() {
        return modifyButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }

    public JButton getPlanningButton() {
        return planningButton;
    }
}
