package com.noradrenalin.server.view;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * View for TotalHistoryPanel.
 */
public class TotalHistoryPanelView {
    private final JPanel mainPanel;
    private final JButton addTimeSlotButton;
    private final JLabel searchDateLabel;
    private final JTextField searchDateTextField;
    private final JButton searchDateButton;
    private final JLabel filterLabel;
    private final JComboBox<Object> filterComboBox;
    private final JComboBox<Object> empOrDeptComboBox;
    private final JScrollPane historyPanel;
    private final JTable historyTable;
    private final DefaultTableModel historyModel;

    public TotalHistoryPanelView() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());

        addTimeSlotButton = new JButton();
        searchDateLabel = new JLabel();
        searchDateTextField = new JTextField();
        searchDateButton = new JButton();
        filterLabel = new JLabel();
        filterComboBox = new JComboBox<>();
        empOrDeptComboBox = new JComboBox<>();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning searchDateLabel
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(8, 5, 5, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(searchDateLabel, c);

        // Positioning searchDateTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 6;
        c.insets = new Insets(8, 0, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(searchDateTextField, c);

        // Positioning searchDateButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(8, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(searchDateButton, c);

        // Positioning filterLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 5, 5, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(filterLabel, c);

        // Positioning filterComboBox
        c.gridx = 4;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(filterComboBox, c);

        // Positioning empOrDeptComboBox
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 5;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(5, 0, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(empOrDeptComboBox, c);

        // Positioning historyPanel
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.weighty = 1;
        c.gridwidth = 6;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.PAGE_START;

        // Positioning historyTable
        historyModel = new CustomTableModel();
        historyTable = new JTable(historyModel);
        historyPanel = new JScrollPane(historyTable);

        mainPanel.add(historyPanel, c);

        // Positioning addTimeSlotButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 1;
        c.weighty = 0;
        c.gridwidth = 6;
        c.insets = new Insets(0, 0, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(addTimeSlotButton, c);
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JButton getAddTimeSlotButton() {
        return addTimeSlotButton;
    }

    public JLabel getSearchDateLabel() {
        return searchDateLabel;
    }

    public JTextField getSearchDateTextField() {
        return searchDateTextField;
    }

    public JButton getSearchDateButton() {
        return searchDateButton;
    }

    public JLabel getFilterLabel() {
        return filterLabel;
    }

    public JComboBox<Object> getFilterComboBox() {
        return filterComboBox;
    }

    public JComboBox<Object> getEmpOrDeptComboBox() {
        return empOrDeptComboBox;
    }

    public JTable getHistoryTable() {
        return historyTable;
    }

    public DefaultTableModel getHistoryModel() {
        return historyModel;
    }
}
