package com.noradrenalin.server.view;

import com.noradrenalin.common.network.NetworkConfig;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * JTable model, allows us to store an object as a row in a JTable.
 * It allows us to get the selected row's object directly instead of having to parse Strings.
 */
public class ClientListTableModel extends AbstractTableModel {
    private static final int COLUMN_COUNT = 2;
    private static final String[] HEADER_LIST = {"IP Address", "Port"};

    private List<NetworkConfig> networkConfigs;

    public ClientListTableModel() {
        networkConfigs = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return networkConfigs.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return HEADER_LIST[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object retValue = null;
        NetworkConfig networkConfig = networkConfigs.get(rowIndex);

        switch (columnIndex) {
            case 0 -> retValue = networkConfig.getIp();
            case 1 -> retValue = networkConfig.getPort();
        }

        return retValue;
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public void refresh(List<NetworkConfig> networkConfigs) {
        this.networkConfigs = networkConfigs;
        this.fireTableDataChanged();
    }
}
