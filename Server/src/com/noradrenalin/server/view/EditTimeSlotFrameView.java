package com.noradrenalin.server.view;

import com.noradrenalin.server.model.employee.Employee;

import javax.swing.*;
import java.awt.*;

/**
 * View for EditTimeSlotPanel.
 */
public class EditTimeSlotFrameView {
    private final JFrame mainFrame;
    private final JPanel mainPanel;

    private final JLabel employeeInfoLabel;
    private final JLabel dateInfoLabel;
    private final JLabel employeeLabel;
    private final JLabel dateLabel;
    private final JComboBox<Employee> employeeComboBox;
    private final JTextField dateTextField;
    private final JLabel startTimeLabel;
    private final JTextField startTimeTextField;
    private final JLabel endTimeLabel;
    private final JTextField endTimeTextField;
    private final JButton addButton;
    private final JButton modifyButton;
    private final JButton deleteButton;

    public EditTimeSlotFrameView(String title) {
        mainFrame = new JFrame(title);
        mainFrame.setLayout(new GridBagLayout());
        mainPanel = new JPanel();

        employeeInfoLabel = new JLabel();
        dateInfoLabel = new JLabel();
        employeeLabel = new JLabel();
        dateLabel = new JLabel();
        employeeComboBox = new JComboBox<>();
        dateTextField = new JTextField();
        startTimeLabel = new JLabel();
        startTimeTextField = new JTextField();
        endTimeLabel = new JLabel();
        endTimeTextField = new JTextField();
        addButton = new JButton();
        modifyButton = new JButton();
        deleteButton = new JButton();

        GridBagConstraints c = new GridBagConstraints();

        // Positioning employeeInfoLabel
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(10, 0, 5, 0);
        c.anchor = GridBagConstraints.CENTER;
        mainFrame.add(employeeInfoLabel, c);

        // Positioning dateInfoLabel
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.anchor = GridBagConstraints.CENTER;
        mainFrame.add(dateInfoLabel, c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 4;
        c.weightx = 1;
        c.weighty = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.CENTER;
        mainFrame.add(mainPanel, c);
        mainPanel.setBorder(BorderFactory.createEtchedBorder());
        mainPanel.setLayout(new GridBagLayout());

        c = new GridBagConstraints();

        // Positioning employeeLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 5, 0);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(employeeLabel, c);

        // Positioning employeeComboBox
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(employeeComboBox, c);

        // Positioning dateLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 0;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 5, 5);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(dateLabel, c);

        // Positioning dateTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 0;
        c.weightx = 3;
        c.insets = new Insets(0, 0, 5, 35);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(dateTextField, c);

        // Positioning startTimeLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 0, 5);
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(startTimeLabel, c);

        // Positioning endTimeLabel
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 1;
        c.weightx = 1;
        c.anchor = GridBagConstraints.LINE_END;
        mainPanel.add(endTimeLabel, c);

        // Positioning startTimeTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 1;
        c.insets = new Insets(0, 0, 0, 0);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(startTimeTextField, c);

        // Positioning endTimeTextField
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 4;
        c.gridy = 1;
        c.weightx = 3;
        c.insets = new Insets(0, 0, 0, 35);
        c.anchor = GridBagConstraints.LINE_START;
        mainPanel.add(endTimeTextField, c);

        // Positioning addButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 1;
        c.insets = new Insets(15, 0, 15, 0);
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(addButton, c);

        // Positioning modifyButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 1;
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(modifyButton, c);

        // Positioning deleteButton
        c.fill = GridBagConstraints.NONE;
        c.gridx = 3;
        c.gridy = 2;
        c.weightx = 1;
        c.anchor = GridBagConstraints.CENTER;
        mainPanel.add(deleteButton, c);

        mainFrame.setLocationRelativeTo(null);
        mainFrame.setMinimumSize(new Dimension(560, 250));
        mainFrame.pack();
        mainFrame.setVisible(false);
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public JLabel getEmployeeLabel() {
        return employeeLabel;
    }

    public JLabel getDateLabel() {
        return dateLabel;
    }

    public JLabel getEmployeeInfoLabel() {
        return employeeInfoLabel;
    }

    public JLabel getDateInfoLabel() {
        return dateInfoLabel;
    }

    public JComboBox<Employee> getEmployeeComboBox() {
        return employeeComboBox;
    }

    public JTextField getDateTextField() {
        return dateTextField;
    }

    public JLabel getStartTimeLabel() {
        return startTimeLabel;
    }

    public JTextField getStartTimeTextField() {
        return startTimeTextField;
    }

    public JLabel getEndTimeLabel() {
        return endTimeLabel;
    }

    public JTextField getEndTimeTextField() {
        return endTimeTextField;
    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getModifyButton() {
        return modifyButton;
    }

    public JButton getDeleteButton() {
        return deleteButton;
    }
}
