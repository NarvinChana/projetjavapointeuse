import com.noradrenalin.server.controller.HistoryPanelUtils;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.AbstractMap;

import static org.junit.jupiter.api.Assertions.*;

public class HistoryPanelUtilsTest {
    static Employee employee;

    @BeforeEach
    void initEmployee() throws IncorrectTimeException, TimeSlotOverlapException {
        employee = new Employee(
                0,
                "Jean",
                "Dupont",
                LocalDate.of(1987, 2, 1),
                new Department(0, "Administration"));

        employee.getPlanned().addTimeSlotOnDate(
                LocalDate.of(2018,1,1),
                new TimeSlot(LocalTime.of(8,0), LocalTime.of(12,0)));

        employee.getPlanned().addTimeSlotOnDate(
                LocalDate.of(2018,1,1),
                new TimeSlot(LocalTime.of(14,0), LocalTime.of(18,0)));
    }

    @Test
    void employeeShouldBeStartEarly() throws IncorrectTimeException {
        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> state = HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(
                employee,
                LocalDate.of(2021, 6, 7),
                new TimeSlot(LocalTime.of(7, 40), LocalTime.of(12, 0)));

        assertEquals(HistoryPanelUtils.EmployeeState.EarlyStart, state.getKey());
        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getValue());
    }

    @Test
    void employeeShouldBeStartLate() throws IncorrectTimeException {
        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> state = HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(
                employee,
                LocalDate.of(2021, 6, 7),
                new TimeSlot(LocalTime.of(8, 15), LocalTime.of(12, 0)));

        assertEquals(HistoryPanelUtils.EmployeeState.LateStart, state.getKey());
        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getValue());
    }

    @Test
    void employeeShouldBeEndEarly() throws IncorrectTimeException {
        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> state = HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(
                employee,
                LocalDate.of(2021, 6, 7),
                new TimeSlot(LocalTime.of(8, 0), LocalTime.of(11, 45)));

        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getKey());
        assertEquals(HistoryPanelUtils.EmployeeState.EarlyEnd, state.getValue());
    }

    @Test
    void employeeShouldBeEndLate() throws IncorrectTimeException {
        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> state = HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(
                employee,
                LocalDate.of(2021, 6, 7),
                new TimeSlot(LocalTime.of(8, 0), LocalTime.of(12, 15)));

        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getKey());
        assertEquals(HistoryPanelUtils.EmployeeState.LateEnd, state.getValue());
    }

    @Test
    void employeeShouldNotBeLate() throws IncorrectTimeException {
        AbstractMap.SimpleEntry<HistoryPanelUtils.EmployeeState, HistoryPanelUtils.EmployeeState> state = HistoryPanelUtils.checkIfEmployeeIsLateOrEarly(
                employee,
                LocalDate.of(2021, 6, 7),
                new TimeSlot(LocalTime.of(8, 0), LocalTime.of(12, 0)));

        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getKey());
        assertEquals(HistoryPanelUtils.EmployeeState.NotLate, state.getValue());
    }
}
