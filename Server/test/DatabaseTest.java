import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IdAlreadyTakenException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.database.Database;
import com.noradrenalin.server.model.database.DatabaseListener;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import org.junit.jupiter.api.*;
import org.mockito.Mock;

import java.time.LocalDate;
import static org.junit.jupiter.api.Assertions.*;


public class DatabaseTest {

    static Database database;
    static Department department;
    static Employee employee;

    @BeforeEach
    void initDatabase() throws IdAlreadyTakenException, IncorrectTimeException {
        department = new Department(0, "Administration");
        employee = new Employee(0, "Jean", "Dupont", LocalDate.of(1987,2,1), department);
        database = new Database();
        database.addDepartment(department);
        database.addEmployee(employee);
    }

    @Test
    void addEmployeeShouldSuccess() throws IncorrectTimeException, IdAlreadyTakenException {
        Employee e = new Employee(1, "Jeanne", "Dupont", LocalDate.of(1987,2,1), department);
        database.addEmployee(e);

        assertEquals(database.getEmployees().size(), 2);
    }

    @Test
    void addEmployeeShouldThrowException() throws IncorrectTimeException {
        Employee e = new Employee(0, "Jean", "Dupont", LocalDate.of(1987,2,1), department);

        assertThrows(IdAlreadyTakenException.class,() -> database.addEmployee(e));
    }

    @Test
    void removeEmployeeShouldSuccess() throws ElementNotFoundException {
        database.removeEmployee(employee);

        assertEquals(database.getEmployees().size(), 0);
    }

    @Test
    void removeEmployeeShouldThrowException() throws IncorrectTimeException {
        Employee e = new Employee(1, "Jeanne", "Dupont", LocalDate.of(1987,2,1), department);

        assertThrows(ElementNotFoundException.class,() -> database.removeEmployee(e));
    }

    @Test
    void addDepartmentShouldSuccess() throws IdAlreadyTakenException {
        Department d = new Department(1, "Finance");

        database.addDepartment(d);
        assertEquals(database.getDepartments().get(1), d);
    }

    @Test
    void addDepartmentShouldThrowException() {
        Department d = new Department(0, "Administration");

        assertThrows(IdAlreadyTakenException.class,() -> database.addDepartment(d));
    }

    @Test
    void removeDepartmentShouldSuccess() throws ElementNotFoundException {
        database.removeEmployee(employee);

        assertEquals(database.getEmployees().size(), 0);
    }

    @Test
    void removeDepartmentShouldThrowException() {
        Department d = new Department(1, "Finance");

        assertThrows(ElementNotFoundException.class,() -> database.removeDepartment(d));
    }

    @Test
    void getDepartmentByIdShouldSuccess() {
        Department d = database.getDepartmentById(0);

        assertEquals(department, d);
    }

    @Test
    void getDepartmentByIdShouldReturnNull() {
        Department d = database.getDepartmentById(1);

        assertNull(d);
    }

    @Test
    void versionCodeShouldIncrement() throws IncorrectTimeException, IdAlreadyTakenException {
        long oldVersionCode = database.getVersionCode();
        database.addEmployee(new Employee(1, "Jeanne", "Dupont", LocalDate.of(1987, 1, 1), department));
        assertNotEquals(oldVersionCode, database.getVersionCode());
    }

    @Test
    void listenerShouldTrigger() throws IncorrectTimeException, IdAlreadyTakenException {
        final boolean[] triggered = {false};

        database.addListener(new DatabaseListener() {
            @Override
            public void versionCodeUpdated(Database d, long oldCode, long newCode) {
                triggered[0] = true;
            }

            @Override
            public void departmentsListSizeChanged(Database d, int newSize) {

            }
        });

        database.addEmployee(new Employee(1, "Jeanne", "Dupont", LocalDate.of(1987, 1, 1), department));

        assertTrue(triggered[0]);
    }
}
