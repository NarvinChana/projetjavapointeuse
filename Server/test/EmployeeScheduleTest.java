import com.noradrenalin.server.model.ElementNotFoundException;
import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.TimeSlotOverlapException;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import com.noradrenalin.server.model.schedule.TimeSlot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeScheduleTest {
    Employee employee;

    @BeforeEach
    void initEmployee() throws IncorrectTimeException, TimeSlotOverlapException {
        employee = new Employee(
                0,
                "Jean",
                "Dupont",
                LocalDate.of(1987, 2, 1),
                new Department(0, "Administration"));

        employee.getPlanned().addTimeSlotOnDate(
                LocalDate.of(2018,1,1),
                new TimeSlot(LocalTime.of(8,0), LocalTime.of(12,0)));

        employee.getPlanned().addTimeSlotOnDate(
                LocalDate.of(2018,1,1),
                new TimeSlot(LocalTime.of(14,0), LocalTime.of(18,0)));
    }

    @Test
    void addPunchShouldAddTimeSlot() throws TimeSlotOverlapException, IncorrectTimeException, ElementNotFoundException {
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(1987, 2, 1), LocalTime.of(8, 0)));
        assertEquals(1, employee.getActual().getTimeSlotsOfDate(LocalDate.of(1987, 2, 1)).size());
    }

    @Test
    void addPunchShouldModifyTimeSlot() throws TimeSlotOverlapException, IncorrectTimeException, ElementNotFoundException {
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(1987, 2, 1), LocalTime.of(8, 0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(1987, 2, 1), LocalTime.of(18, 0)));
        assertEquals(1, employee.getActual().getTimeSlotsOfDate(LocalDate.of(1987, 2, 1)).size());
        assertEquals(18, employee.getActual().getTimeSlotsOfDate(LocalDate.of(1987, 2, 1)).get(0).getEndTime().getHour());
    }

    @Test
    void hourCreditShouldBePositive() throws TimeSlotOverlapException, IncorrectTimeException {
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(8,0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(18,0)));

        assertEquals(120, employee.getMinutesCredit());
    }

    @Test
    void hourCreditShouldBeZero() throws TimeSlotOverlapException, IncorrectTimeException {
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(8,0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(12,0)));

        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(14,0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(18,0)));

        assertEquals(0, employee.getMinutesCredit());
    }

    @Test
    void hourCreditShouldBeNegative() throws TimeSlotOverlapException, IncorrectTimeException {
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(7,0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(11,0)));

        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(15,0)));
        employee.addPunchIn(LocalDateTime.of(LocalDate.of(2021, 6, 7), LocalTime.of(17,0)));

        assertEquals(-120, employee.getMinutesCredit());
    }
}
