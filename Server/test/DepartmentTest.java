import com.noradrenalin.server.model.department.Department;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest {
    @Test
    void departmentEqualsTrue() {
        Department d1 = new Department(0, "Administration");
        Department d2 = new Department(0, "Administration");

        assertEquals(d1, d2);
    }

    @Test
    void departmentEqualsFalse() {
        Department d1 = new Department(0, "Administration");
        Department d2 = new Department(1, "Finance");

        assertNotEquals(d1, d2);
    }
}
