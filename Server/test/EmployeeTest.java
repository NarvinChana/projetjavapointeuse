import com.noradrenalin.server.model.IncorrectTimeException;
import com.noradrenalin.server.model.department.Department;
import com.noradrenalin.server.model.employee.Employee;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {
    @Test
    void employeeEqualsTrue() throws IncorrectTimeException {
        Department d = new Department(0, "Administration");
        Employee e1 = new Employee(0, "Jean", "Dupont", LocalDate.of(1984, 2, 1), d);
        Employee e2 = new Employee(0, "Jean", "Dupont", LocalDate.of(1984, 2, 1), d);

        assertEquals(e1, e2);
    }

    @Test
    void employeeEqualsFalse() throws IncorrectTimeException {
        Department d = new Department(0, "Administration");
        Employee e1 = new Employee(0, "Jean", "Dupont", LocalDate.of(1984, 2, 1), d);
        Employee e2 = new Employee(1, "Jeanne", "Dupont", LocalDate.of(1984, 2, 1), d);

        assertNotEquals(e1, e2);
    }
}
