Noradrenalin :
Narvin Chana
Antonio Gomès
Alexandre Millard
Noé Pécault

There are three modules in our project : 
Client (Punch In Clock), Server (Main Application) and Common which contains the dependencies of Client and Server.
In Client, the main controller is PunchClockController.
In Server, the main controller is MainTabFrameController.

In Common, the main controller is TCPController. TCPController possesses the threads that allows it to manage incoming and outgoing packets. It relays them to the controller that possesses it with listeners.

The main structure of the server and the client is as follows : -> Launch main controller in main method. -> Initialise all data and views. -> wait for interaction from user

While all this happens, the TCPController of each side will maintain communications and keep the database updated.

One important thing to know is that the client takes the path of its save file as argument when it is launched (example : "java .\Client client-save.ser").
If multiple clients are launched it is recommended to launch them with different save file paths.
The program was tested on the latest version of Java (Java 16). 
Java 15 could work and using a java version under 15 will not work.