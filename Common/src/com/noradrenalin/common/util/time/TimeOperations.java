package com.noradrenalin.common.util.time;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public final class TimeOperations {
    private TimeOperations(){}

    public static LocalTime roundToNearestQuarterHour(LocalTime timeParam) {
        LocalTime roundedNewTime = timeParam.truncatedTo(ChronoUnit.HOURS);

        int modulo = timeParam.getMinute() % 15;
        if (modulo >= 8) {
            roundedNewTime = roundedNewTime.plusMinutes(timeParam.getMinute() + 15 - modulo);
        } else {
            roundedNewTime = roundedNewTime.plusMinutes(timeParam.getMinute() - modulo);
        }
        return roundedNewTime;
    }
}
