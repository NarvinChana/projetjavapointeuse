package com.noradrenalin.common.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Stores the simplified com.noradrenalin.common.model needed by the client.
 */
public class ClientDatabase implements Serializable {
    private long versionCode;
    private ArrayList<ClientEmployee> employees;

    /**
     * Default constructor of the ClientDatabase.
     *
     * It initializes the database in an incorrect state with an empty list of Employees and a version code of -1."
     */
    public ClientDatabase() {
        versionCode = -1;
        employees = new ArrayList<>();
    }

    /**
     * Modifies the Employee list of the local database and the version code.
     * @param employees The new Employee list
     * @param versionCode The version code associated with the new data
     */
    public void setEmployeesWithVersion(ArrayList<ClientEmployee> employees, long versionCode) {
        this.versionCode = versionCode;
        this.employees = employees;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public ArrayList<ClientEmployee> getEmployees() {
        return employees;
    }
}
