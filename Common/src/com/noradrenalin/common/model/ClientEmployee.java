package com.noradrenalin.common.model;

import java.io.Serializable;

/**
 * Lightweight version of the Employee class.
 */
public class ClientEmployee implements Serializable {
    private final long id;
    private final String firstName;
    private final String lastName;

    /**
     * Default constructor of ClientEmployee.
     * @param id Employee's ID
     * @param fName Employee's first name
     * @param lName Employee's last name
     */
    public ClientEmployee(long id, String fName, String lName) {
        this.id = id;
        firstName = fName;
        lastName = lName;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return id + " - " + getLastName() + " " + getFirstName();
    }
}
