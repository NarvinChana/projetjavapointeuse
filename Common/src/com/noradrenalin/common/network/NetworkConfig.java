package com.noradrenalin.common.network;

import java.io.Serializable;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Objects;

/**
 * Stores the client's ip address and port.
 */
public class NetworkConfig implements Serializable {
    private Inet4Address ip;
    private int port;

    public NetworkConfig() throws UnknownHostException {
        setIpAddress("127.0.0.1");
        setPort(2323);
    }

    public NetworkConfig(int port) {
        setPort(port);
    }

    public NetworkConfig(String ip, int port) throws UnknownHostException {
        setIpAddress(ip);
        setPort(port);
    }

    public NetworkConfig(Inet4Address ip, int port) {
        setIpAddress(ip);
        setPort(port);
    }

    public Inet4Address getIp() {
        return ip;
    }

    public String getIpAsString() {
        return ip.getHostAddress();
    }

    public void setIpAddress(Inet4Address ipAddress) {
        ip = ipAddress;
    }

    public void setIpAddress(String ipAddress) throws UnknownHostException {
        ip = (Inet4Address) Inet4Address.getByName(ipAddress);
    }

    public int getPort() {
        return port;
    }

    public String getPortAsString() {
        return String.valueOf(port);
    }

    /**
     * Checks if the port is valid.
     * @param port Must be between 0 and 2^16 otherwise IllegalArgumentException is thrown.
     */
    public void setPort(Integer port) {
        if (port > 65536 || port < 0)
        {
            throw new IllegalArgumentException("Port number " + port + " is not a valid port number.");
        }
        else {
            this.port = port;
        }
    }

    @Override
    public String toString() {
        return "ip=" + ip + ", port=" + port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NetworkConfig that = (NetworkConfig) o;
        return getPort() == that.getPort() && Objects.equals(getIp(), that.getIp());
    }
}
