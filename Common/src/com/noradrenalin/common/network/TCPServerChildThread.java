package com.noradrenalin.common.network;

import com.noradrenalin.common.network.packets.Packet;
import com.noradrenalin.common.network.packets.listeners.PacketListener;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Inet4Address;
import java.net.Socket;

/**
 * Accepts an incoming socket connection. Thread closes after it has fired the listener to the main.
 */
public class TCPServerChildThread extends Thread {
    protected Socket socket;
    protected ObjectInputStream in = null;
    protected PacketListener pListener;

    public TCPServerChildThread(Socket clientSocket, PacketListener pListener) {
        socket = clientSocket;
        this.pListener = pListener;
    }

    @Override
    public void run() {
        try {
            in = new ObjectInputStream(socket.getInputStream());

            Packet packet = (Packet) in.readObject();

            if (pListener != null) {
                pListener.packetReceived(packet, new NetworkConfig((Inet4Address) socket.getInetAddress(), socket.getLocalPort()));
            }

            in.close();
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioException) {
                    System.out.println("In stream close failed.");
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ioException) {
                    System.out.println("Socket close failed. Hopefully this won't cause a problem elsewhere!");
                }
            }
            System.out.println("Packet receive error, connection closed.");
        }
    }
}
