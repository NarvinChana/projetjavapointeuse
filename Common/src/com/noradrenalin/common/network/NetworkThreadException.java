package com.noradrenalin.common.network;

import com.noradrenalin.common.network.packets.Packet;

/**
 * Exception to be raised when a networkThread encounters a problem. Can contain a Packet and a NetworkConfig.
 */
public class NetworkThreadException extends Exception {
    public enum Type {
        ActiveSocketOpen,
        PassiveSocketOpen,
        SocketClose,
        SocketAccept,
        PacketSend
    }

    private final Packet packet;
    private final NetworkConfig networkConfig;
    private final Type type;

    public NetworkThreadException(Packet p, NetworkConfig n, Type t) {
        packet = p;
        networkConfig = n;
        type = t;
    }

    public Packet getPacket() {
        return packet;
    }

    public NetworkConfig getNetworkConfig() {
        return networkConfig;
    }

    public Type getType() {
        return type;
    }
}
