package com.noradrenalin.common.network.packets;

import java.io.Serializable;

/**
 * Represents a generic packet that can be serialized or sent to the server.
 */
public class Packet implements Serializable {
    private final long packetId;
    private final int port;

    public Packet(long id) {
        this(id, 0);
    }
    public Packet(long id, int port) {
        this.packetId = id;
        this.port = port;
    }

    public long getId() {
        return packetId;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "packetId=" + packetId +
                ", port=" + port +
                '}';
    }
}