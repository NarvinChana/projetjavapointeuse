package com.noradrenalin.common.network.packets;

import com.noradrenalin.common.model.ClientDatabase;

import java.io.Serializable;

/**
 * Represents a database packet that can be serialized or sent to the server.
 */
public class DatabasePacket extends Packet implements Serializable {

    private final ClientDatabase database;

    public DatabasePacket(long packetId, int port, ClientDatabase database) {
        super(packetId, port);
        this.database = database;
    }

    public ClientDatabase getDatabase() {
        return database;
    }
}
