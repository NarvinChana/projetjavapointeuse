package com.noradrenalin.common.network.packets.listeners;

import com.noradrenalin.common.network.packets.PunchClockPacket;

/**
 * Listens for the reception of a punch clock packet.
 */
public interface PunchClockPacketListener extends PacketListener {
    void punchClockPacketReceived(PunchClockPacket packet);
}
