package com.noradrenalin.common.network.packets.listeners;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.packets.PingPacket;

/**
 * Listens for the reception of a ping packet.
 */
public interface PingPacketListener extends PacketListener {
    void pingPacketReceived(PingPacket packet, NetworkConfig networkConfig);
}
