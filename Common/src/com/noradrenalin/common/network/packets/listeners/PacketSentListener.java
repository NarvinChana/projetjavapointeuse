package com.noradrenalin.common.network.packets.listeners;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.packets.Packet;

/**
 * Listens for the sending of a packet.
 */
public interface PacketSentListener {
    void packetSent(Packet packet, NetworkConfig netConf);
}
