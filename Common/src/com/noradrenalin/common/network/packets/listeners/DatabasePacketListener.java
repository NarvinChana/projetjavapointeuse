package com.noradrenalin.common.network.packets.listeners;

import com.noradrenalin.common.network.packets.DatabasePacket;

/**
 * Listens for the reception of a Database packet.
 */
public interface DatabasePacketListener extends PacketListener {
    void databasePacketReceived (DatabasePacket dbPacket);
}
