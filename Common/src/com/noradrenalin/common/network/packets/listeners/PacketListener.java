package com.noradrenalin.common.network.packets.listeners;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.packets.Packet;

import java.util.EventListener;

/**
 * Listens for the reception of a generic packet.
 */
public interface PacketListener extends EventListener {
    void packetReceived(Packet packet, NetworkConfig sender);
}
