package com.noradrenalin.common.network.packets;

import java.io.Serializable;

/**
 * Represents a ping that can be serialized or sent to the server.
 */
public class PingPacket extends Packet implements Serializable {
    private final long versionCode;

    public PingPacket(long id, int port, long versionCode) {
        super(id, port);
        this.versionCode = versionCode;
    }

    public long getVersionCode() {
        return versionCode;
    }
}
