package com.noradrenalin.common.network.packets;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Represents a punch-in/out that can be serialized or sent to the server.
 */
public class PunchClockPacket extends Packet implements Serializable {

    private final long versionCode;
    private final long employeeId;
    private final LocalDateTime time;

    /**
     * Default constructor of a PunchClockPacket
     * @param employeeId Employee's ID who punched-in/out
     * @param t Date and time of the punch-in/out
     */
    public PunchClockPacket(long packetId, int port, long versionCode, long employeeId, LocalDateTime t) {
        super(packetId, port);
        this.versionCode = versionCode;
        this.employeeId = employeeId;
        this.time = t;
    }

    public long getVersionCode() {
        return versionCode;
    }

    public long getEmployeeId() {
        return employeeId;
    }

    public LocalDateTime getTime() {
        return time;
    }
}