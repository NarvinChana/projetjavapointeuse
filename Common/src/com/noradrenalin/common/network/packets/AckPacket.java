package com.noradrenalin.common.network.packets;

import java.io.Serializable;

/**
 * Represents an acknowledgement packet that can be serialized or sent to the server.
 */
public class AckPacket extends Packet implements Serializable {
    private final long receivedPacketId;

    public AckPacket(long id, long receivedPacketId) {
        super(id);
        this.receivedPacketId = receivedPacketId;
    }

    public long getReceivedPacketId() {
        return receivedPacketId;
    }
}
