package com.noradrenalin.common.network;

import com.noradrenalin.common.network.packets.Packet;
import com.noradrenalin.common.network.packets.listeners.PacketTimeoutListener;

import java.time.Duration;
import java.time.LocalTime;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Thread that checks if the acknowledgment packet of each sent packet has been received.
 */
public class TCPTimeoutRunnable implements Runnable {
    private static final long TIMEOUT_CHECK_DELAY = 1000;
    private final int TIMEOUT_DURATION = 15;

    private ConcurrentHashMap<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime> checkAckTimeout;
    private PacketTimeoutListener packetTimeoutListener = null;

    private final boolean running;

    public TCPTimeoutRunnable() {
        checkAckTimeout = new ConcurrentHashMap<>();
        running = true;
    }

    /**
     * Checks if any packets in checkAckTimeout have passed timeout duration.
     * If no packets exist in checkAckTimeout the thread waits.
     */
    @Override
    public void run() {

        while (running) {
            if (checkAckTimeout.isEmpty()) {
                try {
                    synchronized (this) {
                        this.wait();
                    }
                } catch (InterruptedException e) {
                    System.out.println("Timeout thread wakes up.");
                }
            }
            for (Map.Entry<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime> entry : checkAckTimeout.entrySet()) {
                long packetID = entry.getKey().getKey().getId();
                if (Duration.between(entry.getValue(), LocalTime.now()).toSeconds() >= TIMEOUT_DURATION) {
                    removeElementFromHash(packetID);
                    packetTimeoutListener.packetTimeout(entry.getKey());
                }
            }

            try {
                Thread.sleep(TIMEOUT_CHECK_DELAY);
            } catch (InterruptedException e) {
                // We print the stack trace here as this can only occur if a ctrl+c is performed
                e.printStackTrace();
            }
        }
    }

    public void setListener(PacketTimeoutListener ptListener) {
        packetTimeoutListener = ptListener;
    }

    /**
     * Adds an entry to the list of packets that are waiting for an acknowledgement.
     * Wakes up the thread if it was sleeping.
     */
    public synchronized void addElementToHash (AbstractMap.SimpleEntry<Packet, NetworkConfig> duo, LocalTime lt) {
        this.notify();
        checkAckTimeout.put(duo, lt);
    }

    /**
     * Removes an element from the ConcurrentHashMap.
     * @param receivedPacketId key to remove from the ConcurrentHashMap
     */
    public synchronized void removeElementFromHash (long receivedPacketId) {
        for (Map.Entry<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime> entry : checkAckTimeout.entrySet()) {
            if (entry.getKey().getKey().getId() == receivedPacketId) {
                checkAckTimeout.remove(entry.getKey());
            }
        }
    }

    /**
     * Returns an unmodifiable map of the timeouts.
     */
    public Map<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime> getTimeouts() {
        return Collections.unmodifiableMap(checkAckTimeout);
    }

    /**
     * Sets the current packet waiting for acknowledgment.
     */
    public void setTimeouts(ConcurrentHashMap<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime> timeouts) {
        checkAckTimeout = timeouts;
    }
}
