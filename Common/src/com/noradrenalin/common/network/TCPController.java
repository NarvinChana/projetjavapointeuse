package com.noradrenalin.common.network;

import com.noradrenalin.common.collections.CircularList;
import com.noradrenalin.common.network.listeners.PacketMaxRetryListener;
import com.noradrenalin.common.network.listeners.TCPErrorListener;
import com.noradrenalin.common.network.packets.*;
import com.noradrenalin.common.network.packets.listeners.*;

import java.io.*;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Controller that manages the entire network connection process.
 * Creates child threads to send and receive packets.
 * Can communicate simultaneously with multiple networks.
 */
public class TCPController implements Serializable, PacketListener, PacketTimeoutListener, PacketSentListener {
    private static final int HISTORY_SIZE = 16;
    private static final int SEND_QUEUE_SIZE = 1024;
    private static final int TIMEOUT_MAX_COUNT = 5;

    private transient long lastPacketID;

    // Listener lists
    private List<DatabasePacketListener> databasePacketListeners;
    private List<PingPacketListener> pingPacketListeners;
    private List<PunchClockPacketListener> punchClockPacketListeners;
    private List<PacketListener> packetListeners;
    private List<TCPErrorListener> tcpErrorListener;
    private List<PacketMaxRetryListener> maxTryListeners;

    // Queue is read element by element by sendRunnable and each packet is sent to server with netconf
    private BlockingQueue<AbstractMap.SimpleEntry<Packet, NetworkConfig>> queue;
    private CircularList<Long> lastReceivedPackets;
    private Map<Long, Integer> timeoutCounter;

    private NetworkConfig netConf;
    private NetworkConfig localServerNetConf;

    private transient TCPServerRunnable receiveRunnable;
    private transient TCPClientRunnable sendRunnable;
    private transient TCPTimeoutRunnable timeoutRunnable;

    /**
     * Initializes the TCPController.
     * @param netConf IP and port of far-away server
     * @param localServerNetConf Port of local server
     */
    public TCPController(NetworkConfig netConf, NetworkConfig localServerNetConf) {
        lastPacketID = new Random(LocalTime.now().getNano()).nextInt();

        databasePacketListeners = new ArrayList<>();
        pingPacketListeners = new ArrayList<>();
        punchClockPacketListeners = new ArrayList<>();
        packetListeners = new ArrayList<>();
        tcpErrorListener = new ArrayList<>();
        maxTryListeners = new ArrayList<>();

        queue = new ArrayBlockingQueue<>(SEND_QUEUE_SIZE);
        lastReceivedPackets = new CircularList<>(Long.class, HISTORY_SIZE);
        timeoutCounter = new HashMap<>();

        receiveRunnable = new TCPServerRunnable(localServerNetConf);
        receiveRunnable.setPListener(this);

        sendRunnable = new TCPClientRunnable(netConf, queue);
        sendRunnable.setPacketSentListener(this);

        timeoutRunnable = new TCPTimeoutRunnable();
        timeoutRunnable.setListener(this);

        startReceiveThread();
        startSendThread();
        startTimeoutThread();

        this.netConf = netConf;
        this.localServerNetConf = localServerNetConf;
    }

    public long getPacketID() {
        return lastPacketID++;
    }

    /**
     * Manages the adding of a packet to the send thread, while taking into account potential errors.
     * Also adds timer to check the timeout of acknowledgement reception.
     * @param packet Duo composed of a Packet and a NetworkConfig
     * @param netConf Network Config of the destination
     */
    public void sendPacket(Packet packet, NetworkConfig netConf) {
        queue.add(new AbstractMap.SimpleEntry<>(packet, netConf));
    }

    /**
     * Callback that is called when the acknowledgment of the Packet has not been received.
     */
    @Override
    public synchronized void packetTimeout(AbstractMap.SimpleEntry<Packet, NetworkConfig> networkDuo) {
        long packetID = networkDuo.getKey().getId();

        int count;
        if (timeoutCounter.containsKey(packetID)) {
            count = timeoutCounter.get(packetID);
            count++;
            timeoutCounter.replace(packetID, count);
        } else {
            count = 1;
            timeoutCounter.put(packetID, 1);
        }

        if (count < TIMEOUT_MAX_COUNT) {
            sendPacket(networkDuo.getKey(), networkDuo.getValue());
        } else {
            fireMaxTryListener(networkDuo.getKey(), networkDuo.getValue());
            timeoutCounter.remove(packetID);
        }
    }

    /**
     * Callback that is called when a Packet is successfully sent.
     * @param packet Duo composed of a Packet and a NetworkConfig
     * @param netConf Network Config of the destination
     */
    @Override
    public void packetSent(Packet packet, NetworkConfig netConf) {
        if (!(packet instanceof AckPacket)) timeoutRunnable.addElementToHash(new AbstractMap.SimpleEntry<>(packet, netConf), LocalTime.now());
        System.out.println("Packet sent :" + packet + " " + netConf);
    }

    // Listener accessors
    /**
     * Adds a listener that is triggered when a DatabasePacket is received.
     */
    public void addListener(DatabasePacketListener listener) {
        databasePacketListeners.add(listener);
    }

    /**
     * Adds a listener that is triggered when a PingPacket is received.
     */
    public void addListener(PingPacketListener listener) {
        pingPacketListeners.add(listener);
    }

    /**
     * Adds a listener that is triggered when a PunchClockPacket is received.
     */
    public void addListener(PunchClockPacketListener listener) {
        punchClockPacketListeners.add(listener);
    }

    /**
     * Adds a listener that is triggered when any type of Packet is received.
     */
    public void addListener(PacketListener listener) {
        packetListeners.add(listener);
    }

    /**
     * Adds a listener that is triggered when a TCP error occurs.
     */
    public void addListener(TCPErrorListener listener) {
        tcpErrorListener.add(listener);
    }

    /**
     * Adds a listener that is triggered when a Packet exceeded the max number of tries.
     */
    public void addListener(PacketMaxRetryListener listener) {
        maxTryListeners.add(listener);
    }

    /**
     * Called when a packet is received. Determines the action to take next and fires the corresponding listeners.
     * @param packet Packet received
     * @param sender The address of the packet's sender
     */
    @Override
    public void packetReceived(Packet packet, NetworkConfig sender) {
        if (!lastReceivedPackets.contains(packet.getId())) {
            if (packet instanceof AckPacket) {
                timeoutRunnable.removeElementFromHash(((AckPacket) packet).getReceivedPacketId());
                firePacketReceived(packet, sender);
            } else {
                if (packet instanceof DatabasePacket) {
                    firePacketReceived((DatabasePacket) packet, sender);
                } else if (packet instanceof PingPacket) {
                    firePacketReceived((PingPacket) packet, sender);
                } else if (packet instanceof PunchClockPacket) {
                    firePacketReceived((PunchClockPacket) packet, sender);
                } else {
                    firePacketReceived(packet, sender);
                }

                AckPacket ackPacket = new AckPacket(getPacketID(), packet.getId());
                sendPacket(ackPacket, new NetworkConfig(sender.getIp(), packet.getPort()));
            }
            lastReceivedPackets.add(packet.getId());
        }
    }

    /**
     * Changes the server's passive socket port.
     * @param netConf Contains new port
     */
    public synchronized void changeServerPort(NetworkConfig netConf)  {
        receiveRunnable.closeServerSocket();
        receiveRunnable = new TCPServerRunnable(netConf);
        receiveRunnable.setPListener(this);
        startReceiveThread();
    }

    /**
     * Triggers all registered listeners when a DatabasePacket is received.
     * @param packet packet received
     * @param sender sender of packet received
     */
    protected void firePacketReceived(DatabasePacket packet, NetworkConfig sender) {
        for (DatabasePacketListener l : databasePacketListeners) {
            l.packetReceived(packet, sender);
            l.databasePacketReceived(packet);
        }
    }

    /**
     * Triggers all registered listeners when a PingPacket is received.
     * @param packet packet received
     * @param sender sender of packet received
     */
    protected void firePacketReceived(PingPacket packet, NetworkConfig sender) {
        for (PingPacketListener l : pingPacketListeners) {
            l.packetReceived(packet, sender);
            l.pingPacketReceived(packet, sender);
        }
    }

    /**
     * Triggers all registered listeners when a PunchClockPacket is received.
     * @param packet packet received
     * @param sender sender of packet received
     */
    protected void firePacketReceived(PunchClockPacket packet, NetworkConfig sender) {
        for (PunchClockPacketListener l : punchClockPacketListeners) {
            l.packetReceived(packet, sender);
            l.punchClockPacketReceived(packet);
        }
    }

    /**
     * Triggers all registered listeners when any type of Packet is received.
     * @param packet packet received
     * @param sender sender of packet received
     */
    protected void firePacketReceived(Packet packet, NetworkConfig sender) {
        for (PacketListener l : packetListeners) {
            l.packetReceived(packet, sender);
        }
    }

    /**
     * Triggers all registered listeners when an error has been thrown by a network thread.
     * @param e Exception raised
     */
    protected void fireErrorListener(Exception e) {
        for (TCPErrorListener l : tcpErrorListener) {
            l.tcpErrorOccurred(e);
        }
    }

    /**
     * Triggers all registered listeners when an error has been thrown by a network thread.
     */
    protected void fireMaxTryListener(Packet packet, NetworkConfig dest) {
        for (PacketMaxRetryListener l : maxTryListeners) {
            l.packetMaxRetry(packet, dest);
        }
    }

    /**
     * Starts the sendRunnable thread.
     */
    private void startSendThread() {
        // Handles the sendRunnable thread's exceptions
        Thread.UncaughtExceptionHandler sendExceptionHandler = (t, e) -> {
            NetworkThreadException exception = (NetworkThreadException) e.getCause();

            fireErrorListener(exception);

            switch (exception.getType()) {
                case ActiveSocketOpen -> {
                    System.err.println("Socket could not be opened. Retrying.");
                    System.err.println(exception.getPacket() + " " + exception.getNetworkConfig());
                    packetTimeout(new AbstractMap.SimpleEntry<>(exception.getPacket(), exception.getNetworkConfig()));
                }
                case SocketClose -> sendRunnable.setSocketToNull();
                case PacketSend -> sendPacket(exception.getPacket(), exception.getNetworkConfig());
            }
            startSendThread();
        };

        Thread sendThread = new Thread(sendRunnable);
        sendThread.setUncaughtExceptionHandler(sendExceptionHandler);
        sendThread.start();
    }

    /**
     * Starts the receiveRunnable thread.
     */
    private void startReceiveThread() {
        // Handles the receiveRunnable thread's exceptions
        Thread.UncaughtExceptionHandler receiveExceptionHandler = (t, e) -> {
            NetworkThreadException exception = (NetworkThreadException) e.getCause();

            fireErrorListener(exception);

            switch (exception.getType()) {
                case PassiveSocketOpen -> {
                    System.err.println("Server was unable to open on port :" + receiveRunnable.getNetConf().getPortAsString());
                    startReceiveThread();
                }
                case SocketAccept -> {
                    System.err.println("Server was unable to accept port and/or create server child thread.");
                    startReceiveThread();
                }
            }
        };

        Thread receiveThread = new Thread(receiveRunnable);
        receiveThread.setUncaughtExceptionHandler(receiveExceptionHandler);
        receiveThread.start();
    }

    /**
     * Starts the timeoutRunnable thread.
     */
    private void startTimeoutThread() {
        // Handles the timeoutRunnable thread's exceptions
        Thread.UncaughtExceptionHandler timeoutExceptionHandler = (t, e) -> {
            // Timeout does not create any exceptions. This was kept here in case any exceptions are added in the future.
        };

        Thread timeoutThread = new Thread(timeoutRunnable);
        timeoutThread.setUncaughtExceptionHandler(timeoutExceptionHandler);
        timeoutThread.start();
    }

    /**
     * Deserializes the network controller and launches the threads.
     */
    @Serial
    private void readObject(ObjectInputStream aInputStream) throws ClassNotFoundException, IOException
    {
        lastPacketID = new Random(LocalTime.now().getNano()).nextInt();

        databasePacketListeners = new ArrayList<>();
        pingPacketListeners = new ArrayList<>();
        punchClockPacketListeners = new ArrayList<>();
        packetListeners = new ArrayList<>();
        tcpErrorListener = new ArrayList<>();
        maxTryListeners = new ArrayList<>();

        queue = (BlockingQueue<AbstractMap.SimpleEntry<Packet, NetworkConfig>>) aInputStream.readObject();
        lastReceivedPackets = (CircularList<Long>) aInputStream.readObject();
        timeoutCounter = new HashMap<>();

        this.localServerNetConf = (NetworkConfig) aInputStream.readObject();
        this.netConf = (NetworkConfig) aInputStream.readObject();

        receiveRunnable = new TCPServerRunnable(localServerNetConf);
        receiveRunnable.setPListener(this);

        sendRunnable = new TCPClientRunnable(netConf, queue);
        sendRunnable.setPacketSentListener(this);

        timeoutRunnable = new TCPTimeoutRunnable();
        timeoutRunnable.setListener(this);

        timeoutRunnable.setTimeouts((ConcurrentHashMap<AbstractMap.SimpleEntry<Packet, NetworkConfig>, LocalTime>) aInputStream.readObject());

        startReceiveThread();
        startSendThread();
        startTimeoutThread();
    }

    /**
     * Serializes the network controller.
     */
    @Serial
    private void writeObject(ObjectOutputStream aOutputStream) throws IOException
    {
        aOutputStream.writeObject(queue);
        aOutputStream.writeObject(lastReceivedPackets);
        aOutputStream.writeObject(netConf);
        aOutputStream.writeObject(localServerNetConf);
        aOutputStream.writeObject(new ConcurrentHashMap<>(timeoutRunnable.getTimeouts()));
    }
}
