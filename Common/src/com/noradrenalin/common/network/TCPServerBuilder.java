package com.noradrenalin.common.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Abstract class that allows an easier configuration of a receive thread.
 */
public abstract class TCPServerBuilder {
    protected ServerSocket serverSocket = null;
    protected Socket socket = null;
    protected ObjectOutputStream out = null;
    protected ObjectInputStream in = null;
    protected NetworkConfig netConf = null;
    private final int SERVER_TIMEOUT = 0;

    TCPServerBuilder() {}

    public NetworkConfig getNetConf() {
        return netConf;
    }

    /**
     * Closes server socket (first tries .close() and if that doesn't work it sets to null).
     */
    public void closeServerSocket() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch(IOException e) {
                serverSocket = null;
            }
        }
    }

    protected void setSocket(NetworkConfig netConf) throws IOException {
        this.netConf = netConf;
        this.serverSocket = new ServerSocket(this.netConf.getPort());
        System.out.println("Server is open on " + serverSocket.getLocalPort());
        netConf.setPort(serverSocket.getLocalPort());
        this.serverSocket.setSoTimeout(SERVER_TIMEOUT);
    }
}
