package com.noradrenalin.common.network;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Abstract class that allows an easier configuration of a send thread.
 */
public abstract class TCPClientBuilder {
    final int CLIENT_TIMEOUT = 1000;

    protected Socket socket = null;
    protected ObjectOutputStream out = null;
    protected NetworkConfig netConf = null;

    public void setSocketToNull() {
        socket = null;
    }

    protected void setSocket(NetworkConfig netConf) throws IOException {
        if (socket != null) {
            socket.close();
        }
        
        this.netConf = netConf;
        this.socket = new Socket(netConf.getIp(), this.netConf.getPort());

        System.out.println("Client socket is connected to " + this.netConf.getIpAsString() + " on " + netConf.getPortAsString());
        this.socket.setSoTimeout(CLIENT_TIMEOUT);
        this.out = new ObjectOutputStream(socket.getOutputStream());
    }
}