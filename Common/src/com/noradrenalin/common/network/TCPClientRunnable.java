package com.noradrenalin.common.network;

import com.noradrenalin.common.network.packets.Packet;
import com.noradrenalin.common.network.packets.listeners.PacketSentListener;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.concurrent.BlockingQueue;

/**
 * Runnable that sends all the packets that are present in its queue.
 */
public class TCPClientRunnable extends TCPClientBuilder implements Runnable {
    private final BlockingQueue<AbstractMap.SimpleEntry<Packet, NetworkConfig>> queue;
    private PacketSentListener packetSentListener = null;
    private final boolean running = true;
    private static final long RETRY_DELAY = 2000;

    public TCPClientRunnable(NetworkConfig netConf, BlockingQueue<AbstractMap.SimpleEntry<Packet, NetworkConfig>> queue) {
        this.netConf = netConf;
        this.queue = queue;
    }

    /**
     * Runnable logic, blocks while queue is empty.
     * When packet is received, it is sent to specified server.
     */
    @Override
    public void run() {
        AbstractMap.SimpleEntry<Packet, NetworkConfig> temp;
        try {
            while (running) {
                temp = queue.take();

                if (temp.getValue() != null) {
                    netConf = temp.getValue();
                }

                try {
                    setSocket(netConf);
                } catch (IOException e) {
                    NetworkThreadException exception = new NetworkThreadException(temp.getKey(), temp.getValue(), NetworkThreadException.Type.ActiveSocketOpen);
                    Thread.sleep(RETRY_DELAY);
                    throw new RuntimeException(exception);
                }

                try {
                    out.writeObject(temp.getKey());
                } catch (IOException e) {
                    NetworkThreadException exception = new NetworkThreadException(temp.getKey(), temp.getValue(), NetworkThreadException.Type.PacketSend);
                    throw new RuntimeException(exception);
                }

                packetSentListener.packetSent(temp.getKey(), temp.getValue());

                try {
                    socket.close();
                } catch (IOException e) {
                    NetworkThreadException exception = new NetworkThreadException(temp.getKey(), temp.getValue(), NetworkThreadException.Type.SocketClose);
                    throw new RuntimeException(exception);
                }
            }
        } catch (InterruptedException e) {
            // We print the stack trace here as this can only occur if a ctrl+c is performed
            e.printStackTrace();
        }
    }

    public void setPacketSentListener(PacketSentListener packetSentListener) {
        this.packetSentListener = packetSentListener;
    }
}


