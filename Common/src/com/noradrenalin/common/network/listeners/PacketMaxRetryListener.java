package com.noradrenalin.common.network.listeners;

import com.noradrenalin.common.network.NetworkConfig;
import com.noradrenalin.common.network.packets.Packet;

/**
 * Interface for a listener that is triggered when a packet has reached its maximum number of tries.
 */
public interface PacketMaxRetryListener {
    void packetMaxRetry(Packet packet, NetworkConfig dest);
}
