package com.noradrenalin.common.network.listeners;

/**
 * Interface for a listener that is triggered when a TCP connection error occurs.
 */
public interface TCPErrorListener {
    void tcpErrorOccurred(Exception e);
}
