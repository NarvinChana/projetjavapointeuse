package com.noradrenalin.common.network;

import com.noradrenalin.common.network.packets.listeners.PacketListener;

/**
 * Runnable that will open a Socket with each new incoming connection to handle the Packets and notify its controller.
 */
public class TCPServerRunnable extends TCPServerBuilder implements Runnable {
    private PacketListener pListener = null;
    private boolean running = true;

    public TCPServerRunnable(NetworkConfig networkConfig) {
        netConf = networkConfig;
    }

    @Override
    public void run() {
        try {
            this.setSocket(netConf);
        } catch (Exception e) {
            NetworkThreadException exception = new NetworkThreadException(null, null, NetworkThreadException.Type.ActiveSocketOpen);

            throw new RuntimeException(exception);
        }
        while (running) {
            socket = null;
            try {
                socket = serverSocket.accept();
                new TCPServerChildThread(socket, pListener).start();
            } catch (Exception e) {
                NetworkThreadException exception = new NetworkThreadException(null, null, NetworkThreadException.Type.SocketAccept);
                running = false;
                throw new RuntimeException(exception);
            }
        }
    }

    public void setPListener(PacketListener pListener) {
        this.pListener = pListener;
    }
}
