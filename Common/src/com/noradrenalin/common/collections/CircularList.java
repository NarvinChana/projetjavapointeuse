package com.noradrenalin.common.collections;

import java.io.Serializable;
import java.lang.reflect.Array;

/**
 * List of a fixed size of T.
 *
 * When a new element is added and the list is full, the oldest element will be replaced.
 * @param <T> The type of data
 */
public class CircularList<T> implements Serializable {
    private final T[] values;

    private int pos = 0;
    private final int size;

    public CircularList(Class<T> c, int s) {
        values = (T[]) Array.newInstance(c, s);
        size = s;
    }

    /**
     * Adds the element on the oldest position.
     */
    public void add(T element) {
        values[pos] = element;
        pos = (pos + 1) % size;
    }

    /**
     * Checks if an element is in the list.
     * @return true if element is in the list else false
     */
    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (values[i] == element) {
                return true;
            }
        }
        return false;
    }
}
